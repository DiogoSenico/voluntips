require('dotenv').config();
const express = require('express');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');
const path = require('path');
const swaggerUi = require('swagger-ui-express');

const swaggerDocument = require('./docs/swagger.json');
const config = require('./config');
const database = require('./server/utils/database');

//DATABASE
database.init();
database.populate();

const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(fileUpload({ createParentPath: true }));

app.use(
    '/api-docs',
    function (req, res, next) {
        swaggerDocument.host = req.get('host');
        req.swaggerDoc = swaggerDocument;
        next();
    },
    swaggerUi.serve,
    swaggerUi.setup()
);

app.use(express.static(path.join(__dirname, 'client/build')));
app.use('/uploads', express.static(path.join(__dirname, '/server/uploads')));

const UserApi = require('./server/api/user');
app.use('/api', UserApi);

const AuthApi = require('./server/api/auth');
app.use('/api', AuthApi);

const VolunteeringApi = require('./server/api/volunteering');
app.use('/api', VolunteeringApi);

const DonationApi = require('./server/api/donation');
app.use('/api', DonationApi);

const ProductApi = require('./server/api/product');
app.use('/api', ProductApi);

const ProjectApi = require('./server/api/project');
app.use('/api', ProjectApi);

if (process.env.NODE_ENV !== 'dev') {
    app.get('*/', function (req, res) {
        res.sendFile(path.join(__dirname + '/client/build/index.html'));
    });
}

const server = app.listen(config.serverPort || 5000, () => {
    const host =
        server.address().address === '::'
            ? 'localhost'
            : server.address().address;
    const { port } = server.address();
    console.log('App listening at http://%s:%s', host, port);
});

module.exports = server;
