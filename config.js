require('dotenv').config();

module.exports = {
    serverPort: process.env.PORT,
    PROJECT_DIR: __dirname,
    baseUrl: process.env.BASE_CLIENT_URL,
    jwt: {
        secret: process.env.JWT_SECRET,
        options: {
            algorithm: process.env.JWT_OPTIONS_ALGORITHM,
        },
    },
    remotedb: process.env.DB_CONN,
    internalEmailEnd: process.env.INTERNAL_EMAIL_END,
    sendgrid: {
        apikey: process.env.SENDGRID_API_KEY,
        from: {
            email: process.env.SENDGRID_FROM_EMAIL,
            name: process.env.SENDGRID_FROM_NAME,
        },
    },
    adminAccount: {
        name: process.env.ADMIN_ACC_NAME,
        email: process.env.ADMIN_ACC_EMAIL,
        password: process.env.ADMIN_ACC_PASSWORD,
    },
};
