const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mustache = require('mustache');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe(
    mocks.POST_VOLUNTEERING.endPoint + ' Unit tests suggest volunteering',
    () => {
        const endPoint = mocks.POST_VOLUNTEERING.endPoint;
        it('should suggest a volunteering success', (done) => {
            try {
                const data = mocks.POST_VOLUNTEERING.SUGGEST;

                chai.request(localhostUrl)
                    .post(mustache.render(endPoint, data.urlParams))
                    .set('Cookie', 'token=' + data.token)
                    .send(data.body)
                    .end((err, res) => {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.status).to.equal(data.output.status);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        done();
                    });
            } catch (err) {
                console.log(err);
            }
        });
        it('should create a volunteering success', (done) => {
            try {
                const data = mocks.POST_VOLUNTEERING.CREATE;

                chai.request(localhostUrl)
                    .post(mustache.render(endPoint, data.urlParams))
                    .set('Cookie', 'token=' + data.token)
                    .send(data.body)
                    .end((err, res) => {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        chai.expect(res.status).to.equal(data.output.status);
                        done();
                    });
            } catch (err) {
                console.log(err);
            }
        });
    }
);

describe(mocks.VOLUNTEERING_LIST.endPoint + ' Unit tests get projects', () => {
    const endPoint = mocks.VOLUNTEERING_LIST.endPoint;
    it('should get a list of projects with success', (done) => {
        try {
            const data = mocks.VOLUNTEERING_LIST.SUCCESS;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    chai.expect(res.status).to.equal(data.output.status);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(
    mocks.USER_APPLY_VOLUNTEERING.endPoint +
        ' Unit tests apply to volunteering',
    () => {
        // const endPoint = mocks.USER_APPLY_VOLUNTEERING.endPoint;
        // it('should make an apply to a volunteering', (done) => {
        //     try {
        //         const data = mocks.USER_APPLY_VOLUNTEERING.SUCCESS;
        //         chai.request(localhostUrl)
        //             .post(mustache.render(endPoint, data.urlParams))
        //             .set('Cookie', 'token=' + data.token)
        //             .end((err, res) => {
        //                 chai.assert.isNull(err);
        //                 chai.assert.isNotEmpty(res.body);
        //                 chai.expect(res.status).to.equal(data.output.status);
        //                 chai.expect(res.body.message).to.equal(
        //                     data.output.message
        //                 );
        //                 done();
        //             });
        //     } catch (err) {
        //         console.log(err);
        //     }
        // });
    }
);

describe(
    mocks.APPLY_TO_VOLUNTEERING_RESPONSE.endPoint +
        ' Unit tests response to user volunteering aplication',
    () => {
        // const endPoint = mocks.APPLY_TO_VOLUNTEERING_RESPONSE.endPoint;
        // it('should accept the user in the given volunteering', (done) => {
        //     try {
        //         const data = mocks.APPLY_TO_VOLUNTEERING_RESPONSE.SUCCESS;
        //         chai.request(localhostUrl)
        //             .post(mustache.render(endPoint, data.urlParams))
        //             .set('Cookie', 'token=' + data.token)
        //             .send(data.body)
        //             .end((err, res) => {
        //                 chai.assert.isNull(err);
        //                 chai.assert.isNotEmpty(res.body);
        //                 chai.expect(res.status).to.equal(data.output.status);
        //                 chai.expect(res.body.message).to.equal(
        //                     data.output.message
        //                 );
        //                 done();
        //             });
        //     } catch (err) {
        //         console.log(err);
        //     }
        // });
    }
);

describe(
    mocks.VOLUNTEERING_RESPONSE.endPoint +
        ' Unit tests response volunteering aplication',
    () => {
        // const endPoint = mocks.VOLUNTEERING_RESPONSE.endPoint;
        // it('should accept the given volunteering', (done) => {
        //     try {
        //         const data = mocks.VOLUNTEERING_RESPONSE.SUCCESS;
        //         chai.request(localhostUrl)
        //             .post(mustache.render(endPoint, data.urlParams))
        //             .set('Cookie', 'token=' + data.token)
        //             .send(data.body)
        //             .end((err, res) => {
        //                 chai.assert.isNull(err);
        //                 chai.assert.isNotEmpty(res.body);
        //                 chai.expect(res.body.message).to.equal(
        //                     data.output.message
        //                 );
        //                 chai.expect(res.status).to.equal(data.output.status);
        //                 done();
        //             });
        //     } catch (err) {
        //         console.log(err);
        //     }
        // });
    }
);
