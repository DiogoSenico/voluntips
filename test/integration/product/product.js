const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mustache = require('mustache');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe.only(mocks.PRODUCT_LIST.endPoint + ' Unit tests get products', () => {
    const endPoint = mocks.PRODUCT_LIST.endPoint;
    it('should get a list of products with success', done => {
        try {
            const data = mocks.PRODUCT_LIST.SUCCESS;

            chai.request(localhostUrl)
                .get(endPoint)
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(
    mocks.PRODUCT_UPDATE.endPoint + ' Unit tests updates a product',
    () => {
        const endPoint = mocks.PRODUCT_UPDATE.endPoint;
        it('should update a product with success', done => {
            try {
                const data = mocks.PROJECT_UPDATE.SUCCESS;

                chai.request(localhostUrl)
                    .put(mustache.render(endPoint, data.urlParams))
                    .set('Cookie', 'token=' + data.token)
                    .send(data.body)
                    .end((err, res) => {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.status).to.equal(data.output.status);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        done();
                    });
            } catch (err) {
                console.log(err);
            }
        });
    }
);
