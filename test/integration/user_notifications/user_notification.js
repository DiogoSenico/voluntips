/*
const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');

const config = require('../../../config');

const User = require('../../../server/models/user');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

mongoose.connect(config.remotedb, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
});

const localhostUrl = 'http://localhost:5000';

describe('Integration test Register with Notification', () => {
    const endPoint = mocks.REGISTER_NOTIFICATION.endPoint;
    it('/api/auth/register should register an internal user', (done) => {
        try {
            const data = mocks.REGISTER_NOTIFICATION.INTERNAL_USER;

            chai.request(localhostUrl)
                .post(endPoint)
                //.set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end(async (err, res) => {
                    try {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.status).to.equal(data.output.status);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        await User.deleteOne({ email: data.body.email });
                        done();
                    } catch (err) {
                        console.log('---');
                    }
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('/api/auth/register should register an external user', (done) => {
        try {
            const data = mocks.REGISTER_NOTIFICATION.EXTERNAL_USER;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end(async (err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    await User.deleteOne({ email: data.body.email });
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
*/
