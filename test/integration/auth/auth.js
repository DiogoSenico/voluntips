const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mustache = require('mustache');

// eslint-disable-next-line no-unused-vars
const server = require('../../../app');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe(mocks.CONFIRMATION.endPoint + ' Unit tests confirmation', () => {
    const endPoint = mocks.CONFIRMATION.endPoint;
    // it('should activate an user with success', (done) => {
    //     try {
    //         const data = mocks.CONFIRMATION.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .get(mustache.render(endPoint, data.urlParams))
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should not activate an user', (done) => {
        try {
            const data = mocks.CONFIRMATION.UNSUCCESSFUL;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.RECOVER_TOKEN.endPoint + ' Unit recover account verify', () => {
    const endPoint = mocks.RECOVER_TOKEN.endPoint;
    // it('should verify if the given token is correct', (done) => {
    //     try {
    //         const data = mocks.RECOVER_TOKEN.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .post(mustache.render(endPoint, data.urlParams))
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should pass if the token is not valid', (done) => {
        try {
            const data = mocks.RECOVER_TOKEN.NOT_FOUND;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.RECOVER_VERIFY.endPoint + ' Unit recover change pass', () => {
    const endPoint = mocks.RECOVER_VERIFY.endPoint;
    // it('should change the user password for the given token', (done) => {
    //     try {
    //         const data = mocks.RECOVER_VERIFY.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .post(endPoint)
    //             .send(data.body)
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should pass if the token is not valid', (done) => {
        try {
            const data = mocks.RECOVER_VERIFY.NOT_UPDATED;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.RECOVER_REQUEST.endPoint + ' Unit recover request', () => {
    const endPoint = mocks.RECOVER_REQUEST.endPoint;
    // it('should make a recovery account request', (done) => {
    //     try {
    //         const data = mocks.RECOVER_REQUEST.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .post(endPoint)
    //             .send(data.body)
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    // it('should pass if account recovery is already active', (done) => {
    //     try {
    //         const data = mocks.RECOVER_REQUEST.ALREADY_RECOVERING;

    //         chai.request(localhostUrl)
    //             .post(endPoint)
    //             .send(data.body)
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should pass if account does not exist', (done) => {
        try {
            const data = mocks.RECOVER_REQUEST.NOT_REGISTERED;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should pass if account is not active', (done) => {
        try {
            const data = mocks.RECOVER_REQUEST.NOT_ACTIVE;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.LOGIN.endPoint + ' Unit tests login', () => {
    const endPoint = mocks.LOGIN.endPoint;
    it('should login an user with success', (done) => {
        try {
            const data = mocks.LOGIN.SUCCESSFUL_LOGIN;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not be a valid user', (done) => {
        try {
            const data = mocks.LOGIN.INVALID_USER;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not be a valid password', (done) => {
        try {
            const data = mocks.LOGIN.INVALID_PASSWORD;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not be an active account', (done) => {
        try {
            const data = mocks.LOGIN.ACCOUNT_NOT_ACTIVE;

            chai.request(localhostUrl)
                .post(endPoint)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.AUTH.endPoint + ' Unit test auth', () => {
    const endPoint = mocks.AUTH.endPoint;
    it('should verify if the user is logged and has permission', (done) => {
        try {
            const data = mocks.AUTH.SUCCESSFUL;

            chai.request(localhostUrl)
                .post(endPoint)
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should verify if the user is logged but doesnt have permission', (done) => {
        try {
            const data = mocks.AUTH.INVALID_ROLE;

            chai.request(localhostUrl)
                .post(endPoint)
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should pass if user not logged', (done) => {
        try {
            const data = mocks.AUTH.INVALID_ROLE;

            chai.request(localhostUrl)
                .post(endPoint)
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
