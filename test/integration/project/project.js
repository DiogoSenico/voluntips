const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mustache = require('mustache');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe(mocks.PROJECT_LIST.endPoint + ' Unit tests get projects', () => {
    const endPoint = mocks.PROJECT_LIST.endPoint;
    it('should get a list of projects with success', (done) => {
        try {
            const data = mocks.PROJECT_LIST.SUCCESS;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.PROJECT_DETAILS.endPoint + ' Unit tests get a project', () => {
    const endPoint = mocks.PROJECT_DETAILS.endPoint;
    it('should get a project with success', (done) => {
        try {
            const data = mocks.PROJECT_DETAILS.SUCCESS;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should get not found a project', (done) => {
        try {
            const data = mocks.PROJECT_DETAILS.NOT_FOUND;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(
    mocks.PROJECT_UPDATE.endPoint + ' Unit tests updates a project',
    () => {
        const endPoint = mocks.PROJECT_UPDATE.endPoint;
        it('should update a project with success', (done) => {
            try {
                const data = mocks.PROJECT_UPDATE.SUCCESS;

                chai.request(localhostUrl)
                    .put(mustache.render(endPoint, data.urlParams))
                    .set('Cookie', 'token=' + data.token)
                    .send(data.body)
                    .end((err, res) => {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.status).to.equal(data.output.status);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        done();
                    });
            } catch (err) {
                console.log(err);
            }
        });
    }
);
