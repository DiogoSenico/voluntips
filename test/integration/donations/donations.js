const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mustache = require('mustache');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe(mocks.POST_DONATION.endPoint + ' Unit tests suggest donation', () => {
    const endPoint = mocks.POST_DONATION.endPoint;
    it('should create a donation success', done => {
        try {
            const data = mocks.POST_DONATION.CREATE;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(mocks.DONATION_LIST.endPoint + ' Unit tests get donations', () => {
    const endPoint = mocks.DONATION_LIST.endPoint;
    it('should get a list of donations with success', done => {
        try {
            const data = mocks.DONATION_LIST.SUCCESS;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    chai.expect(res.status).to.equal(data.output.status);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(
    mocks.DONATION_ACT_LIST.endPoint + ' Unit tests get all donation acts',
    () => {
        const endPoint = mocks.DONATION_ACT_LIST.endPoint;
        it('should get a list of all donation acts with success', done => {
            try {
                const data = mocks.DONATION_ACT_LIST.SUCCESS;

                chai.request(localhostUrl)
                    .get(mustache.render(endPoint, data.urlParams))
                    .set('Cookie', 'token=' + data.token)
                    .end((err, res) => {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        chai.expect(res.status).to.equal(data.output.status);
                        done();
                    });
            } catch (err) {
                console.log(err);
            }
        });
    }
);

describe(
    mocks.MAKE_DONATION.endPoint + ' Unit tests get all donation acts',
    () => {
        const endPoint = mocks.MAKE_DONATION.endPoint;
        it('should get a list of all donation acts with success', done => {
            try {
                const data = mocks.MAKE_DONATION.SUCCESS;

                chai.request(localhostUrl)
                    .post(mustache.render(endPoint, data.urlParams))
                    .set('Cookie', 'token=' + data.token)
                    .send(data.body)
                    .end((err, res) => {
                        chai.assert.isNull(err);
                        chai.assert.isNotEmpty(res.body);
                        chai.expect(res.body.message).to.equal(
                            data.output.message
                        );
                        chai.expect(res.status).to.equal(data.output.status);
                        done();
                    });
            } catch (err) {
                console.log(err);
            }
        });
    }
);
