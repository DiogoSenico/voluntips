const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const mustache = require('mustache');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe(`${mocks.UPDATE_USER_BY_ID.endPoint} Unit tests update by user id`, () => {
    const endPoint = mocks.UPDATE_USER_BY_ID.endPoint;
    it('should update an user by id with success', (done) => {
        try {
            const data = mocks.UPDATE_USER_BY_ID.SUCCESSFUL;

            chai.request(localhostUrl)
                .put(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not update an user by id with success', (done) => {
        try {
            const data = mocks.UPDATE_USER_BY_ID.UNSUCCESSFUL;

            chai.request(localhostUrl)
                .put(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
describe(`${mocks.GET_USER_BY_ID.endPoint} Unit tests get user by id`, () => {
    const endPoint = mocks.GET_USER_BY_ID.endPoint;
    it('should get an user by id with success', (done) => {
        try {
            const data = mocks.GET_USER_BY_ID.SUCCESSFUL;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not find an user with this id', (done) => {
        try {
            const data = mocks.GET_USER_BY_ID.NOT_FOUND;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
describe(`${mocks.GET_ALL_USERS.endPoint} Unit tests get all users`, () => {
    const endPoint = mocks.GET_ALL_USERS.endPoint;
    it('should get all users with success', (done) => {
        try {
            const data = mocks.GET_ALL_USERS.SUCCESSFUL;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
describe(`${mocks.ACTIVATE_ACCOUNT.endPoint} Unit tests activate account`, () => {
    const endPoint = mocks.ACTIVATE_ACCOUNT.endPoint;
    it('should activate an account with success', (done) => {
        try {
            const data = mocks.ACTIVATE_ACCOUNT.SUCCESSFUL;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not activate an account with success', (done) => {
        try {
            const data = mocks.ACTIVATE_ACCOUNT.NOT_FOUND;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
describe(`${mocks.DEACTIVATE_ACCOUNT.endPoint} Unit tests deactivate account`, () => {
    const endPoint = mocks.DEACTIVATE_ACCOUNT.endPoint;
    it('should deactivate an account with success', (done) => {
        try {
            const data = mocks.DEACTIVATE_ACCOUNT.SUCCESSFUL;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
    it('should not deactivate an account with success', (done) => {
        try {
            const data = mocks.DEACTIVATE_ACCOUNT.NOT_FOUND;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
describe(`${mocks.ACCEPT_EXTERNAL_ACCOUNT.endPoint} Unit tests accept external account`, () => {
    const endPoint = mocks.ACCEPT_EXTERNAL_ACCOUNT.endPoint;
    // it('should accept an external account with success', (done) => {
    //     try {
    //         const data = mocks.ACCEPT_EXTERNAL_ACCOUNT.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .post(mustache.render(endPoint, data.urlParams))
    //             .set('Cookie', 'token=' + data.token)
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should not accept an external account with success', (done) => {
        try {
            const data = mocks.ACCEPT_EXTERNAL_ACCOUNT.NOT_FOUND;

            chai.request(localhostUrl)
                .post(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
describe(`${mocks.REJECT_EXTERNAL_ACCOUNT.endPoint} Unit tests reject external account`, () => {
    const endPoint = mocks.REJECT_EXTERNAL_ACCOUNT.endPoint;
    // it('should reject an external account with success', (done) => {
    //     try {
    //         const data = mocks.REJECT_EXTERNAL_ACCOUNT.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .delete(mustache.render(endPoint, data.urlParams))
    //             .set('Cookie', 'token=' + data.token)
    //             .end((err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should not reject an external account with success', (done) => {
        try {
            const data = mocks.REJECT_EXTERNAL_ACCOUNT.NOT_FOUND;

            chai.request(localhostUrl)
                .delete(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(`${mocks.ADD_MANAGER.endPoint} Unit tests add manager`, () => {
    const endPoint = mocks.ADD_MANAGER.endPoint;
    // it('should add a manager with success', (done) => {
    //     try {
    //         const data = mocks.ADD_MANAGER.SUCCESSFUL;

    //         chai.request(localhostUrl)
    //             .post(endPoint)
    //             .set('Cookie', 'token=' + data.token)
    //             .send(data.body)
    //             .end(async (err, res) => {
    //                 chai.assert.isNull(err);
    //                 chai.assert.isNotEmpty(res.body);
    //                 chai.expect(res.status).to.equal(data.output.status);
    //                 chai.expect(res.body.message).to.equal(data.output.message);
    //                 done();
    //             });
    //     } catch (err) {
    //         console.log(err);
    //     }
    // });
    it('should not add a manager with success', (done) => {
        try {
            const data = mocks.ADD_MANAGER.UNSUCCESSFUL;

            chai.request(localhostUrl)
                .post(endPoint)
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(`${mocks.GET_USER_VOLUNTEERINGS.endPoint} Unit tests get user's volunteerings`, () => {
    const endPoint = mocks.GET_USER_VOLUNTEERINGS.endPoint;
    it('should get all user created and suggested volunteerings', (done) => {
        try {
            const data = mocks.GET_USER_VOLUNTEERINGS.SUCCESSFUL;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    chai.expect(res.status).to.equal(data.output.status);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});

describe(`${mocks.GET_USER_APPLICATIONS.endPoint} Unit tests get user's applications`, () => {
    const endPoint = mocks.GET_USER_APPLICATIONS.endPoint;
    it('should get all user volunteering applications', (done) => {
        try {
            const data = mocks.GET_USER_APPLICATIONS.SUCCESSFUL;

            chai.request(localhostUrl)
                .get(mustache.render(endPoint, data.urlParams))
                .set('Cookie', 'token=' + data.token)
                .send(data.body)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    chai.expect(res.status).to.equal(data.output.status);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
