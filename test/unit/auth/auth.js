const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');

const mocks = require('./mocks.json');

chai.use(chaiHttp);
chai.should();

const localhostUrl = 'http://localhost:5000';

describe(mocks.LOGOUT.endPoint + ' Unit test logout', () => {
    const endPoint = mocks.LOGOUT.endPoint;
    it('should logout an user with success', (done) => {
        try {
            const data = mocks.LOGOUT.SUCCESSFUL;

            chai.request(localhostUrl)
                .post(endPoint)
                .set('Cookie', 'token=' + data.token)
                .end((err, res) => {
                    chai.assert.isNull(err);
                    chai.assert.isNotEmpty(res.body);
                    chai.expect(res.status).to.equal(data.output.status);
                    chai.expect(res.body.message).to.equal(data.output.message);
                    done();
                });
        } catch (err) {
            console.log(err);
        }
    });
});
