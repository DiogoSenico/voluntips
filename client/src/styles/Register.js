const styles = () => ({
    container: {
        margin: 'auto',
    },
    wrapper: {
        padding: 16,
        margin: '32px 0px',
    },
    submitBtn: {
        alignSelf: 'flex-end',
    },
});

export default styles;
