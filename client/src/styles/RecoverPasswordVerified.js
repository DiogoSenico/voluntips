const styles = () => ({
    container: {
        width: 500,
        padding: 16,
    },
    title: {
        paddingBottom: 10,
    },
    submitBtn: {
        marginTop: 10,
    },
});

export default styles;