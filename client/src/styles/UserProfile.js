const styles = () => ({
    profileImage: {
        height: 100,
        marginBottom: 16,
    },
});

export default styles;