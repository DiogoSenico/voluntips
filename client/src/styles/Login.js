const styles = () => ({
    image: {
        backgroundRepeat: 'no-repeat',
        backgroundColor: 'white',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
    },
    paper: {
        margin: '64px 32px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%',
        marginTop: '8px',
    },
    submit: {
        margin: '24px 0px 16px',
    },
    name: {
        backgroundColor: '#212121dd',
        color: 'white',
        padding: '8px 16px',
    },
    errorMsg: {
        alignSelf: 'stretch',
        marginTop: 16,
    },
});

export default styles;
