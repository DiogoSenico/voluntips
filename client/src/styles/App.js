const styles = () => ({
    app: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
    },
    content: {
        flex: 1,
        overflow: 'auto',
        '& > div': {
            minHeight: '100%',
        },
    },
    toast: {
        '& div.Toastify__toast': {
            minHeight: 'auto',
        },
        '& div.Toastify__toast--error': {
            backgroundColor: '#f44336',
        },
        '& div.Toastify__toast--success': {
            backgroundColor: '#4caf50',
        },
        '& div.Toastify__toast-body': {
            margin: '5px 0',
            fontWeight: 500,
        },
    },
});

export default styles;
