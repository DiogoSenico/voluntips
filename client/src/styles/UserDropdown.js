const styles = () => ({
    wrapper: {
        display: 'inline-block',
        position: 'relative',
    },
    paper: {
        position: 'absolute',
        width: 200,
        right: 0,
    },
});

export default styles;
