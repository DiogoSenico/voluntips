import { api } from '../utils/request';
import moment from 'moment';

async function getProjects(
    page,
    pageSize,
    search,
    minDate,
    maxDate,
    interestAreas
) {
    if (minDate) minDate = moment(minDate).format('YYYY-MM-DD');
    if (maxDate) maxDate = moment(maxDate).format('YYYY-MM-DD');

    let queryObj = {
        page,
        pageSize,
        search,
        minDate,
        maxDate,
        interestAreas,
    };

    Object.keys(queryObj).forEach((key) => {
        if (queryObj[key] === undefined || queryObj[key] === null) {
            delete queryObj[key];
        }
    });
    const query = new URLSearchParams(queryObj).toString();

    const res = await api({
        method: 'get',
        url: `/api/project?${query}`,
    });

    return res;
}

async function getProject(id) {
    const res = await api({
        method: 'get',
        url: `/api/project/${id}`,
    });

    return res;
}

async function updateProject(id, newInfo) {
    const res = await api({
        method: 'put',
        url: `/api/project/${id}`,
        data: newInfo,
        message: true,
    });

    return res;
}

export { getProjects, getProject, updateProject };
