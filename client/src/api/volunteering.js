import { api } from '../utils/request';

async function getVolunteerings(page, pageSize, search) {
    const query = new URLSearchParams({ page, pageSize, search }).toString();

    const res = await api({
        method: 'get',
        url: `/api/volunteering?${query}`,
    });

    return res;
}

async function createVolunteering(project) {
    Object.keys(project).forEach((key) => {
        if (project[key] === undefined || project[key] === '') {
            delete project[key];
        }
    });

    const res = await api({
        method: 'post',
        url: '/api/volunteering',
        data: project,
        message: true,
    });
    return res;
}

async function applyToVolunteering(projectId) {
    const res = await api({
        method: 'post',
        url: `/api/volunteering/${projectId}/apply`,
        message: true,
    });
    return res;
}

async function responseUserToVolunteering(projectId, userId, data) {
    const res = await api({
        method: 'post',
        url: `/api/volunteering/${projectId}/user/${userId}/response`,
        data: data,
        message: true,
    });
    return res;
}

async function validateVolunteering(projectId, data) {
    const res = await api({
        method: 'post',
        url: `/api/volunteering/${projectId}/response`,
        data: data,
        message: true,
    });
    return res;
}

export {
    getVolunteerings,
    createVolunteering,
    applyToVolunteering,
    responseUserToVolunteering,
    validateVolunteering,
};
