import { api } from '../utils/request';

async function getConstants(type) {
    const res = await api({
        method: 'post',
        url: `/api/user/constants`,
        data: { type },
    });

    return res;
}

async function getUsers(userType, page, pageSize, search) {
    const query = new URLSearchParams({
        userType,
        page,
        pageSize,
        search,
    }).toString();

    const res = await api({
        method: 'get',
        url: `/api/user?${query}`,
    });

    return res;
}

async function getUserProfile(id) {
    const res = await api({
        method: 'get',
        url: `/api/user/${id}`,
    });

    return res;
}

async function getUserVolunteerings(id) {
    const res = await api({
        method: 'get',
        url: `/api/user/${id}/volunteering`,
    });

    return res;
}

async function getUserApplications(id) {
    const res = await api({
        method: 'get',
        url: `/api/user/${id}/applications`,
    });

    return res;
}

async function updateUser(user) {
    const res = await api({
        method: 'put',
        url: `/api/user/${user._id}`,
        data: user,
        message: true,
    });

    return res;
}

async function uploadProfileImage(id, image) {
    const res = await api({
        method: 'post',
        url: `/api/user/${id}/upload`,
        data: image,
        headers: { 'Content-Type': 'multipart/form-data' },
        message: true,
    });

    return res;
}

async function activateUser(id) {
    const res = await api({
        method: 'post',
        url: `/api/user/activate/${id}`,
        message: true,
    });

    return res;
}

async function deactivateUser(id) {
    const res = await api({
        method: 'post',
        url: `/api/user/deactivate/${id}`,
        message: true,
    });

    return res;
}

async function acceptUser(id) {
    const res = await api({
        method: 'post',
        url: `/api/user/acceptexternal/${id}`,
        message: true,
    });

    return res;
}

async function rejectUser(id) {
    const res = await api({
        method: 'delete',
        url: `/api/user/rejectexternal/${id}`,
        message: true,
    });

    return res;
}

async function registerManager(user) {
    const res = await api({
        method: 'post',
        url: '/api/user/manager',
        data: user,
        message: true,
    });
    return res;
}

export {
    getConstants,
    getUsers,
    getUserProfile,
    getUserVolunteerings,
    getUserApplications,
    updateUser,
    uploadProfileImage,
    activateUser,
    deactivateUser,
    acceptUser,
    rejectUser,
    registerManager,
};
