import { api } from '../utils/request';

async function getDonationProjects(page, pageSize, search) {
    const query = new URLSearchParams({ page, pageSize, search }).toString();

    const res = await api({
        method: 'get',
        url: `/api/donation?${query}`,
    });

    return res;
}

async function getDonationsActs(page, pageSize, search) {
    const query = new URLSearchParams({ page, pageSize, search }).toString();

    const res = await api({
        method: 'get',
        url: `/api/donation/allActs?${query}`,
    });

    return res;
}

async function createDonationProject(project) {
    Object.keys(project).forEach((key) => {
        if (project[key] === undefined || project[key] === '') {
            delete project[key];
        }
    });

    const res = await api({
        method: 'post',
        url: '/api/donation',
        data: project,
        message: true,
    });
    return res;
}

async function makeDonation(id, data) {
    const res = await api({
        method: 'post',
        url: `/api/donation/${id}/donate`,
        data: data,
        message: true,
    });
    return res;
}

export {
    getDonationProjects,
    getDonationsActs,
    createDonationProject,
    makeDonation,
};
