import { api } from '../utils/request';

async function verifyAuth(role) {
    const res = await api({
        method: 'post',
        url: '/api/auth/',
        data: role,
    });
    return res;
}

async function register(user) {
    const res = await api({
        method: 'post',
        url: '/api/auth/register',
        data: user,
        message: true,
    });
    return res;
}

async function activateAccount(token) {
    const res = await api({
        method: 'get',
        url: `/api/auth/confirmation/${token}`,
    });
    return res;
}

async function login(user) {
    const res = await api({
        method: 'post',
        url: '/api/auth/login',
        data: user,
    });
    return res;
}

async function logout() {
    const res = await api({
        method: 'post',
        url: '/api/auth/logout',
    });
    return res;
}

async function recoverPassword(email) {
    const res = await api({
        method: 'post',
        url: '/api/auth/recover',
        data: email,
    });
    return res;
}

async function verifyPasswordRecoveryToken(token) {
    const res = await api({
        method: 'post',
        url: `/api/auth/recover/${token}`,
    });
    return res;
}

async function recoverPasswordVerified(data) {
    const res = await api({
        method: 'post',
        url: '/api/auth/recoverVerified',
        data,
    });
    return res;
}

export {
    verifyAuth,
    register,
    activateAccount,
    login,
    logout,
    recoverPassword,
    verifyPasswordRecoveryToken,
    recoverPasswordVerified,
};
