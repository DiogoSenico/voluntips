import { api } from '../utils/request';

async function getProducts() {
    const res = await api({
        method: 'get',
        url: `/api/product`,
    });

    return res;
}

async function changeProductStock(id, amount) {
    const res = await api({
        method: 'put',
        url: `/api/product/${id}`,
        data: { amount },
        message: true,
    });

    return res;
}

export { getProducts, changeProductStock };
