import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createMuiTheme } from '@material-ui/core';
import { withStyles, ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import styles from '../styles/App';
import Header from './header/Header';
import Register from './auth/Register';
import ActivateAccount from './auth/ActivateAccount';
import Login from './auth/Login';
import RecoverPassword from './auth/RecoverPassword';
import RecoverPasswordVerified from './auth/RecoverPasswordVerified';
import UserProfile from './user/UserProfile';
import withAuth from './auth/withAuth';
import withoutAuth from './auth/withoutAuth';
import Feed from './projects/Feed';
import VolunteeringCreation from './projects/volunteerings/VolunteeringCreation';
import VolunteeringDetails from './projects/volunteerings/VolunteeringDetails';
import VolunteeringEdit from './projects/volunteerings/VolunteeringEdit';
import DonationCreation from './projects/donations/DonationCreation';
import DonationDetails from './projects/donations/DonationDetails';
import DonationEdit from './projects/donations/DonationEdit';
import Management from './management/Management';

const theme = createMuiTheme({
    palette: {
        common: { black: '#000', white: '#fff' },
        background: { paper: '#fff', default: '#fafafa' },
        primary: {
            light: 'rgba(99, 164, 255, 1)',
            main: 'rgba(25, 118, 210, 1)',
            dark: 'rgba(0, 75, 160, 1)',
            contrastText: '#fff',
        },
        secondary: {
            light: 'rgba(255, 173, 66, 1)',
            main: 'rgba(245, 124, 0, 1)',
            dark: 'rgba(187, 77, 0, 1)',
            contrastText: '#fff',
        },
        error: {
            light: '#e57373',
            main: '#f44336',
            dark: '#d32f2f',
            contrastText: '#fff',
        },
        text: {
            primary: 'rgba(0, 0, 0, 0.87)',
            secondary: 'rgba(0, 0, 0, 0.54)',
            disabled: 'rgba(0, 0, 0, 0.38)',
            hint: 'rgba(0, 0, 0, 0.38)',
        },
    },
});

class App extends Component {
    renderRoutes() {
        return (
            <Switch>
                <Route exact path="/" component={Feed} />
                <Route
                    exact
                    path="/project/create"
                    component={withAuth(VolunteeringCreation, 'USER', '/login')}
                />
                <Route
                    exact
                    path="/project/edit/:id"
                    component={withAuth(VolunteeringEdit, 'USER')}
                />
                <Route
                    exact
                    path="/project/:id"
                    component={withAuth(VolunteeringDetails, 'USER', '/login')}
                />
                <Route
                    exact
                    path="/donation/create"
                    component={withAuth(DonationCreation, 'MANAGER')}
                />
                <Route
                    exact
                    path="/donation/edit/:id"
                    component={withAuth(DonationEdit, 'MANAGER')}
                />
                <Route
                    exact
                    path="/donation/:id"
                    component={withAuth(DonationDetails, 'USER', '/login')}
                />
                <Route
                    exact
                    path="/register"
                    component={withoutAuth(Register)}
                />
                <Route exact path="/login" component={withoutAuth(Login)} />
                <Route
                    exact
                    path="/activateaccount/:token"
                    component={withoutAuth(ActivateAccount)}
                />
                <Route
                    exact
                    path="/recover"
                    component={withoutAuth(RecoverPassword)}
                />
                <Route
                    exact
                    path="/recover/:token"
                    component={withoutAuth(RecoverPasswordVerified)}
                />

                <Route
                    exact
                    path="/management"
                    component={withAuth(Management, 'MANAGER')}
                />
                <Route
                    exact
                    path="/management/users"
                    component={withAuth(Management, 'MANAGER')}
                />
                <Route
                    exact
                    path="/management/managers"
                    component={withAuth(Management, 'ADMIN')}
                />
                <Route
                    exact
                    path="/management/volunteerings"
                    component={withAuth(Management, 'MANAGER')}
                />
                <Route
                    exact
                    path="/management/donations"
                    component={withAuth(Management, 'MANAGER')}
                />
                <Route
                    exact
                    path="/management/donations/history"
                    component={withAuth(Management, 'MANAGER')}
                />
                <Route
                    exact
                    path="/management/stocks"
                    component={withAuth(Management, 'MANAGER')}
                />
                <Route
                    exact
                    path="/user/:id"
                    component={withAuth(UserProfile, 'USER')}
                />
            </Switch>
        );
    }

    render() {
        const { classes } = this.props;

        return (
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <BrowserRouter>
                    <div className={classes.app}>
                        <Header />
                        <div className={classes.content}>
                            {this.renderRoutes()}
                        </div>
                        <ToastContainer
                            position="bottom-right"
                            newestOnTop
                            className={classes.toast}
                        />
                    </div>
                </BrowserRouter>
            </ThemeProvider>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
