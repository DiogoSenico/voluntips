import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import styles from '../../styles/RecoverPassword';
import { recoverPassword } from '../../api/auth';

class RecoverPassword extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            notificationSent: false,
            errorMsg: null,
        };

        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeEmail(e) {
        this.setState({ email: e.target.value });
    }

    async handleSubmit(e) {
        const { email } = this.state;
        e.preventDefault();

        const response = await recoverPassword({ email });
        if (response.status === 200) {
            this.setState({ notificationSent: true });
        } else {
            this.setState({ errorMsg: response.status });
        }
    }

    renderEmailForm() {
        const { classes } = this.props;
        const { email } = this.state;

        return (
            <Box component={Paper} p={2}>
                <Typography variant="h5" className={classes.title}>
                    {'Recuperação de conta:'}
                </Typography>
                <Typography variant="body1">
                    {'Insira o email relacionado com a sua conta:'}
                </Typography>
                <form className={classes.form} onSubmit={this.handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        value={email}
                        onChange={this.handleChangeEmail}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        {'Enviar pedido'}
                    </Button>
                </form>
            </Box>
        );
    }

    renderNotificationSent() {
        const { classes } = this.props;

        return (
            <Box component={Paper} p={2}>
                <Typography variant="h5" className={classes.title}>
                    {'Notificação foi enviada para o seu email'}
                </Typography>
                <Typography variant="body1">
                    {
                        'Por favor verifique o seu email para recuperar a sua password'
                    }
                </Typography>
            </Box>
        );
    }

    render() {
        const { notificationSent } = this.state;

        return (
            <Box display="flex" alignItems="center">
                <Container maxWidth="sm" disableGutters>
                    {notificationSent
                        ? this.renderNotificationSent()
                        : this.renderEmailForm()}
                </Container>
            </Box>
        );
    }
}

RecoverPassword.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecoverPassword);
