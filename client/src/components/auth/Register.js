import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Box from '@material-ui/core/Box';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';
import Chip from '@material-ui/core/Chip';
import Autocomplete from '@material-ui/lab/Autocomplete';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { toast } from 'react-toastify';

import styles from '../../styles/Register';
import { getConstants } from '../../api/user';
import { register } from '../../api/auth';

let initialState = {
    name: '',
    email: '',
    phone: '',
    password: '',
    passwordConf: '',
    address: '',
    birthdate: null,
    rgpd: false,
    memberType: 'STUDENT',
    interestAreas: [],
    reasons: [],
    entity: '',
    school: null,
    course: null,
    service: null,

    loadedMemberTypes: [],
    loadedInterestAreas: [],
    loadedReasons: [],
    loadedServices: [],
    loadedSchools: {},
    loadedCourses: [],
};

class Register extends Component {
    constructor() {
        super();
        this.state = initialState;

        this.getConstants = this.getConstants.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleSchoolChange = this.handleSchoolChange.bind(this);
        this.handleCourseChange = this.handleCourseChange.bind(this);
        this.handleServiceChange = this.handleServiceChange.bind(this);
        this.handleAreasChange = this.handleAreasChange.bind(this);
        this.handleMemberTypeChange = this.handleMemberTypeChange.bind(this);
        this.handleReasonsChange = this.handleReasonsChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRGPD = this.handleRGPD.bind(this);
    }

    getMaxDate() {
        let d = new Date();
        d.setFullYear(d.getFullYear() - 10);
        return d;
    }

    async getConstants() {
        const loadedMemberTypes = (await getConstants('memberTypes')).data;
        const loadedInterestAreas = (await getConstants('interestAreas')).data;
        const loadedReasons = (await getConstants('reasons')).data;
        const loadedServices = (await getConstants('services')).data;
        const loadedSchools = (await getConstants('schoolCourse')).data;

        this.setState(
            {
                loadedMemberTypes,
                loadedInterestAreas,
                loadedReasons,
                loadedServices,
                loadedSchools,
            },
            () => {
                initialState = this.state;
            }
        );
    }

    componentDidMount() {
        this.getConstants();
    }

    getFormData() {
        let {
            name,
            email,
            phone,
            password,
            passwordConf,
            address,
            birthdate,
            rgpd,
            memberType,
            interestAreas,
            reasons,
            entity,
            school,
            course,
            service,
        } = this.state;

        if (birthdate) birthdate = moment(birthdate).format('YYYY-MM-DD');

        if (password !== passwordConf) {
            throw new Error('Passwords não são iguais');
        }
        if (!rgpd)
            throw new Error('Para se registar deve aceitar os termos RGPD');

        return {
            name,
            email,
            phone,
            password,
            passwordConf,
            address,
            birthdate,
            rgpd,
            memberType,
            interestAreas,
            reasons,
            entity,
            school,
            course,
            service,
        };
    }

    async handleSubmit(e) {
        const { interestAreas, reasons } = this.state;
        e.preventDefault();

        if (interestAreas === undefined || interestAreas.length === 0) {
            toast.error('Por favor preencha o campo das áreas de interesse');
            return false;
        }
        if (reasons === undefined || reasons.length === 0) {
            toast.error('Por favor preencha o campo das razões');
            return false;
        }

        try {
            const data = this.getFormData();
            const response = await register(data);
            if (response.status === 201) {
                window.location.replace('/login');
            }
        } catch (e) {
            toast.error(e.message);
        }
    }

    handleInputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleRGPD(e) {
        this.setState((prevState) => ({ rgpd: !prevState.rgpd }));
    }

    handleDateChange(birthdate) {
        this.setState({ birthdate });
    }

    handleAreasChange(e, interestAreas) {
        this.setState({ interestAreas });
    }

    handleReasonsChange(e, reasons) {
        this.setState({ reasons });
    }

    handleSchoolChange(e, school) {
        const { loadedSchools } = this.state;
        this.setState({
            loadedCourses: loadedSchools[school],
            school,
            course: null,
        });
    }

    handleCourseChange(e, course) {
        this.setState({ course });
    }

    handleServiceChange(e, service) {
        this.setState({ service });
    }

    handleMemberTypeChange(e) {
        this.setState({
            memberType: e.target.value,
            entity: '',
            school: null,
            course: null,
            service: null,
        });
    }

    renderMemberTypes() {
        const { loadedMemberTypes, memberType } = this.state;

        let types = [];

        for (let m in loadedMemberTypes) {
            types.push(
                <MenuItem key={m} value={m}>
                    {loadedMemberTypes[m].text}
                </MenuItem>
            );
        }

        return (
            <FormControl variant="outlined" required fullWidth>
                <InputLabel id="demo-simple-select-outlined-label">
                    Tipo de membro
                </InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={memberType}
                    onChange={this.handleMemberTypeChange}
                    label="Tipo de membro"
                >
                    {types}
                </Select>
            </FormControl>
        );
    }

    renderInterestAreas() {
        const { loadedInterestAreas, interestAreas } = this.state;

        return (
            <Autocomplete
                multiple
                filterSelectedOptions
                options={loadedInterestAreas}
                value={interestAreas}
                onChange={this.handleAreasChange}
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip
                            variant="outlined"
                            label={option}
                            {...getTagProps({ index })}
                        />
                    ))
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="outlined"
                        label="Áreas de interesse *"
                    />
                )}
            />
        );
    }

    renderReasons() {
        const { loadedReasons, reasons } = this.state;

        return (
            <Autocomplete
                multiple
                freeSolo
                filterSelectedOptions
                options={loadedReasons}
                value={reasons}
                onChange={this.handleReasonsChange}
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip
                            variant="outlined"
                            label={option}
                            {...getTagProps({ index })}
                        />
                    ))
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="outlined"
                        label="Razões para ser voluntário *"
                        helperText="Pode adicionar as suas próprias razões"
                    />
                )}
            />
        );
    }

    renderSchools() {
        const { loadedSchools, school } = this.state;

        return (
            <Autocomplete
                value={school}
                options={Object.keys(loadedSchools)}
                onChange={this.handleSchoolChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required
                        variant="outlined"
                        label="Escola"
                    />
                )}
            />
        );
    }

    renderCourses() {
        const { loadedCourses, course } = this.state;

        return (
            <Autocomplete
                disabled={loadedCourses && loadedCourses.length === 0}
                options={loadedCourses || []}
                value={course}
                onChange={this.handleCourseChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required
                        variant="outlined"
                        label="Curso"
                    />
                )}
            />
        );
    }

    renderServices() {
        const { loadedServices, service } = this.state;

        return (
            <Autocomplete
                options={loadedServices}
                value={service}
                onChange={this.handleServiceChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required
                        variant="outlined"
                        label="Serviço"
                    />
                )}
            />
        );
    }

    renderEntity() {
        const { entity } = this.state;

        return (
            <TextField
                required
                id="entity"
                name="entity"
                label="Nome da entidade"
                type="text"
                fullWidth
                variant="outlined"
                value={entity}
                onChange={this.handleInputChange}
            />
        );
    }

    renderAdminData() {
        const { memberType } = this.state;

        switch (memberType) {
            case 'STUDENT':
            case 'GRATUATE':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                {'Dados administrativos:'}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            {this.renderSchools()}
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            {this.renderCourses()}
                        </Grid>
                    </>
                );
            case 'NON_TEACHER':
            case 'SCHOLAR':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                {'Dados administrativos:'}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            {this.renderServices()}
                        </Grid>
                    </>
                );
            case 'TEACHER':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                {'Dados administrativos:'}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            {this.renderSchools()}
                        </Grid>
                    </>
                );
            case 'ENTITY':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                {'Dados administrativos:'}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            {this.renderEntity()}
                        </Grid>
                    </>
                );
            default:
                return null;
        }
    }

    renderForm() {
        const { classes } = this.props;
        const {
            name,
            email,
            phone,
            address,
            birthdate,
            password,
            passwordConf,
            rgpd,
        } = this.state;

        return (
            <Paper variant="outlined" className={classes.wrapper}>
                <Typography variant="h4" gutterBottom>
                    {'Registar'}
                </Typography>
                <form onSubmit={this.handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                {'Dados Pessoais:'}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="name"
                                name="name"
                                label="Nome Completo"
                                type="text"
                                fullWidth
                                autoComplete="fname"
                                variant="outlined"
                                value={name}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                                <KeyboardDatePicker
                                    required
                                    fullWidth
                                    disableFuture
                                    openTo="year"
                                    variant="inline"
                                    inputVariant="outlined"
                                    orientation="landscape"
                                    format="DD/MM/YYYY"
                                    maxDate={this.getMaxDate()}
                                    minDateMessage="Data inválida (demasiado antiga)"
                                    maxDateMessage="Data inválida (demasiado no presente/futuro)"
                                    id="date-picker-inline"
                                    label="Data de nascimento"
                                    value={birthdate}
                                    views={['year', 'month', 'date']}
                                    onChange={this.handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="email"
                                name="email"
                                label="Email"
                                type="email"
                                variant="outlined"
                                fullWidth
                                autoComplete="email"
                                value={email}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="phone"
                                name="phone"
                                label="Telemóvel"
                                type="tel"
                                variant="outlined"
                                fullWidth
                                inputProps={{
                                    pattern: '[0-9]{3}[0-9]{3}[0-9]{3}',
                                    maxLength: '9',
                                    title: 'Exemplo: 999999999',
                                }}
                                autoComplete="tel-national"
                                value={phone}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                fullWidth
                                variant="outlined"
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                helperText="A sua password deverá conter no mínimo 8 caracteres, 1 número, 1 letra maiúscula e 1 letra minúscula."
                                inputProps={{
                                    pattern:
                                        '(?=.*d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
                                    title:
                                        'Por favor coloque o padrão requisitado',
                                }}
                                autoComplete="current-password"
                                value={password}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                fullWidth
                                variant="outlined"
                                name="passwordConf"
                                label="Confirmação de password"
                                type="password"
                                id="passwordConf"
                                helperText="A sua password deverá conter no mínimo 8 caracteres, 1 número, 1 letra maiúscula e 1 letra minúscula."
                                inputProps={{
                                    pattern:
                                        '(?=.*d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
                                    title:
                                        'Por favor coloque o padrão requisitado',
                                }}
                                autoComplete="current-password"
                                value={passwordConf}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                variant="outlined"
                                id="address"
                                name="address"
                                label="Morada"
                                fullWidth
                                autoComplete="billing address-line1"
                                value={address}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            {this.renderMemberTypes()}
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            {this.renderInterestAreas()}
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            {this.renderReasons()}
                        </Grid>
                        {this.renderAdminData()}
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={rgpd}
                                        onChange={(e) => this.handleRGPD(e)}
                                    />
                                }
                                label={
                                    'Consinto, ao abrigo do Regulamento Geral de Proteção de Dados (RGPD), a utilização dos meus dados pessoais, fornecidos no formulário, ficando informado/a do direito a retirar o consentimento a qualquer momento e que o tratamento de dados é da responsabilidade do IPS, sendo-lhe aplicada a Política de Proteção de Dados do IPS (disponível aqui)'
                                }
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Box display="flex" flexDirection="row-reverse">
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    type="submit"
                                >
                                    {'Registar'}
                                </Button>
                            </Box>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        );
    }

    render() {
        const { classes } = this.props;

        return (
            <Box display="flex" alignItems="center">
                <Container className={classes.container}>
                    {this.renderForm()}
                </Container>
            </Box>
        );
    }
}

Register.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Register);
