import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom';

import styles from '../../styles/RecoverPasswordVerified';
import {
    verifyPasswordRecoveryToken,
    recoverPasswordVerified,
} from '../../api/auth';

class RecoverPasswordVerified extends Component {
    constructor() {
        super();
        this.state = {
            password: '',
            passwordConfirmation: '',
            passwordChanged: false,
            errorMsg: null,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.verifyToken();
    }

    async verifyToken() {
        const { match } = this.props;

        const response = await verifyPasswordRecoveryToken(match.params.token);
        if (response.status !== 200) {
            window.location.replace('/');
        }
    }

    handleInputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    async handleSubmit(e) {
        const { match } = this.props;
        const { password, passwordConfirmation } = this.state;

        e.preventDefault();
        if (password === passwordConfirmation) {
            const response = await recoverPasswordVerified({
                token: match.params.token,
                password,
            });
            if (response.status === 200) {
                this.setState({ passwordChanged: true });
            } else {
                this.setState({
                    errorMsg: 'Não foi possivel alterar a sua password',
                });
            }
        } else {
            this.setState({ errorMsg: 'As passwords colocadas não coincidem' });
        }
    }

    renderPasswordForm() {
        const { classes } = this.props;
        const { password, passwordConfirmation } = this.state;

        return (
            <Box component={Paper} p={2}>
                <Typography variant="h5" className={classes.title}>
                    {'Recuperação de conta:'}
                </Typography>
                <Typography variant="body1">
                    {'Insira a nova password a associar á sua conta:'}
                </Typography>
                <form className={classes.form} onSubmit={this.handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Nova password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={password}
                        onChange={this.handleInputChange}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="passwordConfirmation"
                        label="Confirmar Password"
                        type="password"
                        id="passwordConfirmation"
                        autoComplete="current-password"
                        value={passwordConfirmation}
                        onChange={this.handleInputChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submitBtn}
                    >
                        {'Alterar password'}
                    </Button>
                </form>
            </Box>
        );
    }

    renderPasswordChanged() {
        const { classes } = this.props;

        return (
            <Box component={Paper} p={2}>
                <Typography variant="h5" className={classes.title}>
                    {'A sua password foi alterada com sucesso'}
                </Typography>
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    component={Link}
                    to={'/login'}
                >
                    {'Ir para login'}
                </Button>
            </Box>
        );
    }

    render() {
        const { passwordChanged } = this.state;

        return (
            <Box display="flex" alignItems="center">
                <Container maxWidth="sm" disableGutters>
                    {passwordChanged
                        ? this.renderPasswordChanged()
                        : this.renderPasswordForm()}
                </Container>
            </Box>
        );
    }
}

RecoverPasswordVerified.propTypes = {
    classes: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecoverPasswordVerified);
