import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Alert, AlertTitle } from '@material-ui/lab';

import styles from '../../styles/Login';
import background from '../../img/login_background.jpg';
import { login } from '../../api/auth';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            errorMsg: null,
        };

        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeEmail(e) {
        this.setState({ email: e.target.value });
    }

    handleChangePassword(e) {
        this.setState({ password: e.target.value });
    }

    async handleSubmit(e) {
        e.preventDefault();

        const { email, password } = this.state;
        const response = await login({ email, password });

        if (response.status < 400) {
            localStorage.setItem('user', JSON.stringify(response.data.user));
            window.location.href = '/';
        } else {
            this.setState({ errorMsg: response.message });
        }
    }

    renderErrors() {
        const { classes } = this.props;
        const { errorMsg } = this.state;

        if (errorMsg) {
            return (
                <Alert severity="error" className={classes.errorMsg}>
                    <AlertTitle>Erro</AlertTitle>
                    {errorMsg}
                </Alert>
            );
        }
        return null;
    }

    render() {
        const { classes } = this.props;
        const { email, password } = this.state;

        return (
            <Grid container className={classes.root}>
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    style={{ backgroundImage: `url(${background})` }}
                    className={classes.image}
                />
                <Grid
                    item
                    xs={12}
                    sm={8}
                    md={5}
                    component={Paper}
                    elevation={6}
                    square
                >
                    <div className={classes.paper}>
                        <Typography component="h1" variant="h4">
                            {'Login'}
                        </Typography>
                        {this.renderErrors()}
                        <form
                            className={classes.form}
                            onSubmit={this.handleSubmit}
                        >
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                value={email}
                                onChange={this.handleChangeEmail}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                value={password}
                                onChange={this.handleChangePassword}
                            />
                            <Button
                                id="loginBtn"
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                {'Login'}
                            </Button>
                            <Grid container>
                                <Grid item xs>
                                    <Link href="/recover" variant="body2">
                                        {'Esqueceu a palavra-passe?'}
                                    </Link>
                                </Grid>
                                <Grid item>
                                    <Link href="/register" variant="body2">
                                        {'Não tem conta? Registe-se aqui'}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Grid>
            </Grid>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
