import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { verifyAuth } from '../../api/auth';

function withAuth(ComponentToProtect, role, redirectPath = '/') {
    return class extends Component {
        constructor() {
            super();
            this.state = {
                loading: true,
                redirect: false,
                user: null,
            };
        }

        async componentWillMount() {
            const response = await verifyAuth({ role });
            if (response.status !== 200) {
                this.setState({ loading: false, redirect: true });
                return;
            }
            this.setState({ loading: false, user: response.data });
        }

        render() {
            const { loading, redirect, user } = this.state;
            if (loading) {
                return null;
            }
            if (redirect) {
                return (
                    <Redirect
                        to={{
                            pathname: redirectPath,
                            state: { from: this.props.location },
                        }}
                    />
                );
            }
            return <ComponentToProtect {...this.props} user={user} />;
        }
    };
}

export default withAuth;
