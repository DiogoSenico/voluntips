import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { verifyAuth } from '../../api/auth';

function withoutAuth(ComponentToProtect) {
    return class extends Component {
        constructor() {
            super();
            this.state = {
                loading: true,
                redirect: false,
            };
        }

        async componentWillMount() {
            const response = await verifyAuth({ role: 'USER' });
            if (response.status !== 401) {
                this.setState({ loading: false, redirect: true });
                return;
            }
            this.setState({ loading: false });
        }

        render() {
            const { loading, redirect } = this.state;
            if (loading) {
                return null;
            }
            if (redirect) {
                return (
                    <Redirect
                        to={{
                            pathname: '/',
                            state: { from: this.props.location },
                        }}
                    />
                );
            }
            return <ComponentToProtect {...this.props} />;
        }
    };
}

export default withoutAuth;
