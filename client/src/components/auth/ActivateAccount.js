import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import styles from '../../styles/Login';
import { activateAccount } from '../../api/auth';

class ActivateAccount extends Component {
    constructor() {
        super();
        this.state = {
            success: false,
        };
    }

    async componentDidMount() {
        const { match } = this.props;

        const response = await activateAccount(match.params.token);
        if (response.status === 200) {
            this.setState({ success: true });
        } else {
            window.location.replace('/');
        }
    }

    render() {
        const { success } = this.state;
        return (
            <Box display="flex" alignItems="center" justifyContent="center">
                {success ? (
                    <Paper component={Box} p={2}>
                        <Typography variant="h4">
                            {'Conta ativada com sucesso'}
                        </Typography>
                    </Paper>
                ) : (
                    <CircularProgress />
                )}
            </Box>
        );
    }
}

ActivateAccount.propTypes = {
    classes: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

export default withStyles(styles)(ActivateAccount);
