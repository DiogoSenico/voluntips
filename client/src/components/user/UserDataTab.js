import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Chip from '@material-ui/core/Chip';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';

import styles from '../../styles/UserProfile';

class UserData extends Component {
    getMaxDate() {
        let d = new Date();
        d.setFullYear(d.getFullYear() - 10);
        return d;
    }

    renderInterestAreas() {
        const { handleAreasChange } = this.props;
        const { loadedInterestAreas, interestAreas } = this.props.info;

        return (
            <Autocomplete
                multiple
                filterSelectedOptions
                options={loadedInterestAreas}
                value={interestAreas}
                onChange={handleAreasChange}
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip
                            variant="outlined"
                            label={option}
                            {...getTagProps({ index })}
                        />
                    ))
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="outlined"
                        label="Áreas de interesse"
                    />
                )}
            />
        );
    }

    renderReasons() {
        const { handleReasonsChange } = this.props;
        const { loadedReasons, reasons } = this.props.info;

        return (
            <Autocomplete
                multiple
                freeSolo
                filterSelectedOptions
                options={loadedReasons}
                value={reasons}
                onChange={handleReasonsChange}
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip
                            variant="outlined"
                            label={option}
                            {...getTagProps({ index })}
                        />
                    ))
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="outlined"
                        label="Razões para ser voluntário"
                        helperText="Pode adicionar as suas próprias razões"
                    />
                )}
            />
        );
    }

    renderSchools() {
        const { handleSchoolChange } = this.props;
        const { loadedSchools, school } = this.props.info;

        return (
            <Autocomplete
                value={school}
                options={Object.keys(loadedSchools)}
                onChange={handleSchoolChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required
                        variant="outlined"
                        label="Escola"
                    />
                )}
            />
        );
    }

    renderCourses() {
        const { handleCourseChange } = this.props;
        const { loadedCourses, course } = this.props.info;

        return (
            <Autocomplete
                disabled={
                    !course && loadedCourses && loadedCourses.length === 0
                }
                options={loadedCourses || []}
                value={course}
                onChange={handleCourseChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required
                        variant="outlined"
                        label="Curso"
                    />
                )}
            />
        );
    }

    renderServices() {
        const { handleServiceChange } = this.props;
        const { loadedServices, service } = this.props.info;

        return (
            <Autocomplete
                options={loadedServices}
                value={service}
                onChange={handleServiceChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required
                        variant="outlined"
                        label="Serviço"
                    />
                )}
            />
        );
    }

    renderEntity() {
        const { handleInputChange } = this.props;
        const { entity } = this.props.info;

        return (
            <TextField
                required
                id="entity"
                name="entity"
                label="Nome da entidade"
                type="text"
                fullWidth
                variant="outlined"
                value={entity}
                onChange={handleInputChange}
            />
        );
    }

    renderAdminDataEdit() {
        const { memberType } = this.props.info;

        switch (memberType) {
            case 'STUDENT':
            case 'GRATUATE':
                return (
                    <>
                        <Grid item xs={12} md={6}>
                            {this.renderSchools()}
                        </Grid>
                        <Grid item xs={12} md={6}>
                            {this.renderCourses()}
                        </Grid>
                    </>
                );
            case 'NON_TEACHER':
            case 'SCHOLAR':
                return (
                    <>
                        <Grid item xs={12}>
                            {this.renderServices()}
                        </Grid>
                    </>
                );
            case 'TEACHER':
                return (
                    <>
                        <Grid item xs={12}>
                            {this.renderSchools()}
                        </Grid>
                    </>
                );
            case 'ENTITY':
                return (
                    <>
                        <Grid item xs={12}>
                            {this.renderEntity()}
                        </Grid>
                    </>
                );
            default:
                return null;
        }
    }

    renderAdminDataRead() {
        const { memberType, school, course, service, entity } = this.props.info;

        switch (memberType) {
            case 'STUDENT':
            case 'GRATUATE':
                return (
                    <>
                        <Grid item xs={12} md={6}>
                            <Typography variant="body1">Escola:</Typography>
                            <Typography variant="body2"> {school} </Typography>
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <Typography variant="body1"> Curso: </Typography>
                            <Typography variant="body2"> {course} </Typography>
                        </Grid>
                    </>
                );
            case 'NON_TEACHER':
            case 'SCHOLAR':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="body1"> Serviço: </Typography>
                            <Typography variant="body2"> {service} </Typography>
                        </Grid>
                    </>
                );
            case 'TEACHER':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="body1"> Escola: </Typography>
                            <Typography variant="body2"> {school} </Typography>
                        </Grid>
                    </>
                );
            case 'ENTITY':
                return (
                    <>
                        <Grid item xs={12}>
                            <Typography variant="body1"> Entidade: </Typography>
                            <Typography variant="body2"> {entity} </Typography>
                        </Grid>
                    </>
                );
            default:
                return null;
        }
    }

    renderEditMode() {
        const { handleChangeInput, handleDateChange } = this.props;
        const { name, email, phone, address, birthdate } = this.props.info;

        return (
            <>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        fullWidth
                        variant="outlined"
                        label="Nome"
                        name="name"
                        autoComplete="fname"
                        value={name}
                        onChange={handleChangeInput}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <KeyboardDatePicker
                            required
                            fullWidth
                            disableFuture
                            openTo="year"
                            variant="inline"
                            inputVariant="outlined"
                            orientation="landscape"
                            format="DD/MM/YYYY"
                            maxDate={this.getMaxDate()}
                            minDateMessage="Data inválida (demasiado antiga)"
                            maxDateMessage="Data inválida (demasiado no presente/futuro)"
                            id="date-picker-inline"
                            label="Data de nascimento"
                            value={birthdate}
                            views={['year', 'month', 'date']}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        InputProps={{ readOnly: true }}
                        fullWidth
                        variant="outlined"
                        label="Email"
                        name="email"
                        autoComplete="email"
                        value={email}
                        onChange={handleChangeInput}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="phone"
                        name="phone"
                        label="Telemóvel"
                        type="tel"
                        variant="outlined"
                        fullWidth
                        inputProps={{
                            pattern: '[0-9]{3}[0-9]{3}[0-9]{3}',
                            maxLength: '9',
                            title: 'Exemplo: 999999999',
                        }}
                        autoComplete="tel-national"
                        value={phone}
                        onChange={handleChangeInput}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        required
                        fullWidth
                        variant="outlined"
                        label="Morada"
                        name="address"
                        autoComplete="address"
                        value={address}
                        onChange={handleChangeInput}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderInterestAreas()}
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderReasons()}
                </Grid>
                {this.renderAdminDataEdit()}
            </>
        );
    }

    renderReadMode() {
        const {
            name,
            email,
            phone,
            address,
            birthdate,
            interestAreas,
            reasons,
        } = this.props.info;

        return (
            <>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1"> Name: </Typography>
                    <Typography variant="body2"> {name} </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1">
                        {'Data de nascimento'}
                    </Typography>
                    <Typography variant="body2">
                        {moment(birthdate).format('DD/MM/YYYY')}
                    </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1"> Email: </Typography>
                    <Typography variant="body2"> {email} </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1"> Telémovel: </Typography>
                    <Typography variant="body2"> {phone} </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body1"> Morada: </Typography>
                    <Typography variant="body2"> {address} </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1">
                        {'Áreas de interesse:'}
                    </Typography>
                    {interestAreas.map((area, index) => (
                        <Typography key={index} variant="body2">
                            • {area}
                        </Typography>
                    ))}
                </Grid>
                <Grid item xs={12} md={6}>
                    <Typography variant="body1">{'Razões:'}</Typography>
                    {reasons.map((reason, index) => (
                        <Typography key={index} variant="body2">
                            • {reason}
                        </Typography>
                    ))}
                </Grid>
                {this.renderAdminDataRead()}
            </>
        );
    }

    renderEditOptions() {
        const { handleChangeMode } = this.props;
        const { editMode, ownProfile } = this.props.info;

        if (ownProfile) {
            return (
                <Grid
                    item
                    xs={12}
                    component={Box}
                    display="flex"
                    flexDirection="row-reverse"
                >
                    {editMode ? (
                        <Box>
                            <Button
                                variant="contained"
                                color="secondary"
                                style={{ marginRight: 10 }}
                                type="submit"
                            >
                                Salvar
                            </Button>
                            <Button
                                variant="contained"
                                color="secondary"
                                onClick={handleChangeMode}
                            >
                                Cancelar
                            </Button>
                        </Box>
                    ) : (
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleChangeMode}
                            style={{ alignSelf: 'flex-end' }}
                        >
                            Edit
                        </Button>
                    )}
                </Grid>
            );
        }
        return null;
    }

    render() {
        const { editMode } = this.props.info;
        const { handleSaveChanges } = this.props;

        return (
            <form onSubmit={handleSaveChanges}>
                <Grid container spacing={2}>
                    {this.renderEditOptions()}
                    {editMode ? this.renderEditMode() : this.renderReadMode()}
                </Grid>
            </form>
        );
    }
}

UserData.propTypes = {
    info: PropTypes.object.isRequired,
    handleChangeMode: PropTypes.func.isRequired,
    handleChangeInput: PropTypes.func.isRequired,
    handleDateChange: PropTypes.func.isRequired,
    handleAreasChange: PropTypes.func.isRequired,
    handleSchoolChange: PropTypes.func.isRequired,
    handleCourseChange: PropTypes.func.isRequired,
    handleServiceChange: PropTypes.func.isRequired,
    handleSaveChanges: PropTypes.func.isRequired,
};

export default withStyles(styles)(UserData);
