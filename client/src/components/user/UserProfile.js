import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { Alert, AlertTitle } from '@material-ui/lab';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import UserDefaultImage from '../../img/user_default_image.webp';
import styles from '../../styles/UserProfile';

import {
    getConstants,
    getUserProfile,
    updateUser,
    uploadProfileImage,
    activateUser,
    deactivateUser,
    acceptUser,
    rejectUser,
} from '../../api/user';
import UserDataTab from './UserDataTab';
import UserHistoryTab from './UserHistoryTab';
import UserApplicationsTab from './UserApplicationsTab';
import UserStatsTab from './UserStatsTab';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Paper variant="outlined" square component={Box} p={2}>
                    {children}
                </Paper>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

class UserProfile extends Component {
    constructor() {
        super();
        this.state = {
            _id: '',
            name: '',
            image: UserDefaultImage,
            email: '',
            phone: '',
            birthdate: '',
            address: '',
            accountStatus: '',
            activationToken: '',
            isExternal: false,
            role: '',

            memberType: '',
            interestAreas: [],
            reasons: [],
            entity: '',
            school: null,
            course: null,
            service: null,

            ownProfile: false,
            currentTab: 0,

            loadedInterestAreas: [],
            loadedReasons: [],
            loadedServices: [],
            loadedSchools: {},
            loadedCourses: [],
        };

        this.handleChangeMode = this.handleChangeMode.bind(this);
        this.handleImageFile = this.handleImageFile.bind(this);

        this.handleAcceptUser = this.handleAcceptUser.bind(this);
        this.handleRejectUser = this.handleRejectUser.bind(this);
        this.handleSaveChanges = this.handleSaveChanges.bind(this);
        this.handleActivateUser = this.handleActivateUser.bind(this);
        this.handleDeactivateUser = this.handleDeactivateUser.bind(this);

        this.handleChangeTab = this.handleChangeTab.bind(this);
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleSchoolChange = this.handleSchoolChange.bind(this);
        this.handleCourseChange = this.handleCourseChange.bind(this);
        this.handleServiceChange = this.handleServiceChange.bind(this);
        this.handleAreasChange = this.handleAreasChange.bind(this);
        this.handleReasonsChange = this.handleReasonsChange.bind(this);
    }

    componentDidMount() {
        this.getConstants();
        this.getProfileInfo();
    }

    async getConstants() {
        const loadedMemberTypes = (await getConstants('memberTypes')).data;
        const loadedInterestAreas = (await getConstants('interestAreas')).data;
        const loadedReasons = (await getConstants('reasons')).data;
        const loadedServices = (await getConstants('services')).data;
        const loadedSchools = (await getConstants('schoolCourse')).data;

        this.setState({
            loadedMemberTypes,
            loadedInterestAreas,
            loadedReasons,
            loadedServices,
            loadedSchools,
        });
    }

    async getProfileInfo() {
        const { match } = this.props;
        const response = await getUserProfile(match.params.id);

        if (response.status === 200) {
            this.setState({ ...response.data }, this.isOwnProfile);
        } else {
            window.location.replace('/');
        }
    }

    isOwnProfile() {
        const { user } = this.props;
        const { _id } = this.state;
        this.setState({ ownProfile: _id === user._id });
    }

    async handleImageFile(e) {
        const { _id } = this.state;

        let file = e.target.files[0];

        if (file) {
            let data = new FormData();
            data.append('file', file);

            const responseUpload = await uploadProfileImage(_id, data);
            if (responseUpload.status === 200) {
                this.setState({ image: responseUpload.data.filePath });
            }
        }
    }

    async handleActivateUser() {
        const { _id } = this.state;
        const response = await activateUser(_id);

        if (response.status === 200) {
            this.setState({ ...response.data });
        }
    }

    async handleDeactivateUser() {
        const { _id } = this.state;
        const response = await deactivateUser(_id);

        if (response.status === 200) {
            this.setState({ ...response.data });
        }
    }

    async handleAcceptUser() {
        const { _id } = this.state;
        const response = await acceptUser(_id);

        if (response.status === 200) {
            this.setState({ ...response.data });
        }
    }

    async handleRejectUser() {
        const { _id } = this.state;
        const response = await rejectUser(_id);

        if (response.status === 200) {
            this.setState({ ...response.data });
        }
    }

    handleImageError(e) {
        e.target.onerror = null;
        e.target.src = UserDefaultImage;
    }

    handleChangeMode() {
        this.setState(prevState => ({ editMode: !prevState.editMode }));
    }

    handleChangeInput(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleDateChange(birthdate) {
        this.setState({ birthdate });
    }

    handleAreasChange(e, interestAreas) {
        this.setState({ interestAreas });
    }

    handleReasonsChange(e, reasons) {
        this.setState({ reasons });
    }

    handleSchoolChange(e, school) {
        const { loadedSchools } = this.state;
        this.setState({
            loadedCourses: loadedSchools[school],
            school,
            course: null,
        });
    }

    handleCourseChange(e, course) {
        this.setState({ course });
    }

    handleServiceChange(e, service) {
        this.setState({ service });
    }

    handleChangeTab(e, currentTab) {
        this.setState({ currentTab });
    }

    getUserData() {
        const {
            _id,
            name,
            image,
            email,
            phone,
            birthdate,
            address,
            interestAreas,
            reasons,
            entity,
            school,
            course,
            service,
        } = this.state;

        return {
            _id,
            name,
            image,
            email,
            phone,
            birthdate,
            address,
            interestAreas,
            reasons,
            entity,
            school,
            course,
            service,
        };
    }

    async handleSaveChanges(e) {
        e.preventDefault();
        const user = this.getUserData();

        const response = await updateUser(user);

        if (response.status === 200) {
            this.handleChangeMode();
        }
    }

    renderStatus() {
        const { user } = this.props;
        const { accountStatus, ownProfile } = this.state;

        if (user.role !== 'USER' && !ownProfile) {
            switch (accountStatus) {
                case 'PENDING':
                    return (
                        <Grid item xs={12}>
                            <Box
                                display="flex"
                                component={Paper}
                                variant="outlined"
                                p={2}
                                style={{
                                    backgroundColor: 'rgb(255, 244, 229)',
                                }}
                            >
                                <Box flexGrow={1}>
                                    <Typography variant="h5">
                                        {'Utilizador pendente'}
                                    </Typography>
                                </Box>
                                <Box display="flex" flexDirection="row-reverse">
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={this.handleRejectUser}
                                    >
                                        {'Recusar utilizador'}
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={this.handleAcceptUser}
                                        style={{ marginRight: 10 }}
                                    >
                                        {'Aceitar utilizador'}
                                    </Button>
                                </Box>
                            </Box>
                        </Grid>
                    );
                case 'ACTIVE':
                    return (
                        <Grid item xs={12}>
                            <Alert
                                severity="info"
                                action={
                                    <Button
                                        color="inherit"
                                        size="small"
                                        onClick={this.handleDeactivateUser}
                                    >
                                        {'Desativar conta'}
                                    </Button>
                                }
                            >
                                <AlertTitle>Estado da conta</AlertTitle>
                                Esta conta encontra-se <strong>ativa</strong>.
                            </Alert>
                        </Grid>
                    );
                case 'INACTIVE':
                    return (
                        <Grid item xs={12}>
                            <Alert
                                severity="info"
                                action={
                                    <Button
                                        color="inherit"
                                        size="small"
                                        onClick={this.handleActivateUser}
                                    >
                                        {'Ativar conta'}
                                    </Button>
                                }
                            >
                                <AlertTitle>Estado da conta</AlertTitle>
                                Esta conta encontra-se <strong>inativa</strong>.
                            </Alert>
                        </Grid>
                    );
                default:
                    return null;
            }
        }
        return null;
    }

    render() {
        const { classes } = this.props;
        const { name, image, editMode, currentTab } = this.state;
        return (
            <Container component={Box} pt={3}>
                <Grid container spacing={2}>
                    {this.renderStatus()}
                    <Grid item xs={12} sm={4} md={3}>
                        <Paper variant="outlined" square component={Box} p={2}>
                            {editMode ? (
                                <Box display="flex" flexDirection="row-reverse">
                                    <input
                                        accept="image/*"
                                        style={{ display: 'none' }}
                                        id="icon-button-file"
                                        name="file"
                                        type="file"
                                        onChange={this.handleImageFile}
                                    />
                                    <label htmlFor="icon-button-file">
                                        <IconButton
                                            color="secondary"
                                            aria-label="upload picture"
                                            component="span"
                                        >
                                            <PhotoCamera />
                                        </IconButton>
                                    </label>
                                </Box>
                            ) : null}
                            <Box
                                display="flex"
                                flexDirection="column"
                                alignItems="center"
                            >
                                <img
                                    src={image}
                                    className={classes.profileImage}
                                    alt="Profile"
                                    id="profile-image"
                                    onError={this.handleImageError}
                                />
                                <Typography variant="h5"> {name} </Typography>
                            </Box>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={8} md={9}>
                        <AppBar position="static">
                            <Tabs
                                value={currentTab}
                                onChange={this.handleChangeTab}
                                variant="scrollable"
                            >
                                <Tab label="Dados pessoais" {...a11yProps(0)} />
                                <Tab
                                    label="Inscrições em voluntariados"
                                    {...a11yProps(1)}
                                />
                                <Tab
                                    label="Sugestões realizadas"
                                    {...a11yProps(2)}
                                />
                                <Tab label="Estatisticas" {...a11yProps(3)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel value={currentTab} index={0}>
                            <UserDataTab
                                info={this.state}
                                handleChangeMode={this.handleChangeMode}
                                handleChangeInput={this.handleChangeInput}
                                handleDateChange={this.handleDateChange}
                                handleSchoolChange={this.handleSchoolChange}
                                handleCourseChange={this.handleCourseChange}
                                handleServiceChange={this.handleServiceChange}
                                handleAreasChange={this.handleAreasChange}
                                handleReasonsChange={this.handleReasonsChange}
                                handleSaveChanges={this.handleSaveChanges}
                            />
                        </TabPanel>
                        <TabPanel value={currentTab} index={1}>
                            <UserApplicationsTab user={this.state} />
                        </TabPanel>
                        <TabPanel value={currentTab} index={2}>
                            <UserHistoryTab user={this.state} />
                        </TabPanel>
                        <TabPanel value={currentTab} index={3}>
                            <UserStatsTab user={this.state} />
                        </TabPanel>
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

UserProfile.propTypes = {
    classes: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserProfile);
