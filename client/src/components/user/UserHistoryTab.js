import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CircularProgress, Box, Tooltip } from '@material-ui/core';
import MaterialTable from 'material-table';
import { withStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

import { getUserVolunteerings } from '../../api/user';
import styles from '../../styles/UserProfile';

class UserHistoryTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            volunteerings: [],
        };
    }

    componentDidMount() {
        this.getVolunteerings();
    }

    async getVolunteerings() {
        const { user } = this.props;
        const response = await getUserVolunteerings(user._id);
        if (response.status === 200) {
            this.setState({ loading: false, volunteerings: response.data });
        }
    }

    renderLoading() {
        return (
            <Box display="flex" alignItems="center" justifyContent="center">
                <CircularProgress color="secondary" />
            </Box>
        );
    }

    render() {
        const { history } = this.props;
        const { loading, volunteerings } = this.state;

        const columns = [
            { title: 'Titulo', field: 'title' },
            {
                title: 'Interno/Externo',
                field: 'isExternal',
                render: (rowData) =>
                    rowData.isExternal ? 'Externo' : 'Interno',
            },
            { title: 'Entidade', field: 'entity' },
            {
                title: 'Estado',
                field: 'status',
                headerStyle: { textAlign: 'center' },
                cellStyle: { textAlign: 'center' },
                render: (rowData) => {
                    switch (rowData.status) {
                        case 'ACCEPTED':
                            return (
                                <Tooltip title="Ativo">
                                    <span>
                                        <CheckIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'REJECTED':
                            return (
                                <Tooltip title="Inativo">
                                    <span>
                                        <CloseIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'PENDDING':
                            return (
                                <Tooltip title="Pendente">
                                    <span>
                                        <HourglassEmptyIcon />
                                    </span>
                                </Tooltip>
                            );
                        default:
                            return null;
                    }
                },
            },
        ];

        return loading ? (
            this.renderLoading()
        ) : (
            <MaterialTable
                title="Histórico"
                columns={columns}
                components={{
                    Container: (props) => <div {...props} />,
                }}
                options={{
                    pageSize: 15,
                    pageSizeOptions: [15, 30, 50],
                    draggable: false,
                    emptyRowsWhenPaging: false,
                    showTitle: false,
                }}
                localization={{
                    toolbar: {
                        searchTooltip: 'Pesquisar',
                        searchPlaceholder: 'Pesquisar',
                    },
                    body: {
                        emptyDataSourceMessage:
                            'Não foram encontrados registos',
                    },
                    pagination: {
                        labelDisplayedRows: '{from}-{to} de {count}',
                        labelRowsSelect: 'registos',
                    },
                }}
                data={volunteerings}
                onRowClick={(event, rowData) =>
                    history.push(`/project/${rowData._id}`)
                }
            />
        );
    }
}

UserHistoryTab.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(UserHistoryTab));
