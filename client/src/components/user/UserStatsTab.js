import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
    Grid,
    Paper,
    Box,
    Typography,
    CircularProgress,
    Divider,
} from '@material-ui/core';
import Chart from 'react-google-charts';
import moment from 'moment';

import { getUserApplications } from '../../api/user';
import styles from '../../styles/UserProfile';

class UserStats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            applications: [],
        };
    }

    componentDidMount() {
        this.getApplications();
    }

    async getApplications() {
        const { user } = this.props;
        const response = await getUserApplications(user._id);
        if (response.status === 200) {
            this.setState({ loading: false, applications: response.data });
        }
    }

    renderLoading() {
        return (
            <Box display="flex" alignItems="center" justifyContent="center">
                <CircularProgress color="secondary" />
            </Box>
        );
    }

    render() {
        const { loading, applications } = this.state;

        const acceptedApplications = applications.filter(
            (application) => application.userApplication.status === 'ACCEPTED',
        );
        const rejectedApplications = applications.filter(
            (application) => application.userApplication.status === 'REJECTED',
        );
        const pendingAplications = applications.filter(
            (application) => application.userApplication.status === 'PENDDING',
        );

        let applicationGraphData = {};
        applications.forEach((application) => {
            const date = moment(application.userApplication.createdAt).format(
                'MM/YY',
            );
            applicationGraphData[date]
                ? (applicationGraphData[date] += 1)
                : (applicationGraphData[date] = 1);
        });
        applicationGraphData = Object.entries(applicationGraphData);

        if (loading) {
            return this.renderLoading();
        }

        return (
            <Grid container spacing={2}>
                <Grid item xs={12} sm={4}>
                    <Paper
                        variant="outlined"
                        square
                        component={Box}
                        p={2}
                        style={{
                            backgroundColor: 'rgb(237, 247, 237)',
                        }}
                    >
                        <Typography variant="body1">
                            Inscrições aceites
                        </Typography>
                        <Typography variant="h6">
                            {acceptedApplications.length}
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Paper
                        variant="outlined"
                        square
                        component={Box}
                        p={2}
                        style={{
                            backgroundColor: 'rgb(253, 236, 234)',
                        }}
                    >
                        <Typography variant="body1">
                            Inscrições rejeitadas
                        </Typography>
                        <Typography variant="h6">
                            {rejectedApplications.length}
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Paper
                        variant="outlined"
                        square
                        component={Box}
                        p={2}
                        style={{
                            backgroundColor: 'rgb(255, 244, 229)',
                        }}
                    >
                        <Typography variant="body1">
                            Inscrições pendentes
                        </Typography>
                        <Typography variant="h6">
                            {pendingAplications.length}
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Paper variant="outlined" square>
                        <Typography variant="h5" component={Box} p={2}>
                            {'Inscrições realizadas'}
                        </Typography>
                        <Divider />
                        <Box p={2}>
                            {applicationGraphData.length > 0 && (
                                <Chart
                                    height={'400px'}
                                    chartType="LineChart"
                                    loader={this.renderLoading()}
                                    data={[
                                        ['x', 'Inscrições'],
                                        ...applicationGraphData,
                                    ]}
                                    options={{
                                        vAxis: {
                                            format: '0',
                                        },
                                    }}
                                />
                            )}
                        </Box>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

UserStats.propTypes = {
    classes: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    stats: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserStats);
