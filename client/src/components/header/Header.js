import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';

import UserDropdown from './UserDropdown';
import { verifyAuth, logout } from '../../api/auth';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
        };

        this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }

    async componentDidMount() {
        const response = await verifyAuth({ role: 'USER' });
        if (response.status === 200) {
            this.setState({ user: response.data });
        }
    }

    async handleLogoutClick() {
        const response = await logout();
        if (response.status !== 200) return;
        window.location.replace('/');
    }

    renderManagementOptions() {
        const { user } = this.state;

        if (user.role !== 'USER') {
            return (
                <Button
                    id="managementBtn"
                    color="inherit"
                    component={Link}
                    to={`/management`}
                    variant="outlined"
                    style={{ marginRight: 10 }}
                >
                    Gestão
                </Button>
            );
        }
        return null;
    }

    render() {
        const { user } = this.state;
        return (
            <AppBar position="static">
                <Toolbar>
                    <Typography
                        variant="h6"
                        onClick={() => {
                            window.location.replace('/');
                        }}
                        style={{ cursor: 'pointer' }}
                    >
                        {'VoluntIPS'}
                    </Typography>
                    {user ? (
                        <Box
                            display="flex"
                            flexDirection="row-reverse"
                            alignItems="center"
                            flexGrow={1}
                        >
                            <UserDropdown user={user} />
                            {this.renderManagementOptions()}
                        </Box>
                    ) : (
                        <Box
                            display="flex"
                            flexDirection="row-reverse"
                            alignItems="center"
                            flexGrow={1}
                        >
                            <Button
                                variant="outlined"
                                color="inherit"
                                component={Link}
                                to={'/login'}
                            >
                                {'Login'}
                            </Button>
                            <Button
                                variant="outlined"
                                color="inherit"
                                component={Link}
                                to={'/register'}
                                style={{ marginRight: 10 }}
                            >
                                {'Registar'}
                            </Button>
                        </Box>
                    )}
                </Toolbar>
            </AppBar>
        );
    }
}

export default Header;
