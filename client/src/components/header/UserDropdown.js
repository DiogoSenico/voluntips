import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
    Paper,
    ClickAwayListener,
    Grow,
    Avatar,
    IconButton,
    List,
    ListItem,
    ListItemText,
    Typography,
    Box,
    Divider,
} from '@material-ui/core';

import styles from '../../styles/UserDropdown';
import { logout } from '../../api/auth';

class UserDropdown extends Component {
    constructor() {
        super();

        this.state = {
            open: false,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleClickAway = this.handleClickAway.bind(this);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
        this.handleMyProfileClick = this.handleMyProfileClick.bind(this);
    }

    handleClick() {
        const { open } = this.state;

        this.setState({ open: !open });
    }

    handleClickAway(e) {
        this.setState({ open: false });
    }

    handleMyProfileClick() {
        const { user } = this.props;

        window.location.replace(`/user/${user._id}`);
    }

    async handleLogoutClick() {
        const response = await logout();
        if (response.status !== 200) return;
        window.location.replace('/');
    }

    renderDropdownContent() {
        const { user, classes } = this.props;
        const { open } = this.state;

        const options = [
            {
                title: 'Meu perfil',
                onClick: this.handleMyProfileClick,
            },
            {
                title: 'Logout',
                onClick: this.handleLogoutClick,
            },
        ];

        return (
            <Grow in={open} style={{ transformOrigin: 'right top' }}>
                <Paper variant="outlined" className={classes.paper}>
                    <Box p={1}>
                        <Typography variant="body1" align="center">
                            {user.name}
                        </Typography>
                    </Box>
                    <Divider />
                    <List dense>
                        {options.map((op, index) => (
                            <ListItem key={index} button onClick={op.onClick}>
                                <ListItemText primary={op.title} />
                            </ListItem>
                        ))}
                    </List>
                </Paper>
            </Grow>
        );
    }

    render() {
        const { user, classes } = this.props;

        return (
            <ClickAwayListener onClickAway={this.handleClickAway}>
                <div className={classes.wrapper}>
                    <IconButton size="small" onClick={this.handleClick}>
                        <Avatar alt={user.name} src={user.image} />
                    </IconButton>
                    {this.renderDropdownContent()}
                </div>
            </ClickAwayListener>
        );
    }
}

UserDropdown.propTypes = {
    user: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserDropdown);
