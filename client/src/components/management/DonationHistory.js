import React, { Component } from 'react';
import MaterialTable from 'material-table';
import moment from 'moment';

import { getDonationsActs } from '../../api/donation';

class DonationHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            donations: [],
            loading: true,
        };
    }

    componentDidMount() {
        this.getDonationActs();
    }

    async getDonationActs() {
        const response = await getDonationsActs(0, 1000, '');
        this.setState({ donations: response.data.data, loading: false });
    }

    render() {
        const { loading, donations } = this.state;

        const columns = [
            { title: 'Doador', field: 'donator.name' },
            {
                title: 'Data',
                field: 'created_at',
                render: (rowData) =>
                    moment(rowData.created_at).format('DD/MM/YYYY HH:mm'),
            },
            { title: 'Registado por', field: 'registeredBy.name' },
            {
                title: 'Quantidade',
                render: (rowData) =>
                    rowData.products
                        .map((item) => item.amount)
                        .reduce((prev, curr) => prev + curr, 0),
            },
        ];

        return (
            <div>
                <MaterialTable
                    title="Histórico de doações"
                    columns={columns}
                    options={{
                        exportButton: true,
                        exportDelimiter: ';',
                        actionsColumnIndex: -1,
                        pageSize: 15,
                        pageSizeOptions: [15, 30, 50],
                        draggable: false,
                        emptyRowsWhenPaging: false,
                    }}
                    localization={{
                        toolbar: {
                            exportName: 'Exportar como CSV',
                            exportTitle: 'Exportar',
                            exportAriaLabel: 'Exportar',
                            searchTooltip: 'Pesquisar',
                            searchPlaceholder: 'Pesquisar',
                        },
                        body: {
                            emptyDataSourceMessage:
                                'Não foram encontrados registos',
                        },
                        pagination: {
                            labelDisplayedRows: '{from}-{to} de {count}',
                            labelRowsSelect: 'registos',
                        },
                    }}
                    data={donations}
                    isLoading={loading}
                />
            </div>
        );
    }
}

export default DonationHistory;
