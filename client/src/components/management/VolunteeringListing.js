import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MaterialTable from 'material-table';
import { withRouter, Link } from 'react-router-dom';
import { Button, Box, Tooltip } from '@material-ui/core';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

import { getVolunteerings } from '../../api/volunteering';

class ProjectListing extends Component {
    getVolunteerings(query) {
        return new Promise(async (resolve, reject) => {
            const response = await getVolunteerings(
                query.page,
                query.pageSize,
                query.search
            );
            if (!response.data) return resolve();
            resolve({
                data: response.data.data,
                page: response.data.page,
                totalCount: response.data.totalCount,
            });
        });
    }

    render() {
        const { history } = this.props;

        const columns = [
            { title: 'Titulo', field: 'title' },
            { title: 'Entidade', field: 'entity' },
            {
                title: 'Estado',
                field: 'status',
                headerStyle: { textAlign: 'center' },
                cellStyle: { textAlign: 'center' },
                render: (rowData) => {
                    switch (rowData.status) {
                        case 'ACCEPTED':
                            return (
                                <Tooltip title="Aceite">
                                    <span>
                                        <CheckIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'REJECTED':
                            return (
                                <Tooltip title="Rejeitado">
                                    <span>
                                        <CloseIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'PENDDING':
                            return (
                                <Tooltip title="Pendente">
                                    <span>
                                        <HourglassEmptyIcon />
                                    </span>
                                </Tooltip>
                            );
                        default:
                            return null;
                    }
                },
            },
        ];

        return (
            <div>
                <Box display="flex" flexDirection="row-reverse" mb={2}>
                    <Button
                        variant="contained"
                        color="secondary"
                        component={Link}
                        to={'/project/create'}
                    >
                        {'Criar voluntariado'}
                    </Button>
                </Box>
                <MaterialTable
                    title="Listagem de voluntariados"
                    columns={columns}
                    options={{
                        exportButton: true,
                        exportDelimiter: ';',
                        actionsColumnIndex: -1,
                        pageSize: 15,
                        pageSizeOptions: [15, 30, 50],
                        sorting: false,
                        draggable: false,
                        emptyRowsWhenPaging: false,
                    }}
                    localization={{
                        toolbar: {
                            exportName: 'Exportar como CSV',
                            exportTitle: 'Exportar',
                            exportAriaLabel: 'Exportar',
                            searchTooltip: 'Pesquisar',
                            searchPlaceholder: 'Pesquisar',
                        },
                        body: {
                            emptyDataSourceMessage:
                                'Não foram encontrados registos',
                        },
                        pagination: {
                            labelDisplayedRows: '{from}-{to} de {count}',
                            labelRowsSelect: 'registos',
                        },
                    }}
                    data={this.getVolunteerings}
                    onRowClick={(event, rowData) =>
                        history.push(`/project/${rowData._id}`)
                    }
                />
            </div>
        );
    }
}

ProjectListing.propTypes = {
    history: PropTypes.object.isRequired,
};

export default withRouter(ProjectListing);
