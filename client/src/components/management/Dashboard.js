import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import MaterialTable from 'material-table';
import { withRouter } from 'react-router-dom';
import {
    Tooltip,
    Grid,
    Paper,
    Typography,
    Box,
    CircularProgress,
    Divider,
} from '@material-ui/core';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import Chart from 'react-google-charts';
import moment from 'moment';

import { getUsers } from '../../api/user';
import { getVolunteerings } from '../../api/volunteering';

import styles from '../../styles/Dashboard';

class MainPage extends Component {
    constructor() {
        super();
        this.state = {
            loadingUsers: true,
            loadingVolunteerings: true,

            users: [],
            volunteerings: [],
        };
    }

    componentDidMount() {
        this.getUsers();
        this.getVolunteerings();
    }

    async getUsers() {
        const response = await getUsers('USER', 0, 1000, '');
        if (response.status === 200) {
            this.setState({ loadingUsers: false, users: response.data.data });
        }
    }

    async getVolunteerings() {
        const response = await getVolunteerings(0, 1000, '');
        if (response.status === 200) {
            this.setState({
                loadingVolunteerings: false,
                volunteerings: response.data.data,
            });
        }
    }

    renderLoading() {
        return (
            <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                height="300px"
            >
                <CircularProgress color="secondary" />
            </Box>
        );
    }

    renderUsersTable() {
        const { history } = this.props;
        const { loadingUsers, users } = this.state;

        const columns = [
            { title: 'Nome', field: 'name' },
            { title: 'Tipo', field: 'memberType' },
            {
                title: 'Interno/Externo',
                field: 'isExternal',
                render: (rowData) =>
                    rowData.isExternal ? 'Externo' : 'Interno',
            },
            {
                title: 'Estado',
                field: 'accountStatus',
                headerStyle: { textAlign: 'right' },
                cellStyle: { textAlign: 'right' },
                render: (rowData) => {
                    switch (rowData.accountStatus) {
                        case 'ACTIVE':
                            return (
                                <Tooltip title="Ativo">
                                    <span>
                                        <CheckIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'INACTIVE':
                            return (
                                <Tooltip title="Inativo">
                                    <span>
                                        <CloseIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'PENDING':
                            return (
                                <Tooltip title="Pendente">
                                    <span>
                                        <HourglassEmptyIcon />
                                    </span>
                                </Tooltip>
                            );
                        default:
                            return null;
                    }
                },
            },
        ];

        return (
            <MaterialTable
                title="Utilizadores pendentes"
                columns={columns}
                components={{
                    Container: (props) => (
                        <Paper variant="outlined" square {...props} />
                    ),
                }}
                options={{
                    draggable: false,
                    search: false,
                }}
                localization={{
                    body: {
                        emptyDataSourceMessage:
                            'Não foram encontrados registos',
                    },
                    pagination: {
                        labelDisplayedRows: '{from}-{to} de {count}',
                        labelRowsSelect: 'registos',
                    },
                }}
                isLoading={loadingUsers}
                data={users.filter((user) => user.accountStatus === 'PENDING')}
                onRowClick={(event, rowData) =>
                    history.push(`/user/${rowData._id}`)
                }
            />
        );
    }

    renderVolunteeringsTable() {
        const { history } = this.props;
        const { loadingVolunteerings, volunteerings } = this.state;

        const columns = [
            { title: 'Titulo', field: 'title' },
            { title: 'Entidade', field: 'entity' },
            {
                title: 'Estado',
                field: 'status',
                headerStyle: { textAlign: 'center' },
                cellStyle: { textAlign: 'center' },
                render: (rowData) => {
                    switch (rowData.status) {
                        case 'ACCEPTED':
                            return (
                                <Tooltip title="Aceite">
                                    <span>
                                        <CheckIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'REJECTED':
                            return (
                                <Tooltip title="Rejeitado">
                                    <span>
                                        <CloseIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'PENDDING':
                            return (
                                <Tooltip title="Pendente">
                                    <span>
                                        <HourglassEmptyIcon />
                                    </span>
                                </Tooltip>
                            );
                        default:
                            return null;
                    }
                },
            },
        ];

        return (
            <MaterialTable
                style={{ height: '100%' }}
                title="Voluntariados pendentes"
                columns={columns}
                components={{
                    Container: (props) => (
                        <Paper variant="outlined" square {...props} />
                    ),
                }}
                options={{
                    draggable: false,
                    search: false,
                }}
                localization={{
                    body: {
                        emptyDataSourceMessage:
                            'Não foram encontrados registos',
                    },
                    pagination: {
                        labelDisplayedRows: '{from}-{to} de {count}',
                        labelRowsSelect: 'registos',
                    },
                }}
                isLoading={loadingVolunteerings}
                data={volunteerings.filter(
                    (volunteering) => volunteering.status === 'PENDDING',
                )}
                onRowClick={(event, rowData) =>
                    history.push(`/project/${rowData._id}`)
                }
            />
        );
    }

    renderApplicationsMade() {
        const { loadingVolunteerings, volunteerings } = this.state;

        let applications = 0;
        if (!loadingVolunteerings) {
            volunteerings.forEach((volunteering) => {
                applications += volunteering.volunteers.length;
            });
        }

        return (
            <Paper variant="outlined" square component={Box} p={2}>
                <Typography variant="h5">Candidaturas efetuadas</Typography>
                <Typography variant="h4">{applications}</Typography>
            </Paper>
        );
    }

    renderVolunteeringsMade() {
        const { volunteerings } = this.state;

        return (
            <Paper variant="outlined" square component={Box} p={2}>
                <Typography variant="h5">Voluntariados criados</Typography>
                <Typography variant="h4">{volunteerings.length}</Typography>
            </Paper>
        );
    }

    renderUsersMade() {
        const { users } = this.state;

        return (
            <Paper variant="outlined" square component={Box} p={2}>
                <Typography variant="h5">Utilizadores criados</Typography>
                <Typography variant="h4">{users.length}</Typography>
            </Paper>
        );
    }

    renderInterestAreasStats() {
        const { users } = this.state;

        let interestAreas = {};
        users.forEach((user) => {
            user.interestAreas.forEach((area) => {
                interestAreas[area]
                    ? (interestAreas[area] += 1)
                    : (interestAreas[area] = 1);
            });
        });
        interestAreas = Object.entries(interestAreas);

        return (
            <Paper variant="outlined" square>
                <Typography variant="h5" component={Box} p={2}>
                    {'Áreas de interesse'}
                </Typography>
                <Divider />
                <Box p={1}>
                    <Chart
                        height="400px"
                        chartType="PieChart"
                        loader={this.renderLoading()}
                        data={[['Área', 'Nº de ocorrencias'], ...interestAreas]}
                    />
                </Box>
            </Paper>
        );
    }

    renderReasonsStats() {
        const { users } = this.state;

        let reasons = {};
        users.forEach((user) => {
            user.reasons.forEach((reason) => {
                reasons[reason]
                    ? (reasons[reason] += 1)
                    : (reasons[reason] = 1);
            });
        });
        reasons = Object.entries(reasons);

        return (
            <Paper variant="outlined" square>
                <Typography variant="h5" component={Box} p={2}>
                    {'Razões para ser voluntário'}
                </Typography>
                <Divider />
                <Box p={1}>
                    <Chart
                        height="400px"
                        chartType="PieChart"
                        loader={this.renderLoading()}
                        data={[['Razão', 'Nº de ocorrencias'], ...reasons]}
                    />
                </Box>
            </Paper>
        );
    }

    renderApplicationsStats() {
        const { volunteerings } = this.state;

        let applicationGraphData = {};
        volunteerings.forEach((volunteering) => {
            volunteering.volunteers.forEach((volunteer) => {
                applicationGraphData[volunteer.createdAt]
                    ? (applicationGraphData[volunteer.createdAt] += 1)
                    : (applicationGraphData[volunteer.createdAt] = 1);
            });
        });
        applicationGraphData = Object.entries(
            applicationGraphData,
        ).map(([date, count]) => [moment(date).toDate(), count]);

        applicationGraphData.sort((a, b) => {
            if (a[0] < b[0]) return -1;
            if (a[0] > b[0]) return 1;
            return 0;
        });

        return (
            <Paper variant="outlined" square>
                <Typography variant="h5" component={Box} p={2}>
                    {'Inscrições realizadas'}
                </Typography>
                <Divider />
                <Box p={2}>
                    <Chart
                        height={'300px'}
                        chartType="LineChart"
                        loader={this.renderLoading()}
                        data={[
                            [{ type: 'date', label: 'Day' }, 'Inscrições'],
                            ...applicationGraphData,
                        ]}
                        options={{ legend: { position: 'none' } }}
                    />
                </Box>
            </Paper>
        );
    }

    renderUsersBySchoolStats() {
        const { users } = this.state;

        let schools = {};
        users.forEach((user) => {
            if (user.school) {
                schools[user.school]
                    ? (schools[user.school] += 1)
                    : (schools[user.school] = 1);
            }
        });
        schools = Object.entries(schools);

        return (
            <Paper variant="outlined" square>
                <Typography variant="h5" component={Box} p={2}>
                    {'Utilizadores por escola'}
                </Typography>
                <Divider />
                <Box p={1}>
                    <Chart
                        height="400px"
                        chartType="PieChart"
                        loader={this.renderLoading()}
                        data={[['Escolas', 'Nº de ocorrencias'], ...schools]}
                    />
                </Box>
            </Paper>
        );
    }

    renderVolunteeringsStats() {
        const { volunteerings } = this.state;

        let volunteeringsGraphData = {};
        volunteerings.forEach((volunteering) => {
            volunteeringsGraphData[
                moment.utc(volunteering.createdAt).startOf('day').format()
            ]
                ? (volunteeringsGraphData[
                      moment.utc(volunteering.createdAt).startOf('day').format()
                  ] += 1)
                : (volunteeringsGraphData[
                      moment.utc(volunteering.createdAt).startOf('day').format()
                  ] = 1);
        });
        volunteeringsGraphData = Object.entries(
            volunteeringsGraphData,
        ).map(([date, count]) => [
            moment.utc(date).startOf('day').toDate(),
            count,
        ]);

        console.log(volunteeringsGraphData);

        volunteeringsGraphData.sort((a, b) => {
            if (a[0] < b[0]) return -1;
            if (a[0] > b[0]) return 1;
            return 0;
        });

        return (
            <Paper variant="outlined" square>
                <Typography variant="h5" component={Box} p={2}>
                    {'Voluntariados criados'}
                </Typography>
                <Divider />
                <Box p={2}>
                    <Chart
                        height={'300px'}
                        chartType="LineChart"
                        loader={this.renderLoading()}
                        data={[
                            [{ type: 'date', label: 'Day' }, 'Voluntariados'],
                            ...volunteeringsGraphData,
                        ]}
                        options={{ legend: { position: 'none' } }}
                    />
                </Box>
            </Paper>
        );
    }

    render() {
        return (
            <Grid container spacing={2}>
                <Grid item xs={12} md={4}>
                    {this.renderApplicationsMade()}
                </Grid>
                <Grid item xs={12} md={4}>
                    {this.renderVolunteeringsMade()}
                </Grid>
                <Grid item xs={12} md={4}>
                    {this.renderUsersMade()}
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderInterestAreasStats()}
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderReasonsStats()}
                </Grid>
                <Grid item xs={12}>
                    {this.renderApplicationsStats()}
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderUsersBySchoolStats()}
                </Grid>
                <Grid item xs={12}>
                    {this.renderVolunteeringsStats()}
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderUsersTable()}
                </Grid>
                <Grid item xs={12} md={6}>
                    {this.renderVolunteeringsTable()}
                </Grid>
            </Grid>
        );
    }
}

MainPage.propTypes = {
    classes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(MainPage));
