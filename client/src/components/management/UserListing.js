import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import MaterialTable from 'material-table';
import { withRouter } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

import { getUsers } from '../../api/user';

import styles from '../../styles/Dashboard';

class UserListing extends Component {
    getUsers(query) {
        return new Promise(async (resolve, reject) => {
            const response = await getUsers(
                'USER',
                query.page,
                query.pageSize,
                query.search
            );
            if (!response.data) return resolve();
            resolve({
                data: response.data.data,
                page: response.data.page,
                totalCount: response.data.totalCount,
            });
        });
    }

    render() {
        const { history } = this.props;

        const columns = [
            { title: 'Nome', field: 'name' },
            {
                title: 'Estado',
                field: 'accountStatus',
                headerStyle: { textAlign: 'center' },
                cellStyle: { textAlign: 'center' },
                render: (rowData) => {
                    switch (rowData.accountStatus) {
                        case 'ACTIVE':
                            return (
                                <Tooltip title="Ativo">
                                    <span>
                                        <CheckIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'INACTIVE':
                            return (
                                <Tooltip title="Inativo">
                                    <span>
                                        <CloseIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'PENDING':
                            return (
                                <Tooltip title="Pendente">
                                    <span>
                                        <HourglassEmptyIcon />
                                    </span>
                                </Tooltip>
                            );
                        default:
                            return null;
                    }
                },
            },
            { title: 'Tipo', field: 'memberType' },
            {
                title: 'Interno/Externo',
                field: 'isExternal',
                render: (rowData) =>
                    rowData.isExternal ? 'Externo' : 'Interno',
            },
            { title: 'Entidade', field: 'entity' },
        ];

        return (
            <MaterialTable
                title="Listagem de Utilizadores"
                columns={columns}
                options={{
                    exportButton: true,
                    exportDelimiter: ';',
                    actionsColumnIndex: -1,
                    pageSize: 15,
                    pageSizeOptions: [15, 30, 50],
                    sorting: false,
                    draggable: false,
                    emptyRowsWhenPaging: false,
                }}
                localization={{
                    toolbar: {
                        exportName: 'Exportar como CSV',
                        exportTitle: 'Exportar',
                        exportAriaLabel: 'Exportar',
                        searchTooltip: 'Pesquisar',
                        searchPlaceholder: 'Pesquisar',
                    },
                    body: {
                        emptyDataSourceMessage:
                            'Não foram encontrados registos',
                    },
                    pagination: {
                        labelDisplayedRows: '{from}-{to} de {count}',
                        labelRowsSelect: 'registos',
                    },
                }}
                data={this.getUsers}
                onRowClick={(event, rowData) =>
                    history.push(`/user/${rowData._id}`)
                }
            />
        );
    }
}

UserListing.propTypes = {
    classes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(UserListing));
