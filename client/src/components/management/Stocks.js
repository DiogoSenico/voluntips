import React, { Component } from 'react';
import MaterialTable from 'material-table';
import {
    Button,
    Dialog,
    DialogTitle,
    DialogActions,
    TextField,
    Box,
    Grid,
    Typography,
} from '@material-ui/core';
import moment from 'moment';
import { toast } from 'react-toastify';

import { getProducts, changeProductStock } from '../../api/product';

class Stocks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            products: [],
            showRemovalDialog: false,
            selectedProduct: null,
            quantityToRemove: 1,
        };

        this.handleQuantityChange = this.handleQuantityChange.bind(this);
        this.showRemovalDialog = this.showRemovalDialog.bind(this);
        this.closeRemovalDialog = this.closeRemovalDialog.bind(this);
        this.validateRemovalDialog = this.validateRemovalDialog.bind(this);
    }

    componentDidMount() {
        this.getProducts();
    }

    async getProducts() {
        const response = await getProducts();
        this.setState({ products: response.data.data, loading: false });
    }

    showRemovalDialog(selectedProduct) {
        this.setState({ showRemovalDialog: true, selectedProduct });
    }

    closeRemovalDialog() {
        this.setState({ showRemovalDialog: false, selectedProduct: null });
    }

    async validateRemovalDialog(e) {
        const { selectedProduct, quantityToRemove, products } = this.state;

        e.preventDefault();

        if (quantityToRemove === '0') {
            toast.error('Quantidade a remover inválida');
            return;
        }

        if (quantityToRemove > selectedProduct.totalAmount) {
            toast.error('Quantidade a remover acima da existente em stock');
            return;
        }

        const response = await changeProductStock(
            selectedProduct._id,
            quantityToRemove
        );
        if (response.status === 200) {
            const productIndex = products.findIndex(
                (p) => p._id === selectedProduct._id
            );
            products[productIndex].totalAmount -= quantityToRemove;
            if (products[productIndex].totalAmount === 0)
                products.splice(productIndex, 1);
            this.setState({ products });
            this.closeRemovalDialog();
        }
    }

    handleQuantityChange(e) {
        const value = e.target.value.replace(/[^\d]/, '');
        this.setState({ quantityToRemove: value });
    }

    renderRemovalDialog() {
        const {
            showRemovalDialog,
            selectedProduct,
            quantityToRemove,
        } = this.state;

        return (
            <Dialog
                open={showRemovalDialog}
                onClose={this.closeRemovalDialog}
                aria-labelledby="removalDialogTitle"
            >
                <DialogTitle id="removalDialogTitle">Remover stock</DialogTitle>
                <form onSubmit={this.validateRemovalDialog}>
                    <Box px={3}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={6}>
                                <Typography variant="body1">
                                    {'Produto:'}
                                </Typography>
                                <Typography variant="body2">
                                    {selectedProduct && selectedProduct.name}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <Typography variant="body1">
                                    {'Quantidade em stock:'}
                                </Typography>
                                <Typography variant="body2">
                                    {selectedProduct &&
                                        selectedProduct.totalAmount}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    value={quantityToRemove}
                                    onChange={this.handleQuantityChange}
                                    autoFocus
                                    variant="outlined"
                                    id="quantity"
                                    label="Quantidade a remover"
                                    name="quantity"
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <DialogActions>
                        <Button
                            onClick={this.closeRemovalDialog}
                            color="primary"
                        >
                            Cancelar
                        </Button>
                        <Button color="primary" type="submit">
                            Remover
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }

    render() {
        const { loading, products } = this.state;

        const columns = [
            { title: 'Nome', field: 'name' },
            { title: 'Quantidade', field: 'totalAmount' },
            {
                title: 'Data de criação',
                field: 'createdAt',
                render: (rowData) => {
                    return moment(rowData.createdAt).format('DD/MM/YYYY HH:mm');
                },
            },
            {
                title: 'Ultima atualização',
                field: 'updatedAt',
                render: (rowData) => {
                    return moment(rowData.updatedAt).format('DD/MM/YYYY HH:mm');
                },
            },
        ];

        return (
            <div>
                {this.renderRemovalDialog()}
                <MaterialTable
                    title="Listagem de stocks"
                    columns={columns}
                    actions={[
                        (rowData) => ({
                            icon: 'delete',
                            tooltip: 'Apagar',
                            hidden: rowData.totalAmount === 0,
                            onClick: (e, rowData) => {
                                this.showRemovalDialog(rowData);
                            },
                        }),
                    ]}
                    options={{
                        exportButton: true,
                        exportDelimiter: ';',
                        actionsColumnIndex: -1,
                        pageSize: 15,
                        pageSizeOptions: [15, 30, 50],
                        draggable: false,
                        emptyRowsWhenPaging: false,
                    }}
                    localization={{
                        header: {
                            actions: 'Ações',
                        },
                        toolbar: {
                            exportName: 'Exportar como CSV',
                            exportTitle: 'Exportar',
                            exportAriaLabel: 'Exportar',
                            searchTooltip: 'Pesquisar',
                            searchPlaceholder: 'Pesquisar',
                        },
                        body: {
                            emptyDataSourceMessage:
                                'Não foram encontrados registos',
                        },
                        pagination: {
                            labelDisplayedRows: '{from}-{to} de {count}',
                            labelRowsSelect: 'registos',
                        },
                    }}
                    isLoading={loading}
                    data={products}
                />
            </div>
        );
    }
}

export default Stocks;
