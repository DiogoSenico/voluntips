import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Dashboard from './Dashboard';
import UserListing from './UserListing';
import VolunteeringListing from './VolunteeringListing';
import ManagerListing from './managers/ManagerListing';
import DonationListing from './DonationListing';
import DonationHistory from './DonationHistory';
import Stocks from './Stocks';

import styles from '../../styles/Dashboard';

class Management extends Component {
    constructor() {
        super();
        this.state = {
            users: [],
            selectedIndex: 0,
            selectedContent: null,
            options: [
                {
                    id: 0,
                    label: 'Dashboard',
                    role: 'MANAGER',
                    url: '/management',
                    content: <Dashboard />,
                },
                {
                    id: 1,
                    label: 'Listagem de utilizadores',
                    role: 'MANAGER',
                    url: '/management/users',
                    content: <UserListing />,
                },
                {
                    id: 2,
                    label: 'Gestão de gestores',
                    role: 'ADMIN',
                    url: '/management/managers',
                    content: <ManagerListing />,
                },
                {
                    id: 3,
                    label: 'Gestão de voluntariados',
                    role: 'MANAGER',
                    url: '/management/volunteerings',
                    content: <VolunteeringListing />,
                },
                {
                    id: 4,
                    label: 'Gestão de doações',
                    role: 'MANAGER',
                    url: '/management/donations',
                    content: <DonationListing />,
                },
                {
                    id: 5,
                    label: 'Histórico de doações',
                    role: 'MANAGER',
                    url: '/management/donations/history',
                    content: <DonationHistory />,
                },
                {
                    id: 6,
                    label: 'Gestão de stocks',
                    role: 'MANAGER',
                    url: '/management/stocks',
                    content: <Stocks />,
                },
            ],
        };
        this.handleChangeIndex = this.handleChangeIndex.bind(this);
    }

    updateSelected(index) {
        let { selectedContent, selectedIndex, options } = this.state;

        selectedContent = options[index].content;
        selectedIndex = index;

        this.setState({ selectedContent, selectedIndex });
    }

    onRouteChange(newPath) {
        const { options } = this.state;

        const optionSelected = options.filter((op) => op.url === newPath)[0];
        this.updateSelected(optionSelected.id);
    }

    componentDidMount() {
        const { match } = this.props;
        this.onRouteChange(match.path);
    }

    componentWillReceiveProps(nextProps) {
        const { match } = this.props;
        if (match.path !== nextProps.match.path) {
            this.onRouteChange(nextProps.match.path);
        }
    }

    handleChangeIndex(e, index) {
        const { history } = this.props;
        const { options } = this.state;

        history.push(options[index].url);
        this.updateSelected(index);
    }

    render() {
        const { user } = this.props;
        const { selectedContent, options, selectedIndex } = this.state;

        return (
            <Grid container>
                <Grid
                    item
                    xs={false}
                    sm={3}
                    md={2}
                    component={Paper}
                    variant="outlined"
                    square
                >
                    <Hidden xsDown>
                        <List>
                            {options.map((option) => {
                                if (user.role === 'ADMIN') {
                                    return (
                                        <ListItem
                                            button
                                            key={option.id}
                                            selected={
                                                selectedIndex === option.id
                                            }
                                            onClick={(event) =>
                                                this.handleChangeIndex(
                                                    event,
                                                    option.id
                                                )
                                            }
                                        >
                                            <ListItemText
                                                primary={option.label}
                                            />
                                        </ListItem>
                                    );
                                } else if (user.role === option.role) {
                                    return (
                                        <ListItem
                                            button
                                            key={option.id}
                                            selected={
                                                selectedIndex === option.id
                                            }
                                            onClick={(event) =>
                                                this.handleChangeIndex(
                                                    event,
                                                    option.id
                                                )
                                            }
                                        >
                                            <ListItemText
                                                primary={option.label}
                                            />
                                        </ListItem>
                                    );
                                }
                                return null;
                            })}
                        </List>
                    </Hidden>
                </Grid>
                <Grid item xs={12} sm={9} md={10} component={Box} p={2}>
                    {selectedContent}
                </Grid>
            </Grid>
        );
    }
}

Management.propTypes = {
    classes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
};

export default withStyles(styles)(Management);
