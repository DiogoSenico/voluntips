import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';

import { registerManager } from '../../../api/user';

const initialState = {
    name: '',
    email: '',
    phone: '',
    address: '',
    city: '',
    zip: '',
    school: '',
    course: '',
    birthDate: null,
};

class ManagerCreation extends Component {
    constructor() {
        super();
        this.state = initialState;
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    getMaxDate() {
        let d = new Date();
        d.setFullYear(d.getFullYear() - 10);
        return d;
    }

    handleInputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleDateChange(date) {
        this.setState({ birthDate: date });
    }

    async handleSubmit(e) {
        e.preventDefault();
        if (e.currentTarget.form.reportValidity()) {
            const data = { ...this.state };
            data.birthDate = moment(data.birthDate).format('YYYY-MM-DD');
            const response = await registerManager(data);
            if (response.status === 201) {
                this.setState(initialState);
            }
        }
    }

    render() {
        const { name, email, phone, birthDate } = this.state;

        return (
            <Box component={Paper} p={3} mb={2}>
                <form onSubmit={this.handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant="h6" gutterBottom>
                                {'Criação de gestor'}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="name"
                                name="name"
                                label="Nome Completo"
                                type="text"
                                fullWidth
                                autoComplete="fname"
                                value={name}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <MuiPickersUtilsProvider utils={MomentUtils}>
                                <KeyboardDatePicker
                                    required
                                    fullWidth
                                    disableFuture
                                    openTo="year"
                                    variant="inline"
                                    orientation="landscape"
                                    format="DD/MM/YYYY"
                                    maxDate={this.getMaxDate()}
                                    minDateMessage="Data inválida (demasiado antiga)"
                                    maxDateMessage="Data inválida (demasiado no presente/futuro)"
                                    id="date-picker-inline"
                                    label="Data de nascimento"
                                    value={birthDate}
                                    views={['year', 'month', 'date']}
                                    onChange={this.handleDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="email"
                                name="email"
                                label="Email"
                                type="email"
                                fullWidth
                                autoComplete="email"
                                value={email}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="phone"
                                name="phone"
                                label="Telemóvel"
                                type="tel"
                                fullWidth
                                inputProps={{
                                    pattern: '[0-9]{3}[0-9]{3}[0-9]{3}',
                                    maxLength: '9',
                                    title: 'Exemplo: 999999999',
                                }}
                                autoComplete="tel-national"
                                value={phone}
                                onChange={this.handleInputChange}
                            />
                        </Grid>
                        <Grid
                            item
                            xs={12}
                            component={Box}
                            display="flex"
                            flexDirection="row-reverse"
                        >
                            <Button
                                type="submit"
                                variant="contained"
                                color="secondary"
                                onClick={this.handleSubmit}
                            >
                                {'Criar'}
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Box>
        );
    }
}

export default ManagerCreation;
