import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Container,
    Grid,
    Box,
    Paper,
    Button,
    Typography,
    LinearProgress,
    Collapse,
    Tooltip,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    IconButton,
    Divider,
    TextField,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Link } from 'react-router-dom';
import moment from 'moment';
import MaterialTable, { MTableActions } from 'material-table';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

import { getProject } from '../../../api/project';
import {
    applyToVolunteering,
    validateVolunteering,
    responseUserToVolunteering,
} from '../../../api/volunteering';

class VolunteeringDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            acceptedVolunteers: [],
            applied: false,
            showManagement: false,
            showConfirmation: false,
        };

        this.getProjectDetails = this.getProjectDetails.bind(this);
        this.checkRelationInfo = this.checkRelationInfo.bind(this);
        this.applyToVolunteering = this.applyToVolunteering.bind(this);
        this.handleValidateVolunteering = this.handleValidateVolunteering.bind(
            this
        );
        this.handleToggleManagement = this.handleToggleManagement.bind(this);
        this.handleDialogOpen = this.handleDialogOpen.bind(this);
        this.handleDialogClose = this.handleDialogClose.bind(this);

        this.showMotiveDialog = this.showMotiveDialog.bind(this);
        this.closeMotiveDialog = this.closeMotiveDialog.bind(this);
        this.validateMotiveDialog = this.validateMotiveDialog.bind(this);
    }

    componentDidMount() {
        this.getProjectDetails();
    }

    async getProjectDetails() {
        const { match } = this.props;

        const response = await getProject(match.params.id);
        if (response.status === 200) {
            this.setState({ ...response.data }, this.checkRelationInfo);
        } else {
            window.location.replace('/');
        }
    }

    checkRelationInfo() {
        const { user } = this.props;
        const { proposedBy, volunteers } = this.state;
        this.setState({
            isOwner: user._id === proposedBy,
            isVolunteer:
                volunteers &&
                volunteers.filter(
                    (volunteer) => volunteer.user._id === user._id
                ).length,
            acceptedVolunteers:
                volunteers &&
                volunteers.filter(
                    (volunteer) => volunteer.status === 'ACCEPTED'
                ),
        });
    }

    async applyToVolunteering() {
        const { match } = this.props;

        const response = await applyToVolunteering(match.params.id);
        if (response.status === 200) {
            this.setState({ applied: true });
        }
    }

    showMotiveDialog() {
        this.setState({ showMotiveDialog: true });
    }

    validateMotiveDialog(e) {
        e.preventDefault();
        if (e.target.motive.value) {
            this.closeMotiveDialog();
            this.handleValidateVolunteering(false, e.target.motive.value);
        }
    }

    closeMotiveDialog() {
        this.setState({ showMotiveDialog: false });
    }

    async handleValidateVolunteering(isAccepted, motive) {
        const { match } = this.props;

        const response = await validateVolunteering(match.params.id, {
            isAccepted,
            motive,
        });
        if (response.status === 200) {
            this.setState({ status: isAccepted ? 'ACCEPTED' : 'REJECTED' });
        }
    }

    async handleUserApplicationResponse(user, choice) {
        const { _id, volunteers } = this.state;
        const response = await responseUserToVolunteering(_id, user._id, {
            isAccepted: choice,
        });
        if (response.status === 200) {
            volunteers.forEach((volunteer) => {
                if (volunteer.user._id === user._id) {
                    volunteer.status = choice ? 'ACCEPTED' : 'REJECTED';
                }
            });
            this.setState({
                volunteers,
                acceptedVolunteers:
                    volunteers &&
                    volunteers.filter(
                        (volunteer) => volunteer.status === 'ACCEPTED'
                    ),
            });
        }
    }

    handleToggleManagement() {
        this.setState((prevState) => ({
            showManagement: !prevState.showManagement,
        }));
    }

    handleDialogOpen() {
        this.setState({ showConfirmation: true });
    }

    handleDialogClose() {
        this.setState({ showConfirmation: false });
    }

    renderMotiveDialog() {
        const { showMotiveDialog } = this.state;

        return (
            <Dialog
                open={showMotiveDialog}
                onClose={this.closeMotiveDialog}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Motivo</DialogTitle>
                <form onSubmit={this.validateMotiveDialog}>
                    <DialogContent>
                        <DialogContentText>
                            Por favor escreva o motivo pela qual este
                            voluntariado irá ser recusado.
                        </DialogContentText>
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            id="motive"
                            label="Motivo"
                            name="motive"
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.closeMotiveDialog}
                            color="primary"
                        >
                            Cancelar
                        </Button>
                        <Button color="primary" type="submit">
                            Recusar
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }

    renderApplyDialog() {
        const { showConfirmation } = this.state;

        return (
            <Dialog
                open={showConfirmation}
                onClose={this.handleDialogClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {'Confirmação de candidatura'}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {
                            'Tem a certeza que se pretende candidatar a este voluntariado?'
                        }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleDialogClose} color="primary">
                        {'Cancelar'}
                    </Button>
                    <Button
                        onClick={() => {
                            this.handleDialogClose();
                            this.applyToVolunteering();
                        }}
                        color="primary"
                        autoFocus
                    >
                        {'Confirmar'}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    renderStatus() {
        const { user } = this.props;
        const { status } = this.state;

        if (user.role !== 'USER') {
            switch (status) {
                case 'PENDDING':
                    return (
                        <Grid item xs={12}>
                            <Box
                                display="flex"
                                component={Paper}
                                variant="outlined"
                                p={2}
                                style={{
                                    backgroundColor: 'rgb(255, 244, 229)',
                                }}
                            >
                                <Box flexGrow={1}>
                                    <Typography variant="h5">
                                        {'Aceitação do projeto pendente'}
                                    </Typography>
                                </Box>
                                <Box display="flex" flexDirection="row-reverse">
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={() => this.showMotiveDialog()}
                                    >
                                        {'Rejeitar projeto'}
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={() =>
                                            this.handleValidateVolunteering(
                                                true
                                            )
                                        }
                                        style={{ marginRight: 10 }}
                                    >
                                        {'Aceitar projeto'}
                                    </Button>
                                </Box>
                            </Box>
                        </Grid>
                    );
                case 'ACCEPTED':
                    return (
                        <Grid item xs={12}>
                            <Alert severity="success">
                                <AlertTitle>Estado do projeto</AlertTitle>
                                Este projeto foi <strong>aceite</strong>.
                            </Alert>
                        </Grid>
                    );
                case 'REJECTED':
                    return (
                        <Grid item xs={12}>
                            <Alert severity="error">
                                <AlertTitle>Estado do projeto</AlertTitle>
                                Este projeto foi <strong>rejeitado</strong>.
                            </Alert>
                        </Grid>
                    );
                default:
                    return null;
            }
        }
        return null;
    }

    renderOptions() {
        const { match } = this.props;
        const {
            isVolunteer,
            isOwner,
            acceptedVolunteers,
            maxVolunteers,
            applied,
            status,
            showManagement,
        } = this.state;

        if (
            !isVolunteer &&
            !isOwner &&
            !applied &&
            acceptedVolunteers.length < (maxVolunteers || Infinity) &&
            status === 'ACCEPTED'
        ) {
            return (
                <Grid item xs={12}>
                    <Box display="flex" flexDirection="row-reverse">
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.handleDialogOpen}
                        >
                            {'Candidatar'}
                        </Button>
                    </Box>
                </Grid>
            );
        }
        if (isOwner) {
            return (
                <Grid item xs={12}>
                    <Box display="flex" flexDirection="row-reverse">
                        <Button
                            variant="contained"
                            color="secondary"
                            component={Link}
                            to={`/project/edit/${match.params.id}`}
                        >
                            {'Editar projeto'}
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.handleToggleManagement}
                            style={{ marginRight: 10 }}
                        >
                            {'Gerir inscrições'}
                        </Button>
                    </Box>
                    <Collapse in={showManagement}>
                        {this.renderManagement()}
                    </Collapse>
                </Grid>
            );
        }

        return null;
    }

    renderVacancies() {
        const { maxVolunteers, acceptedVolunteers } = this.state;

        if (maxVolunteers) {
            return (
                <>
                    <LinearProgress
                        variant="determinate"
                        color="secondary"
                        value={
                            (acceptedVolunteers.length / maxVolunteers) * 100
                        }
                    />
                    <Typography variant="body2">
                        {`${acceptedVolunteers.length}/${maxVolunteers}`}
                    </Typography>
                </>
            );
        }
        return (
            <Typography variant="body2">{`${acceptedVolunteers.length}/∞`}</Typography>
        );
    }

    renderManagement() {
        const { volunteers, showManagement } = this.state;

        const columns = [
            { title: 'Nome', field: 'user.name' },
            { title: 'Email', field: 'user.email' },
            { title: 'Telemovel', field: 'user.phone' },
            {
                title: 'Estado',
                field: 'status',
                render: (rowData) => {
                    switch (rowData.status) {
                        case 'ACCEPTED':
                            return (
                                <Tooltip title="Ativo">
                                    <span>
                                        <CheckIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'REJECTED':
                            return (
                                <Tooltip title="Inativo">
                                    <span>
                                        <CloseIcon />
                                    </span>
                                </Tooltip>
                            );
                        case 'PENDDING':
                            return (
                                <Tooltip title="Pendente">
                                    <span>
                                        <HourglassEmptyIcon />
                                    </span>
                                </Tooltip>
                            );
                        default:
                            return null;
                    }
                },
            },
        ];

        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={showManagement}
                onClose={this.handleToggleManagement}
            >
                <Box p={2}>
                    <Box display="flex" alignItems="center" pb={1}>
                        <Box flexGrow={1}>
                            <Typography variant="h5">
                                {'Gerir inscrições'}
                            </Typography>
                        </Box>
                        <IconButton onClick={this.handleToggleManagement}>
                            <CloseIcon />
                        </IconButton>
                    </Box>
                    <Divider />
                    <MaterialTable
                        title="Gerir inscrições"
                        columns={columns}
                        components={{
                            Container: (props) => <div {...props} />,
                            Actions: (props) => (
                                <div
                                    style={{
                                        display: 'flex',
                                        width: '100%',
                                        justifyContent: 'flex-end',
                                    }}
                                >
                                    <MTableActions {...props} />
                                </div>
                            ),
                        }}
                        actions={[
                            (rowData) => ({
                                icon: 'check',
                                tooltip: 'Aceitar voluntário',
                                hidden: rowData.status === 'ACCEPTED',
                                onClick: (e, rowData) => {
                                    this.handleUserApplicationResponse(
                                        rowData.user,
                                        true
                                    );
                                },
                            }),
                            (rowData) => ({
                                icon: 'close',
                                tooltip: 'Rejeitar voluntário',
                                hidden: rowData.status === 'REJECTED',
                                onClick: (e, rowData) => {
                                    this.handleUserApplicationResponse(
                                        rowData.user,
                                        false
                                    );
                                },
                            }),
                            {
                                icon: 'visibility',
                                tooltip: 'Visualizar perfil',
                                onClick: (e, rowData) => {
                                    window.location.href = `/user/${rowData.user._id}`;
                                },
                            },
                        ]}
                        options={{
                            exportButton: true,
                            exportDelimiter: ';',
                            actionsColumnIndex: -1,
                            draggable: false,
                            emptyRowsWhenPaging: false,
                            showTitle: false,
                        }}
                        localization={{
                            toolbar: {
                                exportName: 'Exportar como CSV',
                                exportTitle: 'Exportar',
                                exportAriaLabel: 'Exportar',
                                searchTooltip: 'Pesquisar',
                                searchPlaceholder: 'Pesquisar',
                            },
                            header: {
                                actions: 'Ações',
                            },
                            body: {
                                emptyDataSourceMessage:
                                    'Não foram encontradas inscrições',
                            },
                            pagination: {
                                labelDisplayedRows: '{from}-{to} de {count}',
                                labelRowsSelect: 'registos',
                            },
                        }}
                        data={volunteers}
                    />
                </Box>
            </Dialog>
        );
    }

    render() {
        const {
            entity,
            phone,
            email,
            userName,
            title,
            resume,
            interventionArea,
            objectives,
            description,
            formationType,
            mindate,
            maxdate,
            interestAreas,
            otherEntities,
            targetAudience,
            observations,
            proposedBy,
        } = this.state;

        return (
            <Container component={Box} py={3}>
                {this.renderMotiveDialog()}
                {this.renderApplyDialog()}
                <Grid container spacing={2}>
                    {this.renderStatus()}
                    {this.renderOptions()}
                    <Grid item xs={12} sm={8}>
                        <Paper variant="outlined" square component={Box} p={2}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Typography variant="h4">
                                        {title}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="body1">
                                        {'Resumo'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {resume}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="body1">
                                        {'Descrição'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {description}
                                    </Typography>
                                </Grid>
                                {objectives && (
                                    <Grid item xs={12}>
                                        <Typography variant="body1">
                                            {'Objetivos'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {objectives}
                                        </Typography>
                                    </Grid>
                                )}
                                {observations && (
                                    <Grid item xs={12}>
                                        <Typography variant="body1">
                                            {'Observações'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {observations}
                                        </Typography>
                                    </Grid>
                                )}
                                <Grid item xs={12} sm={6}>
                                    <Typography variant="body1">
                                        {'Áreas afetadas'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {interventionArea}
                                    </Typography>
                                </Grid>
                                {otherEntities && (
                                    <Grid item xs={12} sm={6}>
                                        <Typography variant="body1">
                                            {'Entidades'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {otherEntities}
                                        </Typography>
                                    </Grid>
                                )}
                                <Grid item xs={12} sm={6}>
                                    <Typography variant="body1">
                                        {'Publico Alvo'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {targetAudience}
                                    </Typography>
                                </Grid>
                                {formationType && (
                                    <Grid item xs={12} sm={6}>
                                        <Typography variant="body1">
                                            {'Formação necessária'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {formationType}
                                        </Typography>
                                    </Grid>
                                )}
                                <Grid item xs={12} sm={6}>
                                    <Typography variant="body1">
                                        {'Áreas requisitadas'}
                                    </Typography>
                                    {interestAreas &&
                                        interestAreas.map((area, index) => (
                                            <Typography
                                                key={index}
                                                variant="body2"
                                            >
                                                • {area}
                                            </Typography>
                                        ))}
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper variant="outlined" square component={Box} p={2}>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Nome da entidade'}
                                </Typography>
                                <Typography variant="body2">
                                    {entity}
                                </Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Telemovel'}
                                </Typography>
                                <Typography variant="body2">{phone}</Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Email'}
                                </Typography>
                                <Typography variant="body2">{email}</Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Contato'}
                                </Typography>
                                <Typography variant="body2">
                                    {userName}
                                </Typography>
                            </Box>
                            <Box py={0.5}>
                                <Button
                                    fullWidth
                                    variant="contained"
                                    color="secondary"
                                    component={Link}
                                    to={`/user/${proposedBy}`}
                                >
                                    {'Visualizar criador'}
                                </Button>
                            </Box>
                        </Paper>
                        <Paper
                            variant="outlined"
                            square
                            component={Box}
                            p={2}
                            mt={2}
                        >
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Data de inicio'}
                                </Typography>
                                <Typography variant="body2">
                                    {moment(mindate).format('DD/MM/YYYY')}
                                </Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Data de fim'}
                                </Typography>
                                <Typography variant="body2">
                                    {moment(maxdate).format('DD/MM/YYYY')}
                                </Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Vagas'}
                                </Typography>
                                {this.renderVacancies()}
                            </Box>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

VolunteeringDetails.propTypes = {
    user: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

export default VolunteeringDetails;
