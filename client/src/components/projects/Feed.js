import React, { Component } from 'react';
import {
    Container,
    Grid,
    Box,
    Paper,
    Button,
    Typography,
    TextField,
    Chip,
    CircularProgress,
    Tooltip,
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { Link } from 'react-router-dom';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from 'moment';

import { getConstants } from '../../api/user';
import { getProjects } from '../../api/project';

class Feed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            loading: true,
            page: 0,
            pageSize: 1000,
            search: '',
            minDate: null,
            maxDate: null,
            interestAreas: [],
            loadedInterestAreas: [],
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleMinDateChange = this.handleMinDateChange.bind(this);
        this.handleMaxDateChange = this.handleMaxDateChange.bind(this);
        this.handleAreasChange = this.handleAreasChange.bind(this);
        this.applyFilters = this.applyFilters.bind(this);
    }

    componentDidMount() {
        this.getConstants();
        this.getProjects();
    }

    async getConstants() {
        const loadedInterestAreas = (await getConstants('interestAreas')).data;

        this.setState({ loadedInterestAreas });
    }

    async getProjects() {
        const {
            page,
            pageSize,
            search,
            minDate,
            maxDate,
            interestAreas,
        } = this.state;

        const response = await getProjects(
            page,
            pageSize,
            search,
            minDate,
            maxDate,
            interestAreas
        );
        this.setState({ loading: false });
        if (response.status === 200) {
            this.setState({
                projects: response.data.data,
                page: page + 1,
            });
        }
    }

    handleInputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleMinDateChange(minDate) {
        this.setState({ minDate });
    }

    handleMaxDateChange(maxDate) {
        this.setState({ maxDate });
    }

    handleAreasChange(e, interestAreas) {
        this.setState({ interestAreas });
    }

    applyFilters(e) {
        e.preventDefault();
        this.setState({ loading: true, page: 0 }, this.getProjects);
    }

    renderLoading() {
        return (
            <Box display="flex" alignItems="center" justifyContent="center">
                <CircularProgress color="secondary" />
            </Box>
        );
    }

    renderInterestAreas() {
        const { loadedInterestAreas, interestAreas } = this.state;

        return (
            <Autocomplete
                multiple
                filterSelectedOptions
                options={loadedInterestAreas}
                value={interestAreas}
                onChange={this.handleAreasChange}
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip
                            variant="outlined"
                            label={option}
                            {...getTagProps({ index })}
                        />
                    ))
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="outlined"
                        label="Áreas de interesse"
                    />
                )}
            />
        );
    }

    renderVacancies(project) {
        let acceptedVolunteers = [];
        if (project.volunteers) {
            acceptedVolunteers = project.volunteers.filter(
                (volunteer) => volunteer.status === 'ACCEPTED'
            );
        }

        if (project.maxVolunteers) {
            return (
                <Typography variant="body2">
                    {`${acceptedVolunteers.length}/${project.maxVolunteers}`}
                </Typography>
            );
        }
        return (
            project.volunteers && (
                <Typography variant="body2">{`${acceptedVolunteers.length}/∞`}</Typography>
            )
        );
    }

    renderProjects() {
        const { projects } = this.state;

        if (projects.length) {
            return projects.map((project) => (
                <Paper
                    key={project.id}
                    square
                    variant="outlined"
                    component={Box}
                    p={2}
                    mb={2}
                >
                    <Grid
                        container
                        spacing={2}
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item xs={10}>
                            <Typography variant="h5">
                                {project.title}
                            </Typography>
                            <Typography variant="body2">
                                {project.entity}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <Box display="flex" justifyContent="flex-end">
                                <Tooltip title="Tipo de projeto">
                                    <Chip
                                        label={
                                            project.type === 'VOLUNTEERING'
                                                ? 'Voluntariado'
                                                : 'Doação'
                                        }
                                        variant="outlined"
                                    />
                                </Tooltip>
                            </Box>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1">
                                {project.resume}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            {project.interestAreas.map((area) => (
                                <Chip
                                    variant="outlined"
                                    color="secondary"
                                    label={area}
                                    style={{ margin: '2px 2px' }}
                                />
                            ))}
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <Typography variant="h6">
                                {'Data de inicio'}
                            </Typography>
                            <Typography variant="body2">
                                {moment(project.mindate).format('DD/MM/YYYY')}
                            </Typography>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <Typography variant="h6">
                                {'Data de fim'}
                            </Typography>
                            <Typography variant="body2">
                                {moment(project.maxdate).format('DD/MM/YYYY')}
                            </Typography>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            {project.type === 'VOLUNTEERING' && (
                                <>
                                    <Typography variant="h6">
                                        {'Vagas'}
                                    </Typography>
                                    {this.renderVacancies(project)}
                                </>
                            )}
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <Box display="flex" flexDirection="row-reverse">
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    component={Link}
                                    to={
                                        project.type === 'VOLUNTEERING'
                                            ? `/project/${project._id}`
                                            : `/donation/${project._id}`
                                    }
                                >
                                    {'Ver mais'}
                                </Button>
                            </Box>
                        </Grid>
                    </Grid>
                </Paper>
            ));
        }

        return (
            <Box
                display="flex"
                alignItems="center"
                justifyContent="center"
                p={2}
            >
                <Typography variant="h4" color="textSecondary">
                    {'Não foram encontrados projetos'}
                </Typography>
            </Box>
        );
    }

    render() {
        const { loading, search, minDate, maxDate } = this.state;

        return (
            <Container>
                <Grid container spacing={2} component={Box} py={3}>
                    <Grid item xs={12}>
                        <Typography variant="h3">
                            {'Projetos a decorrer'}
                        </Typography>
                    </Grid>
                    <Grid item xs={12} md={3}>
                        <Paper square variant="outlined" component={Box} p={2}>
                            <Box display="flex" justifyContent="center">
                                <Button
                                    fullWidth
                                    variant="contained"
                                    color="secondary"
                                    component={Link}
                                    to={'/project/create'}
                                >
                                    {'Sugerir Voluntariado'}
                                </Button>
                            </Box>
                        </Paper>

                        <Paper
                            square
                            variant="outlined"
                            component={Box}
                            p={2}
                            mt={2}
                        >
                            <form onSubmit={this.applyFilters}>
                                <Grid container spacing={3}>
                                    <Grid item xs={12}>
                                        <Typography variant="h5">
                                            Filtragem
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            id="search"
                                            name="search"
                                            label="Procurar"
                                            type="text"
                                            fullWidth
                                            variant="outlined"
                                            value={search}
                                            onChange={this.handleInputChange}
                                        />
                                    </Grid>
                                    <Grid item xs={6} sm={12}>
                                        <MuiPickersUtilsProvider
                                            utils={MomentUtils}
                                        >
                                            <KeyboardDatePicker
                                                fullWidth
                                                disablePast
                                                inputVariant="outlined"
                                                format="DD/MM/YYYY"
                                                minDateMessage="Data inválida (demasiado antiga)"
                                                maxDateMessage="Data inválida (demasiado no presente/futuro)"
                                                label="Data minima"
                                                value={minDate}
                                                onChange={
                                                    this.handleMinDateChange
                                                }
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change date',
                                                }}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Grid item xs={6} sm={12}>
                                        <MuiPickersUtilsProvider
                                            utils={MomentUtils}
                                        >
                                            <KeyboardDatePicker
                                                fullWidth
                                                inputVariant="outlined"
                                                format="DD/MM/YYYY"
                                                minDateMessage="Data inválida (demasiado antiga)"
                                                maxDateMessage="Data inválida (demasiado no presente/futuro)"
                                                label="Data máxima"
                                                value={maxDate}
                                                onChange={
                                                    this.handleMaxDateChange
                                                }
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change date',
                                                }}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Grid item xs={12}>
                                        {this.renderInterestAreas()}
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Button
                                            fullWidth
                                            variant="contained"
                                            color="secondary"
                                            type="submit"
                                        >
                                            {'Aplicar'}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} md={9}>
                        {loading ? this.renderLoading() : this.renderProjects()}
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

export default Feed;
