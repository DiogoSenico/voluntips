import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Box, Paper, Typography } from '@material-ui/core';

import { createDonationProject } from '../../../api/donation';
import ProjectForm from '../ProjectForm';

class DonationCreation extends Component {
    constructor(props) {
        super(props);

        this.prefillForm = this.prefillForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.prefillForm();
    }

    prefillForm() {
        const { user } = this.props;
        return {
            entity: user.entity,
            email: user.email,
            phone: user.phone,
            userName: user.name,
        };
    }

    async handleSubmit(data) {
        const { history } = this.props;

        const response = await createDonationProject(data);
        if (response.status === 201) {
            history.goBack();
        }
    }

    render() {
        return (
            <Container component={Box} py={3}>
                <Paper component={Box} p={2}>
                    <Typography variant="h3" gutterBottom>
                        {'Criar doação'}
                    </Typography>
                    <ProjectForm
                        editMode={false}
                        prefillObj={this.prefillForm()}
                        submitFunc={this.handleSubmit}
                        submitText="Criar"
                    />
                </Paper>
            </Container>
        );
    }
}

DonationCreation.propTypes = {
    user: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default DonationCreation;
