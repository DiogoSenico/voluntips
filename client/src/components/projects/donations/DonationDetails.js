import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Container,
    Grid,
    Box,
    Paper,
    Button,
    Typography,
    Collapse,
    Dialog,
    DialogActions,
    IconButton,
    Divider,
    TextField,
    List,
    ListItem,
    ListItemText,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Link } from 'react-router-dom';
import moment from 'moment';
import CloseIcon from '@material-ui/icons/Close';
import MaterialTable from 'material-table';
import { toast } from 'react-toastify';

import { getProject } from '../../../api/project';
import { makeDonation } from '../../../api/donation';
import { getUsers } from '../../../api/user';
import { getProducts } from '../../../api/product';

class DonationDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showManagement: false,
            donations: [],

            users: [],
            products: [],

            donator: null,
            donationProduct: '',
            donationQuantity: 1,
            donationsToMake: [],
        };

        this.getProjectDetails = this.getProjectDetails.bind(this);
        this.checkRelationInfo = this.checkRelationInfo.bind(this);
        this.handleToggleManagement = this.handleToggleManagement.bind(this);
        this.handleDialogOpen = this.handleDialogOpen.bind(this);
        this.handleDialogClose = this.handleDialogClose.bind(this);

        this.handleDonatorChange = this.handleDonatorChange.bind(this);
        this.handleProductInputChange = this.handleProductInputChange.bind(
            this,
        );
        this.handleQuantityChange = this.handleQuantityChange.bind(this);
        this.handleAddDonation = this.handleAddDonation.bind(this);
        this.handleMakeDonation = this.handleMakeDonation.bind(this);
    }

    componentDidMount() {
        this.getProjectDetails();
    }

    async getProjectDetails() {
        const { match } = this.props;

        const response = await getProject(match.params.id);
        if (response.status === 200) {
            this.setState({ ...response.data }, this.checkRelationInfo);
        } else {
            window.location.replace('/');
        }
    }

    checkRelationInfo() {
        const { user } = this.props;
        const { proposedBy } = this.state;

        // if (user._id === proposedBy) {
        //     this.setState({ isOwner: true });
        //     this.getUsers();
        //     this.getProducts();
        // }
        if (user.role === 'ADMIN' || user.role === 'MANAGER') {
            this.setState({ isOwner: true });
            this.getUsers();
            this.getProducts();
        }
    }

    async getUsers() {
        const response = await getUsers('', 0, 1000, '');
        if (response.status === 200) {
            this.setState({ users: response.data.data });
        }
    }

    async getProducts() {
        const response = await getProducts();
        if (response.status === 200) {
            this.setState({ products: response.data.data });
        }
    }

    handleToggleManagement() {
        this.setState((prevState) => ({
            showManagement: !prevState.showManagement,
        }));
    }

    handleDialogOpen() {
        this.setState({ showConfirmation: true });
    }

    handleDialogClose() {
        this.setState({ showConfirmation: false });
    }

    handleDonatorChange(e, donator) {
        this.setState({ donator });
    }

    handleProductInputChange(e, donationProduct) {
        this.setState({ donationProduct });
    }

    handleQuantityChange(e) {
        const value = e.target.value.replace(/[^\d]/, '');
        this.setState({ donationQuantity: value });
    }

    handleAddDonation(e) {
        let { donationProduct, donationQuantity, donationsToMake } = this.state;
        e.preventDefault();

        const donationFound = donationsToMake.findIndex(
            (donation) => donation.product === donationProduct,
        );
        if (donationFound > -1) {
            donationsToMake[donationFound].quantity += donationQuantity;
            this.setState({ donationsToMake });
        } else {
            const newDonation = {
                product: donationProduct,
                quantity: donationQuantity,
            };
            this.setState({
                donationsToMake: [...donationsToMake, newDonation],
            });
        }

        this.setState({
            donationProduct: '',
            donationQuantity: 1,
        });
    }

    async handleMakeDonation(e) {
        const { match } = this.props;
        const { donator, donationsToMake, donations } = this.state;

        if (!donator) {
            toast.error('Por favor preencha o campo do doador');
            return;
        }
        if (donationsToMake.length === 0) {
            toast.error('Não foram registados produtos para doar');
            return;
        }

        const data = {
            userId: donator._id,
            products: donationsToMake.map((d) => ({
                name: d.product,
                amount: Number(d.quantity),
            })),
        };

        const response = await makeDonation(match.params.id, data);
        if (response.status === 200) {
            this.handleToggleManagement();
            const newDonation = {
                created_at: new Date(),
                donator,
                products: data.products,
            };
            this.setState({
                donationsToMake: [],
                donator: null,
                donations: [newDonation, ...donations],
            });
        }
    }

    renderOptions() {
        const { match } = this.props;
        const { isOwner, showManagement } = this.state;

        if (isOwner) {
            return (
                <Grid item xs={12}>
                    <Box display="flex" flexDirection="row-reverse">
                        <Button
                            variant="contained"
                            color="secondary"
                            component={Link}
                            to={`/donation/edit/${match.params.id}`}
                        >
                            {'Editar projeto'}
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={this.handleToggleManagement}
                            style={{ marginRight: 10 }}
                        >
                            {'Registar doação'}
                        </Button>
                    </Box>
                    <Collapse in={showManagement}>
                        {this.renderManagement()}
                    </Collapse>
                </Grid>
            );
        }

        return null;
    }

    renderDonationTable() {
        let { donationsToMake } = this.state;

        return (
            <MaterialTable
                title="Listagem de Utilizadores"
                columns={[
                    { title: 'Produto', field: 'product' },
                    { title: 'Quantidade', field: 'quantity' },
                ]}
                components={{
                    Container: (props) => (
                        <Paper {...props} variant="outlined" square />
                    ),
                }}
                actions={[
                    {
                        icon: 'delete',
                        tooltip: 'Apagar',
                        onClick: (event, rowData) => {
                            donationsToMake = donationsToMake.filter((el) => {
                                return el.product !== rowData.product;
                            });
                            this.setState({ donationsToMake });
                        },
                    },
                ]}
                options={{
                    actionsColumnIndex: -1,
                    draggable: false,
                    emptyRowsWhenPaging: false,
                    toolbar: false,
                    pageSize: 4,
                    pageSizeOptions: [4],
                }}
                localization={{
                    body: {
                        emptyDataSourceMessage:
                            'Não foram encontrados registos',
                    },
                    pagination: {
                        labelDisplayedRows: '{from}-{to} de {count}',
                        labelRowsSelect: 'registos',
                    },
                }}
                data={donationsToMake}
            />
        );
    }

    renderManagement() {
        const {
            showManagement,
            users,
            products,
            donator,
            donationProduct,
            donationQuantity,
        } = this.state;

        return (
            <Dialog
                fullWidth
                maxWidth="md"
                open={showManagement}
                onClose={this.handleToggleManagement}
            >
                <Box p={2}>
                    <Box display="flex" alignItems="center" pb={1}>
                        <Box flexGrow={1}>
                            <Typography variant="h5">
                                {'Registar doação'}
                            </Typography>
                        </Box>
                        <IconButton onClick={this.handleToggleManagement}>
                            <CloseIcon />
                        </IconButton>
                    </Box>
                    <Divider />
                    <Box mt={2}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Autocomplete
                                    id="donationDonator"
                                    value={donator}
                                    options={users}
                                    getOptionLabel={(option) =>
                                        `${option.name} (${option.email})`
                                    }
                                    onChange={this.handleDonatorChange}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            required
                                            variant="outlined"
                                            label="Doador"
                                        />
                                    )}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <form onSubmit={this.handleAddDonation}>
                                    <Paper
                                        variant="outlined"
                                        square
                                        component={Box}
                                        p={2}
                                    >
                                        <Grid container spacing={2}>
                                            <Grid item xs={12}>
                                                <Autocomplete
                                                    freeSolo
                                                    id="donationProduct"
                                                    inputValue={
                                                        donationProduct.name ||
                                                        donationProduct
                                                    }
                                                    options={products}
                                                    getOptionLabel={(option) =>
                                                        option.name
                                                    }
                                                    onInputChange={
                                                        this
                                                            .handleProductInputChange
                                                    }
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            required
                                                            label="Produto"
                                                            variant="outlined"
                                                        />
                                                    )}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    required
                                                    value={donationQuantity}
                                                    onChange={
                                                        this
                                                            .handleQuantityChange
                                                    }
                                                    id="quantidade"
                                                    label="Quantidade"
                                                    name="quantidade"
                                                    variant="outlined"
                                                    fullWidth
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Box
                                                    display="flex"
                                                    flexDirection="row-reverse"
                                                >
                                                    <Button
                                                        variant="contained"
                                                        size="small"
                                                        color="secondary"
                                                        type="submit"
                                                    >
                                                        {'Adicionar'}
                                                    </Button>
                                                </Box>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </form>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                {this.renderDonationTable()}
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
                <DialogActions>
                    <Button color="primary" onClick={this.handleMakeDonation}>
                        Registar
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    renderDonationsMade() {
        const { donations } = this.state;

        return (
            <List style={{ padding: 0 }}>
                {donations.map((donation) => (
                    <ListItem key={donation.created_at} divider>
                        <ListItemText
                            primary={`${
                                donation.donator.name
                            } doou ${donation.products
                                .map((item) => item.amount)
                                .reduce(
                                    (prev, curr) => prev + curr,
                                    0,
                                )} bens para este projeto!`}
                            secondary={moment(donation.created_at).format(
                                'DD/MM/YYYY HH:mm',
                            )}
                        />
                    </ListItem>
                ))}
            </List>
        );
    }

    render() {
        const {
            entity,
            phone,
            email,
            userName,
            title,
            resume,
            interventionArea,
            objectives,
            description,
            formationType,
            mindate,
            maxdate,
            interestAreas,
            otherEntities,
            targetAudience,
            observations,
        } = this.state;

        return (
            <Container component={Box} py={3}>
                <Grid container spacing={2}>
                    {this.renderOptions()}
                    <Grid item xs={12} sm={8}>
                        <Paper variant="outlined" square component={Box} p={2}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Typography variant="h4">
                                        {title}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1">
                                        {'Data de inicio'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {moment(mindate).format('DD/MM/YYYY')}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Typography variant="body1">
                                        {'Data de fim'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {moment(maxdate).format('DD/MM/YYYY')}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="body1">
                                        {'Resumo'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {resume}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="body1">
                                        {'Descrição'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {description}
                                    </Typography>
                                </Grid>
                                {objectives && (
                                    <Grid item xs={12}>
                                        <Typography variant="body1">
                                            {'Objetivos'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {objectives}
                                        </Typography>
                                    </Grid>
                                )}
                                {observations && (
                                    <Grid item xs={12}>
                                        <Typography variant="body1">
                                            {'Observações'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {observations}
                                        </Typography>
                                    </Grid>
                                )}
                                <Grid item xs={12} sm={6}>
                                    <Typography variant="body1">
                                        {'Áreas afetadas'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {interventionArea}
                                    </Typography>
                                </Grid>
                                {otherEntities && (
                                    <Grid item xs={12} sm={6}>
                                        <Typography variant="body1">
                                            {'Entidades'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {otherEntities}
                                        </Typography>
                                    </Grid>
                                )}
                                <Grid item xs={12} sm={6}>
                                    <Typography variant="body1">
                                        {'Publico Alvo'}
                                    </Typography>
                                    <Typography variant="body2">
                                        {targetAudience}
                                    </Typography>
                                </Grid>
                                {formationType && (
                                    <Grid item xs={12} sm={6}>
                                        <Typography variant="body1">
                                            {'Formação necessária'}
                                        </Typography>
                                        <Typography variant="body2">
                                            {formationType}
                                        </Typography>
                                    </Grid>
                                )}
                                <Grid item xs={12} sm={6}>
                                    <Typography variant="body1">
                                        {'Áreas requisitadas'}
                                    </Typography>
                                    {interestAreas &&
                                        interestAreas.map((area, index) => (
                                            <Typography
                                                key={index}
                                                variant="body2"
                                            >
                                                • {area}
                                            </Typography>
                                        ))}
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <Paper variant="outlined" square component={Box} p={2}>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Nome da entidade'}
                                </Typography>
                                <Typography variant="body2">
                                    {entity}
                                </Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Telemovel'}
                                </Typography>
                                <Typography variant="body2">{phone}</Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Email'}
                                </Typography>
                                <Typography variant="body2">{email}</Typography>
                            </Box>
                            <Box py={0.5}>
                                <Typography variant="body1">
                                    {'Contato'}
                                </Typography>
                                <Typography variant="body2">
                                    {userName}
                                </Typography>
                            </Box>
                        </Paper>
                        <Paper variant="outlined" square component={Box} mt={2}>
                            <Box p={2}>
                                <Typography variant="body1">
                                    {'Doações realizadas'}
                                </Typography>
                            </Box>
                            <Divider />
                            <Box>{this.renderDonationsMade()}</Box>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        );
    }
}

DonationDetails.propTypes = {
    user: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};

export default DonationDetails;
