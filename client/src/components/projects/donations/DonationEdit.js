import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Box, Paper, Typography } from '@material-ui/core';

import { getProject, updateProject } from '../../../api/project';
import ProjectForm from '../ProjectForm';

class DonationEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prefillInfo: {},
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.getProjectDetails();
    }

    async getProjectDetails() {
        const { match } = this.props;

        const response = await getProject(match.params.id);
        if (response.status === 200) {
            this.setState({ prefillInfo: response.data }, this.verifyOwnership);
        } else {
            window.location.replace('/');
        }
    }

    verifyOwnership() {
        const { user } = this.props;
        const { prefillInfo } = this.state;

        if (user._id !== prefillInfo.proposedBy) {
            window.location.replace('/');
        }
    }

    async handleSubmit(data) {
        const { match, history } = this.props;

        const response = await updateProject(match.params.id, data);
        if (response.status === 200) {
            history.goBack();
        }
    }

    render() {
        const { prefillInfo } = this.state;

        return (
            <Container component={Box} py={3}>
                <Paper component={Box} p={2}>
                    <Typography variant="h3" gutterBottom>
                        {'Editar doação'}
                    </Typography>
                    <ProjectForm
                        key={prefillInfo._id}
                        editMode={true}
                        prefillObj={prefillInfo}
                        submitFunc={this.handleSubmit}
                        submitText="Alterar doação"
                    />
                </Paper>
            </Container>
        );
    }
}

DonationEdit.propTypes = {
    user: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default DonationEdit;
