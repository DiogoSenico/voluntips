import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Grid,
    Box,
    Button,
    TextField,
    FormControlLabel,
    Checkbox,
    Chip,
} from '@material-ui/core';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import moment from 'moment';
import 'moment/locale/pt';
import MomentUtils from '@date-io/moment';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { toast } from 'react-toastify';

import { getConstants } from '../../api/user';

class ProjectForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mindate: new Date(),
            maxdate: new Date(),

            otherEntities: [],
            interestAreas: [],
            loadedInterestAreas: [],
        };

        this.getConstants = this.getConstants.bind(this);
        this.prefillForm = this.prefillForm.bind(this);

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.handleEntitiesChange = this.handleEntitiesChange.bind(this);
        this.handleAreasChange = this.handleAreasChange.bind(this);
        this.handleMinDateChange = this.handleMinDateChange.bind(this);
        this.handleMaxDateChange = this.handleMaxDateChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.getConstants();
        this.prefillForm();
    }

    async getConstants() {
        const loadedInterestAreas = (await getConstants('interestAreas')).data;
        this.setState({ loadedInterestAreas });
    }

    prefillForm() {
        const { prefillObj } = this.props;

        if (prefillObj) {
            this.setState(prefillObj);
        }
    }

    handleInputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleCheckChange(e) {
        this.setState({ [e.target.name]: e.target.checked });
    }

    handleMinDateChange(mindate) {
        this.setState({ mindate });
    }

    handleMaxDateChange(maxdate) {
        this.setState({ maxdate });
    }

    handleEntitiesChange(e, otherEntities) {
        this.setState({ otherEntities });
    }

    handleAreasChange(e, interestAreas) {
        this.setState({ interestAreas });
    }

    getFormData() {
        let {
            entity,
            phone,
            email,
            userName,
            title,
            resume,
            interventionArea,
            objectives,
            description,
            formationType,
            mindate,
            maxdate,
            interestAreas,
            otherEntities,
            targetAudience,
            observations,
            autoAccept,
            maxVolunteers,
        } = this.state;
        const { type } = this.props;

        mindate = moment(mindate).format('YYYY-MM-DD');
        maxdate = moment(maxdate).format('YYYY-MM-DD');

        let aux = {};
        if (type === 'volunteering')
            aux = { autoAccept, maxVolunteers, formationType };

        return {
            entity,
            phone,
            email,
            userName,
            title,
            resume,
            interventionArea,
            objectives,
            description,
            mindate,
            maxdate,
            interestAreas,
            otherEntities,
            targetAudience,
            observations,
            ...aux,
        };
    }

    async handleSubmit(e) {
        const { submitFunc } = this.props;
        const { interestAreas } = this.state;
        e.preventDefault();

        if (interestAreas === undefined || interestAreas.length === 0) {
            toast.error('Por favor preencha o campo das áreas de interesse');
            return false;
        }

        const data = this.getFormData();
        submitFunc(data);
    }

    renderVolunteeringAditionalFields() {
        const { type } = this.props;
        const {
            needsFormation,
            formationType,
            autoAccept,
            maxVolunteers,
        } = this.state;

        if (type === 'volunteering') {
            return (
                <>
                    <Grid item xs={12} sm={6}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={needsFormation}
                                    onChange={this.handleCheckChange}
                                    name="needsFormation"
                                />
                            }
                            label="Necessária formação especifica?"
                        />
                        <TextField
                            required
                            disabled={!needsFormation}
                            id="formationType"
                            name="formationType"
                            label="Tipo de formação"
                            variant="outlined"
                            fullWidth
                            multiline
                            rows={4}
                            value={formationType}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={autoAccept}
                                    onChange={this.handleCheckChange}
                                    name="autoAccept"
                                />
                            }
                            label="Aceitar automaticamente candidaturas a este projeto?"
                        />
                        <TextField
                            id="maxVolunteers"
                            name="maxVolunteers"
                            label="Número de vagas"
                            variant="outlined"
                            helperText="Para um número ilimitado de vagas não preencha este campo"
                            fullWidth
                            value={maxVolunteers}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                </>
            );
        }

        return null;
    }

    render() {
        const { editMode, submitText } = this.props;
        const {
            entity,
            phone,
            email,
            userName,
            title,
            resume,
            interventionArea,
            objectives,
            description,
            mindate,
            maxdate,
            interestAreas,
            otherEntities,
            targetAudience,
            observations,
            loadedInterestAreas,
        } = this.state;

        return (
            <form onSubmit={this.handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="entity"
                            name="entity"
                            label="Nome da entidade"
                            variant="outlined"
                            fullWidth
                            value={entity}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="userName"
                            name="userName"
                            label="Pessoa de contacto"
                            variant="outlined"
                            fullWidth
                            value={userName}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="phone"
                            name="phone"
                            label="Telemóvel"
                            type="tel"
                            variant="outlined"
                            fullWidth
                            inputProps={{
                                pattern: '[0-9]{3}[0-9]{3}[0-9]{3}',
                                maxLength: '9',
                                title: 'Exemplo: 999999999',
                            }}
                            autoComplete="tel-national"
                            value={phone}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="email"
                            name="email"
                            label="Email"
                            type="email"
                            variant="outlined"
                            fullWidth
                            autoComplete="email"
                            value={email}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="title"
                            name="title"
                            label="Designação do projeto/atividade"
                            variant="outlined"
                            fullWidth
                            value={title}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="interventionArea"
                            name="interventionArea"
                            label="Área de intervenção"
                            variant="outlined"
                            fullWidth
                            value={interventionArea}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <MuiPickersUtilsProvider
                            libInstance={moment}
                            utils={MomentUtils}
                            locale="pt"
                        >
                            <KeyboardDatePicker
                                required
                                fullWidth
                                disablePast={!editMode}
                                inputVariant="outlined"
                                format="DD/MM/YYYY"
                                minDateMessage="Data inválida (demasiado antiga)"
                                invalidDateMessage="Formato de data inválido"
                                label="Data de começo"
                                value={mindate}
                                onChange={this.handleMinDateChange}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <MuiPickersUtilsProvider
                            libInstance={moment}
                            utils={MomentUtils}
                            locale="pt"
                        >
                            <KeyboardDatePicker
                                required
                                fullWidth
                                disablePast={!editMode}
                                inputVariant="outlined"
                                format="DD/MM/YYYY"
                                minDateMessage="Data inválida (demasiado antiga)"
                                invalidDateMessage="Formato de data inválido"
                                label="Data de fim"
                                minDate={mindate}
                                value={maxdate}
                                onChange={this.handleMaxDateChange}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Autocomplete
                            id="otherEntities"
                            multiple
                            freeSolo
                            autoSelect
                            options={[]}
                            value={[...otherEntities]}
                            onChange={this.handleEntitiesChange}
                            renderTags={(value, getTagProps) =>
                                value.map((option, index) => (
                                    <Chip
                                        variant="outlined"
                                        label={option}
                                        {...getTagProps({ index })}
                                    />
                                ))
                            }
                            renderInput={params => (
                                <TextField
                                    {...params}
                                    variant="outlined"
                                    helperText="Pode adicionar várias ao pressionar Enter"
                                    label="Entidades Envolvidas"
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="targetAudience"
                            name="targetAudience"
                            label="Público Alvo/Beneficiários"
                            variant="outlined"
                            fullWidth
                            value={targetAudience}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="resume"
                            name="resume"
                            label="Resumo do projeto/atividade"
                            variant="outlined"
                            fullWidth
                            multiline
                            rows={5}
                            value={resume}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="description"
                            name="description"
                            label="Descrição das atividades"
                            variant="outlined"
                            fullWidth
                            multiline
                            rows={5}
                            value={description}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="objectives"
                            name="objectives"
                            label="Objetivos"
                            variant="outlined"
                            fullWidth
                            multiline
                            rows={5}
                            value={objectives}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            id="observations"
                            name="observations"
                            label="Observações"
                            variant="outlined"
                            fullWidth
                            multiline
                            rows={5}
                            value={observations}
                            onChange={this.handleInputChange}
                        />
                    </Grid>
                    {this.renderVolunteeringAditionalFields()}
                    <Grid item xs={12} sm={6}>
                        <Autocomplete
                            id="interestArea"
                            multiple
                            filterSelectedOptions
                            options={loadedInterestAreas}
                            value={[...interestAreas]}
                            onChange={this.handleAreasChange}
                            renderTags={(value, getTagProps) =>
                                value.map((option, index) => (
                                    <Chip
                                        variant="outlined"
                                        label={option}
                                        {...getTagProps({ index })}
                                    />
                                ))
                            }
                            renderInput={params => (
                                <TextField
                                    {...params}
                                    variant="outlined"
                                    label="Áreas que necessita voluntários *"
                                />
                            )}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box display="flex" flexDirection="row-reverse">
                            <Button
                                variant="contained"
                                color="secondary"
                                type="submit"
                            >
                                {submitText}
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

ProjectForm.propTypes = {
    type: PropTypes.string.isRequired,
    editMode: PropTypes.bool.isRequired,
    prefillObj: PropTypes.object,
    submitFunc: PropTypes.func.isRequired,
    submitText: PropTypes.string.isRequired,
};

export default ProjectForm;
