import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

async function api({ method, url, data, headers = {}, message = false }) {
    const token = localStorage.getItem('token');
    if (token) headers.Authorization = `Bearer ${token}`;

    try {
        axios.defaults.withCredentials = true;
        const res = await axios.request({ method, url, data, headers });

        if (message)
            toast.success(
                <Box display="flex" alignItems="center">
                    <CheckCircleOutlineIcon />
                    <Typography variant="subtitle2" style={{ paddingLeft: 5 }}>
                        {res.data.message}
                    </Typography>
                </Box>
            );

        return {
            status: res.request.status,
            message: res.data.message,
            data: res.data.content,
        };
    } catch (error) {
        if (message)
            toast.error(
                <Box display="flex" alignItems="center">
                    <ErrorOutlineIcon />
                    <Typography variant="subtitle2" style={{ paddingLeft: 5 }}>
                        {error.response.data.message}
                    </Typography>
                </Box>
            );

        return {
            status: error.request.status,
            message: error.response.data.message,
            data: error.response.data.content,
        };
    }
}

export { api };
