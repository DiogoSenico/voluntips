### Documentação de Pipelines

## Preparação para CI (Gitlab)

Antes da utilização do Continous deployment do Gitlab é necessário que o repositório tenha associado a si as seguintes variaveis de ambiente:

-   NODE_ENV: qual o ambiente em que corre a app (dev, prod)
-   PORT: porta da aplicação (5000)
-   BASE_CLIENT_URL: Url base da aplicação
-   JWT_SECRET: secret para encriptação
-   JWT_OPTIONS_ALGORITHM: algoritmo para encriptação
-   DB_CONN: Connection String
-   INTERNAL_EMAIL_END: Sub dominio do email interno da instituição
-   SENDGRID_API_KEY: Chave de acesso do SendGrid
-   SENDGRID_FROM_EMAIL: Email de envio
-   SENDGRID_FROM_NAME: Nome do remetente
-   ADMIN_ACC_NAME: Nome do administrador base da aplicação
-   ADMIN_ACC_EMAIL: Email do administrador base da aplicação
-   ADMIN_ACC_PASSWORD: Password do administrador base da aplicação

## Preparação para CD (GitLab)

Previamente à utilização da funcionalidade de Continuous Deployment é necessário que a aplicação conte com as seguintes variaveis de ambiente:

-   DOCKER_HB_TOKEN: Token da conta docker hub
-   DOCKER_HUB_USER: User name da conta docker hub
-   GK_SERVICE_KEY: Chave de serviço da GCP encriptada em base64

## Utilização da pipeline

Depois de feitas as configurações anteriores será possivel realizar um push para o repositório o que irá despoletar as pipelines do GitLab.
A Pipeline utiliada é a apresentada na imagem seguinte:

![Pipelines](./images/pipeline.png)

Nesta pipeline existem 3 stages sendo eles:

-   Test: Neste stage pretende-se executar jobs relativos a testes na aplicação. Aqui temos o job:

    -   tests: Executa os testes de Integração e Unitários na aplicação

-   Build: Este stage tem por objetivo dar build na aplicação com base no DockerFile construido

    -   build_app: Executa os comandos necessários para realizar a build do docker

-   Deploy: No ultimo stage é onde será realizado o deploy da aplicação
    -   deploy_kubernetes: Neste ultimo job serão realizados os comandos necessários para efetuar o deployment da aplicação para o Google Cloud Platform utilizando o Kubernetes

## Diagrama Kubernetes

Quanto à arquitetura de kubernetes optamos por usar um cluster com uma node e um pod, por uma questão de poupança de fundos no GCP
Essa mesma arquitetura poderá ser visualizada no esquema seguinte:

![Pipelines](./images/diagram.png)
