# Voluntips
Api documentation for Voluntips

## Version: 1.0.0

### /auth/confirmation/{token}

#### GET
##### Summary:

Activate account with token

##### Description:

User receive link in email after register, That link will redirect the user to the client app and it will make a request to this endpoint to verify if the token is valid and then activates the account

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | path | User Activation Token | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User Activated |
| 401 | User is logged |
| 404 | User not found |
| 500 | Unknown error |

### /auth/recover/{token}

#### POST
##### Summary:

Verifies if the given token belongs to any user

##### Description:

User receive link in email after recover request, That link will redirect the user to the client app and it will make a request to this endpoint to verify if the token is valid

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | path | User Recovery Token | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User Recovering |
| 401 | User is logged / User not recovering |
| 500 | Unknown error |

### /auth/recoverVerified/

#### POST
##### Summary:

Changes que user password

##### Description:

Changes user password based on a given user token

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | User Token and new password | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User Recovering |
| 401 | User is logged / User not recovering |
| 500 | Unknown error |

### /auth/recover/

#### POST
##### Summary:

Makes a request to change

##### Description:

Makes a request to change the user email and sends an email to the corresponding user with a link to change the password

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | User Email | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User Recovering |
| 401 | User is logged |
| 404 | User not found |
| 500 | Unknown error |

### /auth/logout

#### POST
##### Summary:

Logs out an user

##### Description:

Clears the user session token

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User logged out |
| 401 | User not logged / Invalid token |
| 500 | Unknown error |

### /auth/login

#### POST
##### Summary:

Log in a User

##### Description:

Creates a token and puts it in a cookie

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | User Email and password | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Login successful |
| 401 | User is logged |
| 500 | Unknown error |

### /auth/register

#### POST
##### Summary:

Register a User

##### Description:

Registers an user in the platform and sends an email. If the user email ends with .ips.pt, the user will receive an email with an activation link otherwise the user will receive an email notifying that his account was sent to review by the staff team

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | User information | Yes | [User](#user) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | User Registered |
| 400 | Phone Number Required / BirthDate Required / Entity Required / Member Type Required / Reasons Require |
| 401 | User is logged |
| 409 | User already exists |
| 500 | Unknown error |

### /auth/

#### POST
##### Summary:

Verify if user is logged

##### Description:

Verifies if the user is logged in the platform and has the required permissions

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | User Role | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Login successful |
| 401 | User not logged / Invalid token |
| 500 | Unknown error |

### /user

#### GET
##### Summary:

Get all users

##### Description:

Get all users

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| search | query | Filtro de pesquisa | Yes | string |
| page | query | Página | Yes | string |
| pageSize | query | Nº de registos por página | Yes | string |
| userType | query | Tipo de utilizador | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Users found |
| 500 | Unknown error |

### /user/{id}

#### GET
##### Summary:

User Details

##### Description:

Get user by information

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User Found |
| 500 | Unknown error |

#### PUT
##### Summary:

Update user details

##### Description:

Update user details

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |
| body | body | New user details | Yes | [User](#user) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User Updated |
| 401 | Can not edit this profile |
| 404 | User not Found |
| 500 | Unknown error |

### /user/activate/{id}

#### POST
##### Summary:

Activate user account

##### Description:

Activate user account

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User activated |
| 404 | User not found |
| 500 | Unknown error |

### /user/deactivate/{id}

#### POST
##### Summary:

Deactivate user account

##### Description:

Deactivate user account

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User deactivated |
| 404 | User not found |
| 500 | Unknown error |

### /user/acceptexternal/{id}

#### POST
##### Summary:

Accept external user account

##### Description:

Accept external user account

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User deactivated |
| 404 | User not found |
| 500 | Unknown error |

### /user/rejectexternal/{id}

#### DELETE
##### Summary:

Reject external user account

##### Description:

Reject external user account

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | User deactivated |
| 404 | User not found |
| 500 | Unknown error |

### /user/manager

#### POST
##### Summary:

Create manager account

##### Description:

Create manager account

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | User details | Yes | [User](#user) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Manager Created |
| 409 | User already exists |
| 500 | Unknown error |

### /user/{id}/upload

#### POST
##### Summary:

Upload user profile image

##### Description:

Upload user profile image

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | User Id | Yes | string |
| file | formData | Image file to upload | Yes | file |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | File uploaded |
| 400 | No file found |
| 401 | Can not edit this profile |
| 404 | User not found |
| 500 | Unknown error |

### /user/{id}/volunteering/

#### GET
##### Summary:

Lists all user submited volunteering

##### Description:

Gets the list of the created/suggested volunteerings of the given user

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Projetos encontrados |
| 500 | Ocorreu um erro inesperado |

### /user/{id}/applications/

#### GET
##### Summary:

Lists all user volunteering applications

##### Description:

Gets the list of the volunteering applications of the given user

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| id | path | User Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Projetos encontrados |
| 500 | Ocorreu um erro inesperado |

### /volunteering/

#### POST
##### Summary:

Create or request a volunteering

##### Description:

Creates or requests a volunteering based on the user role

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| body | body | Volunteering details | Yes | [Project](#project) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Projeto Criado / Projeto enviado para análise |
| 401 | Indique o nome da entidade / Especifique a formação necessária |
| 500 | Ocorreu um erro inesperado |

#### GET
##### Summary:

Gets a volunteering list

##### Description:

Gets a volunteering list based on the search params

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| search | query | Filtro de pesquisa | No | string |
| page | query | Página | Yes | string |
| pageSize | query | Nº de registos por página | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Voluntariado encontrado |
| 500 | Ocorreu um erro inesperado |

### /volunteering/{id}/apply

#### POST
##### Summary:

Applies to a volunteering

##### Description:

Tries to apply the current user to the given volunteering

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| id | path | Volunteering Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Pedido enviado |
| 401 | Numero de vagas já foi atingido / Utilizador já inscrito |
| 500 | Unknown error |

### /volunteering/{vid}/user/{uid}/response

#### POST
##### Summary:

Apply Volunteering response

##### Description:

Responds if the user in the given volunteering was accepted or rejected

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| vid | path | Volunteering Id | Yes | string |
| uid | path | User Id | Yes | string |
| body | body | The response | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Resposta realizada |
| 401 | Utilizador já obteve resposta |
| 404 | Utilizador não se voluntariou / Utilizador não encontrado / Voluntariado não encontrado |
| 500 | Ocorreu um erro inesperado |

### /volunteering/{id}/response

#### POST
##### Summary:

Volunteering response

##### Description:

Responds when the volunteering was accepted or rejected

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| id | path | Volunteering Id | Yes | string |
| body | body | The response | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Resposta realizada |
| 401 | Voluntariado já obteve resposta |
| 404 | Voluntariado não encontrado |
| 500 | Ocorreu um erro inesperado |

### /donation/

#### POST
##### Summary:

Create or request a donation

##### Description:

Creates a Donation project

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| body | body | Donation details | Yes | [Project](#project) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Projeto Criado |
| 500 | Ocorreu um erro inesperado |

#### GET
##### Summary:

Gets a donation list

##### Description:

Gets a donation list based on the search params

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| search | query | Filtro de pesquisa | No | string |
| page | query | Página | Yes | string |
| pageSize | query | Nº de registos por página | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Doações encontradas |
| 500 | Ocorreu um erro inesperado |

### /donation/allActs

#### GET
##### Summary:

Gets all donation acts list

##### Description:

Gets a donation act list based on the search params

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| search | query | Filtro de pesquisa | No | string |
| page | query | Página | Yes | string |
| pageSize | query | Nº de registos por página | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Atos de Doação encontrados |
| 500 | Ocorreu um erro inesperado |

### /donation/{id}/donate

#### POST
##### Summary:

Applies to a donation

##### Description:

Makes a donation act

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| token | cookie | Token de sessão | No | string |
| id | path | Donation Id | Yes | string |
| body | body | Donation Id | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Doação Aceite |
| 404 | Doação não encontrada / Utilizador não encontrado |
| 500 | Ocorreu um erro inesperado |

### /product/

#### GET
##### Summary:

Gets a product list

##### Description:

Gets a product list

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Produtos encontrados |
| 500 | Ocorreu um erro inesperado |

### /product/{id}

#### PUT
##### Summary:

Update the project amount

##### Description:

Updates the project amount

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | Product Id | Yes | string |
| body | body | Product Stock to remove | Yes | object |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Produto Atualizado |
| 401 | Pedido excede o stock |
| 404 | Produtos não encontrado |
| 500 | Ocorreu um erro inesperado |

### /project/

#### GET
##### Summary:

Gets a project list

##### Description:

Gets a project list based on the search params

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| search | query | Filtro de pesquisa | No | string |
| page | query | Página | Yes | string |
| pageSize | query | Nº de registos por página | Yes | string |
| minDate | query | Data minima | No | date |
| maxDate | query | Data máxima | No | date |
| interestAreas | query | Áreas de interesse | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Projetos encontrados |
| 500 | Ocorreu um erro inesperado |

### /project/{id}

#### GET
##### Summary:

Project Details

##### Description:

Gets all the information about the project

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | Volunteering Id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Projeto encontrado |
| 404 | Projeto não encontrado |
| 500 | Ocorreu um erro inesperado |

#### PUT
##### Summary:

Project Update

##### Description:

Updates the project with the given id

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| id | path | Project Id | Yes | string |
| body | body | Project update details | Yes | [Project](#project) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Projeto atualizado / Projeto não sofreu alterações |
| 401 | Não pode editar este projeto |
| 404 | Projeto não encontrado |
| 500 | Ocorreu um erro inesperado |

### Models


#### User

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| name | string |  | No |
| email | string |  | No |
| password | string |  | No |
| phone | string |  | No |
| address | string |  | No |
| birthdate | string |  | No |
| course | string |  | No |
| observations | string |  | No |
| memberType | string |  | No |
| entity | string |  | No |
| reasons | [ string ] |  | No |
| interestAreas | [ string ] |  | No |

#### Project

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| entity | string |  | No |
| phone | string |  | No |
| email | string |  | No |
| userName | string |  | No |
| title | string |  | No |
| resume | string |  | No |
| interventionArea | string |  | No |
| targetAudience | string |  | No |
| objectives | string |  | No |
| description | string |  | No |
| formationType | string |  | No |
| mindate | date |  | No |
| maxdate | date |  | No |
| interestAreas | [ string ] |  | No |
| otherEntities | [ string ] |  | No |
| observations | string |  | No |
| autoAccept | boolean |  | No |
| maxVolunteers | integer |  | No |