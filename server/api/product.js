const express = require('express');

const { NeedsAuth, VerifyRole, response } = require('../utils/middleware');
const { roles } = require('../models/_constants');

const Product = require('../models/product');

var router = express.Router();

// DONATION LIST
router.get(
    '/product/',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            let projects = await Product.find({}).sort({ name: 'asc' });

            const data = {
                data: projects,
            };

            return response(res, 200, 'Produtos encontrados', data);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    }
);

// DONATION LISTs
router.put(
    '/product/:id',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            let product = await Product.findById(req.params.id);
            if (!product) return response(res, 404, 'Produtos não encontrado');

            if (product.totalAmount < req.body.amount)
                return response(res, 401, 'Pedido excede o stock');

            product.totalAmount -= req.body.amount;
            await Product.updateOne(
                { _id: req.params.id },
                { $set: { totalAmount: product.totalAmount } },
                { new: true }
            );

            const data = {
                data: product,
            };
            return response(res, 200, 'Produto Atualizado', data);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    }
);

module.exports = router;
