const express = require('express');
const _ = require('lodash');
const fs = require('fs');

const config = require('../../config');
const { NeedsAuth, VerifyRole, response } = require('../utils/middleware');
const { roles } = require('../models/_constants');
const _constants = require('../models/_constants');
const { generateAlphaNumericString } = require('../utils/utils');
const notify = require('../notifications/notify');

const User = require('../models/user');
const Project = require('../models/project');

var router = express.Router();

// GET ACCOUNT CONSTANTS
router.post('/user/constants/', async (req, res) => {
    try {
        if (['roles', 'accountStatus'].includes(req.body.type))
            throw 'Can not access this types';
        const data = _constants[req.body.type];
        if (!data) return response(res, 404, 'Dados não encontrados');
        return response(res, 200, 'Dados encontrados', data);
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});

// GET USER BY ID
router.get(
    '/user/:id',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            //PROFILE
            if (req.user._id === req.params.id) {
                delete req.user.password;
                return response(res, 200, 'Utilizador Encontrado', req.user);
            }

            //DIFERENT USER
            const user = await User.findById(req.params.id).lean();
            if (user) {
                delete user.activationToken;
                delete user.password;
                return response(res, 200, 'Utilizador Encontrado', user);
            }

            return response(res, 404, 'Utilizador não encontrado', user);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// UPDATE USER BY ID
router.put(
    '/user/:id',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            if (req.user._id != req.params.id) {
                return response(res, 401, 'Não pode editar este perfil');
            }

            let data = {
                name: req.body.name,
                image: req.body.image,
                phone: req.body.phone,
                address: req.body.address,
                birthdate: req.body.birthdate,
                school: req.body.school,
                course: req.body.course,
                entity: req.body.entity,
                service: req.body.service,
                observations: req.body.observations,
                accountStatus: req.body.accountStatus,
                memberType: req.body.memberType,
                reasons: req.body.reasons,
                interestAreas: req.body.interestAreas,
            };

            data = _.omitBy(data, _.isNil);
            data = _.omitBy(data, _.isUndefined);
            data = _.omitBy(data, _.isEmpty);

            const user = await User.updateOne(
                { _id: req.user._id },
                {
                    $set: data,
                },
            ).lean();
            delete req.user.activationToken;
            delete req.user.role;
            delete req.user.password;

            if (user.n) {
                return response(res, 200, 'Utilizador atualizado', req.user);
            } else {
                return response(
                    res,
                    404,
                    'Utilizador não encontrado',
                    req.user,
                );
            }
        } catch (err) {
            response(res, 500, 'Ocorreu um erro inesperado', err);
        }
    },
);

async function getUserByRole(sch, page, pageSize, role) {
    const search = new RegExp(`${sch}`, 'gi');
    const skip = Number(page) * Number(pageSize);
    const limit = Number(pageSize);

    const query = {
        $or: [
            { name: search },
            { email: search },
            { course: search },
            { memberType: search },
            { entity: search },
        ],
    };
    if (role) query.role = role;
    const users = await User.find(
        query,
        '-password -activationToken -address -birthdate -observations',
        { skip, limit },
    ).sort({ name: 'asc' });

    return {
        page: Number(page),
        totalCount: await User.countDocuments(query),
        data: users,
    };
}

// GET ALL USERS
router.get(
    '/user',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const data = await getUserByRole(
                req.query.search,
                req.query.page,
                req.query.pageSize,
                req.query.userType,
            );

            return response(res, 200, 'Utilizadores encontrados', data);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

async function activateAccount(userid) {
    const user = await User.findByIdAndUpdate(userid, {
        accountStatus: 'ACTIVE',
    }).lean();

    if (!user) return;

    user.accountStatus = 'ACTIVE';
    delete user.password;
    return user;
}

async function deactivateAccount(userid) {
    const user = await User.findByIdAndUpdate(userid, {
        accountStatus: 'INACTIVE',
    }).lean();

    if (!user) return;

    user.accountStatus = 'INACTIVE';
    delete user.password;
    return user;
}

// ACTIVATE ACCOUNT
router.post(
    '/user/activate/:id',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const activeUser = await activateAccount(req.params.id);
            if (!activeUser)
                return response(res, 404, 'Utilizador não encontrado');

            return response(res, 200, 'Utilizador ativado', activeUser);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// DEACTIVATE ACCOUNT
router.post(
    '/user/deactivate/:id',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const inactiveUser = await deactivateAccount(req.params.id);
            if (!inactiveUser)
                return response(res, 404, 'Utilizador não encontrado');

            response(res, 200, 'Utilizador desativado', inactiveUser);
        } catch (err) {
            response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// ACCEPT EXTERNAL ACCOUNT
router.post(
    '/user/acceptexternal/:id',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const activeUser = await activateAccount(req.params.id);
            if (!activeUser)
                return response(res, 404, 'Utilizador não encontrado');

            const content = {
                name: activeUser.name,
            };
            if (!notify(activeUser.email, 'ACCOUNT_WAS_ACTIVATED', content))
                throw 'Email não enviado';

            return response(res, 200, 'Utilizador ativado', activeUser);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// REJECT EXTERNAL ACCOUNT
router.delete(
    '/user/rejectexternal/:id',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const pendingUser = await User.findOne({
                _id: req.params.id,
                accountStatus: 'PENDING',
                isExternal: true,
            });
            if (!pendingUser)
                return response(res, 404, 'Utilizador não encontrado');

            const user = await User.findByIdAndRemove(req.params.id);

            const content = {
                name: user.name,
            };
            if (!notify(user.email, 'ACCOUNT_REJECTED', content))
                throw 'Email não enviado';

            return response(res, 200, 'Utilizador rejeitado e apagado', user);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// ADD MANAGER
router.post(
    '/user/manager/',
    [NeedsAuth, VerifyRole(roles.ADMIN)],
    async (req, res) => {
        const user = new User(req.body);

        try {
            const userExists = await User.findOne({ email: user.email });
            if (userExists) return response(res, 409, 'Utilizador já existe');

            const password = generateAlphaNumericString(8);
            user.password = password;
            user.accountStatus = 'ACTIVE';
            user.role = 'MANAGER';

            const err = user.validateSync();

            if (err) throw err;

            const content = {
                name: user.name,
                email: user.email,
                password: password,
            };

            if (!notify(user.email, 'MANAGER_CREATED', content))
                throw 'Email não enviado';

            await user.save();
            delete user.password;
            return response(res, 201, 'Gestor criado', user);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

router.post(
    '/user/:id/upload',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            if (req.user._id != req.params.id) {
                return response(res, 401, 'Não pode editar este perfil');
            }

            if (!req.files || Object.keys(req.files).length === 0) {
                return response(res, 400, 'Imagem não encontrada');
            }

            if (req.user.image) {
                fs.unlinkSync(
                    `${config.PROJECT_DIR}/server${req.user.image}`,
                    (err) => {
                        if (err)
                            console.log(
                                'Não foi possivel apagar a imagem antiga do servidor',
                            );
                    },
                );
            }

            const file = req.files.file;
            const fileName = file.md5 + '.' + file.name.split('.')[1];
            file.mv(
                `${config.PROJECT_DIR}/server/uploads/${fileName}`,
                async (err) => {
                    if (err) throw err;

                    const user = await User.updateOne(
                        { _id: req.user._id },
                        {
                            image: `/uploads/${fileName}`,
                        },
                    ).lean();
                    if (!user) response(res, 404, 'Utilizador não encontrado');
                    delete user.activationToken;
                    delete user.role;
                    delete user.password;

                    user.image = `/uploads/${fileName}`;

                    return response(res, 200, 'Imagem guardada', {
                        filePath: `/uploads/${fileName}`,
                    });
                },
            );
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// GET ALL USER PROPOSED VOLUNTEERINGS
router.get(
    '/user/:id/volunteering/',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const user = await User.findById(req.params.id).lean();
            if (!user)
                return response(res, 404, 'Utilizador não encontrado', user);

            const query = {
                $and: [
                    { type: 'VOLUNTEERING' },
                    { proposedBy: user },
                    { phone: { $ne: '963852741' } },
                ],
            };

            const projects = await Project.find(
                query,
                'entity title resume date volunteers maxVolunteers proposedBy status',
            );

            return response(res, 200, 'Projetos encontrados', projects);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);
// GET ALL USER PROPOSED VOLUNTEERINGS
router.get(
    '/user/:id/applications/',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const user = await User.findById(req.params.id).lean();
            if (!user)
                return response(res, 404, 'Utilizador não encontrado', user);

            const query = {
                $and: [
                    { type: 'VOLUNTEERING' },
                    { 'volunteers.user': user },
                    { phone: { $ne: '963852741' } },
                ],
            };

            const projects = await Project.find(
                query,
                'entity title resume date volunteers maxVolunteers proposedBy',
            ).lean();

            projects.forEach((p) => {
                p.userApplication = _.find(
                    p.volunteers,
                    (v) => v.user.toString() === user._id.toString(),
                );
            });

            return response(res, 200, 'Projetos encontrados', projects);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

module.exports = router;
