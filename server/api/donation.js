const express = require('express');
const mongoose = require('mongoose');

const { NeedsAuth, VerifyRole, response } = require('../utils/middleware');
const { roles } = require('../models/_constants');
const notify = require('../notifications/notify');

const Project = require('../models/project');
const Product = require('../models/product');
const User = require('../models/user');
const Donation = require('../models/donation');

var router = express.Router();

// CREATE DONATION
router.post(
    '/donation/',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const project = new Project(req.body);
            project.type = 'DONATION';
            project.proposedBy = req.user.id;
            project.acceptedBy = req.user.id;
            project.isExternal = false;
            project.status = 'ACCEPTED';
            project.donations = [];

            const err = project.validateSync();
            if (err) throw err;

            await project.save();
            return response(res, 201, 'Projeto Criado');
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// DONATION LIST
router.get(
    '/donation/',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const search = new RegExp(`${req.query.search || '.*'}`, 'gi');
            const skip = Number(req.query.page) * Number(req.query.pageSize);
            const limit = Number(req.query.pageSize);

            let query = {
                $and: [
                    {
                        $or: [
                            { entity: search },
                            { email: search },
                            { userName: search },
                            { title: search },
                            { resume: search },
                        ],
                    },
                    { type: 'DONATION' },
                    { phone: { $ne: '963852741' } },
                ],
            };

            let projects = await Project.find(
                query,
                'entity userName title date status volunteers createdAt',
                { skip, limit },
            ).sort({ mindate: 'desc' });

            const data = {
                page: Number(req.query.page),
                totalCount: await Project.countDocuments(query),
                data: projects,
            };

            return response(res, 200, 'Doações encontradas', data);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// DONATION LIST
router.get(
    '/donation/allActs',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const search = new RegExp(`${req.query.search || '.*'}`, 'gi');
            const skip = Number(req.query.page) * Number(req.query.pageSize);
            const limit = Number(req.query.pageSize);

            let query = {
                $and: [
                    {
                        $or: [
                            { entity: search },
                            { email: search },
                            { userName: search },
                            { title: search },
                            { resume: search },
                        ],
                    },
                    { type: 'DONATION' },
                    { phone: { $ne: '963852741' } },
                ],
            };

            let projects = await Project.find(query, 'donations', {
                skip,
                limit,
            }).sort({ mindate: 'desc' });

            let allDonations = [];
            for (const proj of projects) {
                for (const donId of proj.donations) {
                    const donation = await Donation.findOne(donId).lean();
                    donation.donator = await User.findById(
                        donation.donator,
                        'name',
                    ).lean();
                    donation.registeredBy = await User.findById(
                        donation.registeredBy,
                        'name',
                    ).lean();
                    allDonations.push(donation);
                }
            }

            const data = {
                page: Number(req.query.page),
                totalCount: allDonations.length,
                data: allDonations,
            };

            return response(res, 200, 'Atos de Doação encontrados', data);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// MAKE DONATION
router.post(
    '/donation/:id/donate',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            let project = await Project.findOne({
                _id: req.params.id,
                type: 'DONATION',
            });
            if (!project) return response(res, 404, 'Doação não encontrada');

            const user = await User.findById(req.body.userId);
            if (!user) return response(res, 404, 'Utilizador não encontrado');

            const donationProducts = [];
            for (const p of req.body.products) {
                if (p.amount <= 0) continue;
                const product = await Product.findOneAndUpdate(
                    { name: p.name },
                    { $inc: { totalAmount: p.amount } },
                    { upsert: true, new: true },
                );
                await product.save();
                donationProducts.push({
                    product: mongoose.Types.ObjectId(product._id),
                    amount: p.amount,
                });
            }

            const donationAct = new Donation({
                donator: user._id,
                registeredBy: req.user._id,
                products: donationProducts,
            });
            await donationAct.save();
            const err = donationAct.validateSync();
            if (err) throw err;

            await donationAct.save();

            project = await Project.findByIdAndUpdate(project._id, {
                $push: { donations: donationAct },
            });

            let content = {
                name: user.name,
                donationTitle: project.title,
                products: req.body.products,
            };
            if (!notify(user.email, 'DONATION_ACT_REGISTERED', content))
                throw 'Email não enviado';

            return response(res, 200, 'Doação Aceite');
        } catch (err) {
            console.log(err);
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

module.exports = router;
