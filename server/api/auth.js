const bcrypt = require('bcryptjs');
const express = require('express');
const jwt = require('jsonwebtoken');

const _ = require('lodash');

const config = require('../../config');
const { DontNeedsAuth, NeedsAuth, response } = require('../utils/middleware');
const { generateAlphaNumericString } = require('../utils/utils');
const notify = require('../notifications/notify');

const User = require('../models/user');

var router = express.Router();

router.get('/auth/confirmation/:token', [DontNeedsAuth], async (req, res) => {
    try {
        const { token } = req.params;
        const user = await User.findOneAndUpdate(
            { activationToken: token },
            { accountStatus: 'ACTIVE', $unset: { activationToken: 1 } }
        );
        if (user) {
            return response(res, 200, 'Utiliador ativado');
        } else {
            return response(res, 404, 'Utilizador não encontrado');
        }
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});
router.post('/auth/recover/:token', [DontNeedsAuth], async (req, res) => {
    try {
        const { token } = req.params;
        const user = await User.findOne({ passwordToken: token }).lean();
        if (user) {
            return response(res, 200, 'Utilizador em recuperação');
        } else {
            return response(res, 401, 'Utilizador não está em recuperação');
        }
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});
router.post('/auth/recoverVerified/', [DontNeedsAuth], async (req, res) => {
    try {
        const { password, token } = req.body;

        if (!password || !token) return response(res, 401, 'Data not sent');

        const user = await User.findOneAndUpdate(
            { passwordToken: token },
            { password: password, $unset: { passwordToken: 1 } }
        );
        if (user) {
            return response(res, 200, 'Password atualizada');
        } else {
            return response(res, 401, 'Não foi possivel atualizar a password');
        }
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});

router.post('/auth/recover/', [DontNeedsAuth], async (req, res) => {
    try {
        const { email } = req.body;
        const user = await User.findOne({ email }).lean();

        if (!user) {
            return response(res, 404, 'Utilizador não encontrado');
        }
        if (user.accountStatus !== 'ACTIVE') {
            return response(res, 401, 'Utilizador não ativo');
        }
        if (user.passwordToken) {
            return response(res, 401, 'Recuperação de password ativa');
        }

        const passwordToken = generateAlphaNumericString(32);
        const userToken = await User.findByIdAndUpdate(user._id, {
            passwordToken,
        }).lean();

        const content = {
            name: userToken.name,
            link: `${config.baseUrl}/recover/${passwordToken}`,
        };
        if (!notify(user.email, 'RECOVER_ACCOUNT', content))
            throw { message: 'Email não enviado' };

        delete user.password;
        delete user.activationToken;
        delete user.role;
        return response(res, 200, 'Recuperação bem sucedida', {
            user,
        });
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});

router.post('/auth/logout', [NeedsAuth], (req, res) => {
    res.clearCookie('token');
    return response(res, 200, 'Sessão terminada');
});

router.post('/auth/login', [DontNeedsAuth], async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email }).lean();

        if (!user) {
            return response(res, 401, 'Email ou password inválidos');
        }

        if (user.accountStatus !== 'ACTIVE') {
            return response(res, 401, 'Conta não ativa');
        }

        const isPassword = await bcrypt.compare(password, user.password);
        if (!isPassword) {
            return response(res, 401, 'Email ou password inválidos');
        }

        const token = jwt.sign({ id: user._id }, config.jwt.secret);
        delete user.password;

        res.cookie('token', token, { httpOnly: true });
        return response(res, 200, 'Sessão iniciada', {
            user,
        });
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err);
    }
});

router.post('/auth/register', [DontNeedsAuth], async (req, res) => {
    let data = req.body;
    data = _.omitBy(data, _.isNil);
    data = _.omitBy(data, _.isUndefined);
    data = _.omitBy(data, _.isEmpty);

    const user = new User(data);

    try {
        const userExists = await User.findOne({ email: user.email });
        if (userExists) return response(res, 409, 'Utilizador já existe');

        const err = user.validateSync();

        if (err) throw err;

        if (!user.phone) return response(res, 400, 'Telemovél necessário');
        if (!user.birthdate)
            return response(res, 400, 'Data de Nascimento necessária');
        if (!user.memberType)
            return response(
                res,
                400,
                'Tipo de membro da comunidade necessário'
            );
        if (
            (user.memberType === 'STUDENT' ||
                user.memberType === 'GRATUATE' ||
                user.memberType === 'TEACHER') &&
            !user.school
        )
            return response(res, 400, 'Escola necessária');
        if (
            (user.memberType === 'STUDENT' || user.memberType === 'GRATUATE') &&
            !user.course
        )
            return response(res, 400, 'Curso necessário');
        if (
            (user.memberType === 'SCHOLAR' ||
                user.memberType === 'NON_TEACHER') &&
            !user.service
        )
            return response(res, 400, 'Serviço necessário');
        if (user.memberType === 'ENTITY' && !user.entity)
            return response(res, 400, 'Entidade necessária');
        /*
        if (!user.reasons.length)
            return response(res, 400, 'Razões necessárias');
        if (!user.interestAreas.length)
            return response(res, 400, 'Areas de interesse necessárias');
        */
        if (!user.email.endsWith(config.internalEmailEnd)) {
            // IS AN EXTERNAL USER
            user.isExternal = true;
        } else {
            // IS A MEMBER OF THE COMMUNITY
            user.activationToken = generateAlphaNumericString(32);
        }

        delete user.password;
        delete user.activationToken;

        let content = {
            name: user.name,
        };
        if (!user.isExternal) {
            content.confirmationLink = `${config.baseUrl}/activateaccount/${user.activationToken}`;
            if (!notify(user.email, 'REGISTER_ACTIVE', content))
                throw { message: 'Email não enviado' };
        } else {
            user.accountStatus = 'PENDING';
            if (!notify(user.email, 'REGISTER_NOT_ACTIVE', content))
                throw { message: 'Email não enviado' };
        }

        await user.save();
        return response(res, 201, 'Utilizador registado', user);
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});

router.post('/auth', [NeedsAuth], (req, res) => {
    const { role } = req.body;
    delete req.user.password;
    delete req.user.activationToken;

    if (role === 'USER') {
        return response(res, 200, 'Sessão ativa', req.user);
    } else if (
        role === 'MANAGER' &&
        (req.user.role === 'MANAGER' || req.user.role === 'ADMIN')
    ) {
        return response(res, 200, 'Sessão ativa', req.user);
    } else if (role === 'ADMIN' && req.user.role === 'ADMIN') {
        return response(res, 200, 'Sessão ativa', req.user);
    }
    return response(res, 401, 'Utilizador não tem permissões');
});

module.exports = router;
