const express = require('express');
const _ = require('lodash');

const { NeedsAuth, VerifyRole, response } = require('../utils/middleware');
const { roles } = require('../models/_constants');
const notify = require('../notifications/notify');

const Project = require('../models/project');
const User = require('../models/user');

var router = express.Router();

// CREATE PROJECT
router.post(
    '/volunteering/',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const project = new Project(req.body);
            project.type = 'VOLUNTEERING';
            project.proposedBy = req.user.id;
            if (req.user.role !== 'USER') {
                project.acceptedBy = req.user.id;
                project.isExternal = false;
                project.status = 'ACCEPTED';
            }

            if (project.isExternal && project.entity === '')
                return response(res, 401, 'Indique o nome da entidade');

            if (project.needsFormation && project.formationType === '')
                return response(res, 401, 'Especifique a formação necessária');

            const err = project.validateSync();
            if (err) throw err;

            if (project.isExternal) {
                const content = {
                    name: req.user.name,
                };
                if (!notify(req.user.email, 'VOLUNTEERING_REQUEST', content))
                    throw 'Email não enviado';
                await project.save();
                return response(res, 201, 'Projeto enviado para análise');
            }

            await project.save();
            return response(res, 201, 'Projeto Criado');
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// VOLUNTEERING LIST
router.get(
    '/volunteering/',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const search = new RegExp(`${req.query.search || '.*'}`, 'gi');
            const skip = Number(req.query.page) * Number(req.query.pageSize);
            const limit = Number(req.query.pageSize);

            let query = {
                $and: [
                    {
                        $or: [
                            { entity: search },
                            { email: search },
                            { userName: search },
                            { title: search },
                            { resume: search },
                        ],
                    },
                    { type: 'VOLUNTEERING' },
                    { phone: { $ne: '963852741' } },
                ],
            };

            let projects = await Project.find(
                query,
                'entity userName title date status volunteers createdAt',
                { skip, limit },
            ).sort({ mindate: 'desc' });

            const data = {
                page: Number(req.query.page),
                totalCount: await Project.countDocuments(query),
                data: projects,
            };

            return response(res, 200, 'Voluntariados encontrados', data);
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// REQUEST TO JOIN VOLUNTEERING
router.post(
    '/volunteering/:id/apply',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const volunteering = await Project.findById(req.params.id);

            const currentVolunteers = volunteering.volunteers.filter(
                (v) => v.status !== 'REJECTED' || v.status !== 'PENDDING',
            ).length;

            if (volunteering.maxVolunteers <= currentVolunteers)
                return response(res, 401, 'Número de vagas já foi atingido');

            const exists =
                _.findIndex(
                    volunteering.volunteers,
                    (v) => v.user.toString() === req.user._id.toString(),
                ) !== -1;

            if (exists) return response(res, 401, 'Utilizador já inscrito');

            let volunteer = {
                status: 'PENDDING',
                user: req.user,
            };
            if (volunteering.autoAccept) volunteer.status = 'ACCEPTED';

            let content = {
                name: req.user.name,
                volunteeringTitle: volunteering.title,
            };
            if (volunteer.status === 'PENDDING') {
                if (!notify(req.user.email, 'VOLUNTEERING_TO_REQUEST', content))
                    throw 'Email não enviado';
            } else if (volunteer.status === 'ACCEPTED') {
                content.result = true;
                if (!notify(req.user.email, 'VOLUNTEERING_TO_RESULT', content))
                    throw 'Email não enviado';
            }

            volunteering.volunteers.push(volunteer);
            volunteering.save();

            return response(res, 200, 'Pedido enviado');
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// REQUEST TO JOIN VOLUNTEERING RESULTS
router.post(
    '/volunteering/:vid/user/:uid/response',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const volunteering = await Project.findById(req.params.vid);
            if (!volunteering)
                return response(res, 404, 'Voluntariado não encontrado');

            const user = await User.findById(req.params.uid);
            if (!user) return response(res, 404, 'Utilizador não encontrado');

            const volunteerIndex = _.findIndex(
                volunteering.volunteers,
                (v) => v.user.toString() === user._id.toString(),
            );
            if (volunteerIndex === -1)
                return response(res, 404, 'Utilizador não se voluntariou');

            volunteering.volunteers[volunteerIndex].status = req.body.isAccepted
                ? 'ACCEPTED'
                : 'REJECTED';

            const content = {
                name: user.name,
                volunteeringTitle: volunteering.title,
                result: req.body.isAccepted,
                hasMotive: !!req.body.motive,
                motive: req.body.motive,
            };
            if (!notify(user.email, 'VOLUNTEERING_TO_RESULT', content))
                throw 'Email não enviado';

            volunteering.save();

            if (req.body.isAccepted)
                return response(res, 200, 'Inscrição aceite com sucesso');
            return response(res, 200, 'Inscrição recusada com sucesso');
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

// REQUEST VOLUNTEERING RESULTS
router.post(
    '/volunteering/:id/response',
    [NeedsAuth, VerifyRole(roles.MANAGER)],
    async (req, res) => {
        try {
            const volunteering = await Project.findOne({
                _id: req.params.id,
                type: 'VOLUNTEERING',
            });
            if (!volunteering)
                return response(res, 404, 'Voluntariado não encontrado');

            if (volunteering.status !== 'PENDDING')
                return response(res, 401, 'Voluntariado já obteve resposta');

            volunteering.status = req.body.isAccepted ? 'ACCEPTED' : 'REJECTED';

            const user = await User.findById(volunteering.proposedBy).lean();
            let content = {
                name: user.name,
                volunteeringTitle: volunteering.title,
                result: req.body.isAccepted,
                hasMotive: !!req.body.motive,
                motive: req.body.motive,
            };
            if (!notify(user.email, 'VOLUNTEERING_RESULT', content))
                throw 'Email não enviado';

            volunteering.save();

            return response(
                res,
                200,
                'Estado do projeto atualizado com sucesso',
            );
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

module.exports = router;
