const express = require('express');

const { NeedsAuth, VerifyRole, response } = require('../utils/middleware');
const { roles } = require('../models/_constants');

const Project = require('../models/project');
const Donation = require('../models/donation');
const User = require('../models/user');

var router = express.Router();

// GET PROJECT LIST
router.get('/project/', async (req, res) => {
    try {
        const search = new RegExp(`${req.query.search || '.*'}`, 'gi');
        const skip = Number(req.query.page) * Number(req.query.pageSize);
        const limit = Number(req.query.pageSize);
        const interestAreas = !req.query.interestAreas
            ? []
            : req.query.interestAreas.split(',');

        const minDate = req.query.minDate
            ? new Date(req.query.minDate)
            : new Date();
        const maxDate = req.query.maxDate
            ? new Date(req.query.maxDate)
            : new Date(new Date().getFullYear() + 10, 0);

        let query = {
            $and: [
                {
                    $or: [
                        { entity: search },
                        { email: search },
                        { userName: search },
                        { title: search },
                        { resume: search },
                    ],
                },
                { mindate: { $lt: maxDate } },
                { maxdate: { $gte: minDate } },
                { status: 'ACCEPTED' },
            ],
        };
        if (interestAreas.length)
            query.$and.push({ interestAreas: { $all: interestAreas } });

        let projects = await Project.find(
            query,
            'type entity userName title resume mindate maxdate donations volunteers interestAreas interventionArea maxVolunteers',
            { skip, limit },
        ).sort({ mindate: 'asc' });
        console.log(projects);

        const data = {
            page: Number(req.query.page),
            totalCount: await Project.countDocuments(query),
            data: projects,
        };

        return response(res, 200, 'Projetos encontrados', data);
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});

router.get('/project/:id', async (req, res) => {
    try {
        const project = await Project.findById(req.params.id).lean();
        if (!project) return response(res, 404, 'Projeto não encontrado');

        if (project.volunteers) {
            for (let i = 0; i < project.volunteers.length; i++) {
                const userId = project.volunteers[i].user;
                const user = await User.findById(
                    userId,
                    'name email phone address memberType',
                );
                project.volunteers[i].user = user;
            }
        }
        if (project.donations) {
            for (let i = 0; i < project.donations.length; i++) {
                const donationId = project.donations[i];
                const donation = await Donation.findById(donationId);
                const user = await User.findById(donation.donator, 'name');
                donation.donator = user;
                project.donations[i] = donation;
            }

            project.donations.sort((a, b) => {
                return new Date(b.created_at) - new Date(a.created_at);
            });
        }

        return response(res, 200, 'Projeto encontrado', project);
    } catch (err) {
        return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
    }
});

router.put(
    '/project/:id',
    [NeedsAuth, VerifyRole(roles.USER)],
    async (req, res) => {
        try {
            const project = await Project.findById(req.params.id).lean();
            if (!project) return response(res, 404, 'Projeto não encontrado');

            if (req.user._id.toString() !== project.proposedBy.toString()) {
                return response(res, 401, 'Não pode editar este projeto');
            }

            let data = {
                entity: req.body.entity,
                phone: req.body.phone,
                email: req.body.email,
                userName: req.body.userName,
                title: req.body.title,
                resume: req.body.resume,
                interventionArea: req.body.interventionArea,
                targetAudience: req.body.targetAudience,
                objectives: req.body.objectives,
                description: req.body.description,
                formationType: req.body.formationType,
                mindate: req.body.mindate,
                maxdate: req.body.maxdate,
                interestAreas: req.body.interestAreas,
                otherEntities: req.body.otherEntities,
                observations: req.body.observations,
                autoAccept: req.body.autoAccept,
                maxVolunteers: req.body.maxVolunteers,
            };

            // data = _.omitBy(data, _.isNil);
            // data = _.omitBy(data, _.isUndefined);
            // data = _.omitBy(data, _.isEmpty);

            const proj = await Project.updateOne(
                { _id: project._id },
                {
                    $set: data,
                },
            ).lean();

            if (proj.n) {
                return response(res, 200, 'Projeto atualizado', project);
            } else {
                return response(
                    res,
                    200,
                    'Projeto não sofreu alterações',
                    project,
                );
            }
        } catch (err) {
            return response(res, 500, 'Ocorreu um erro inesperado', err.errors);
        }
    },
);

module.exports = router;
