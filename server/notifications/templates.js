module.exports = {
    REGISTER_ACTIVE: {
        subject: 'A sua conta foi criada com sucesso',
        body: `
Caro(a) {{name}},<br>
<br>
Obrigado por se registar na plataforma VOLUNTIPS e por ser um possivel candidato a se juntar às causas organizadas pelo Instituto Politécnico de Setúbal<br>
<br>
Só falta mais um passo para fazer parte da nossa plataforma, clique <a href="{{{confirmationLink}}}">aqui</a> para confirmar a sua conta e iniciar sessão<br>
`,
    },
    REGISTER_NOT_ACTIVE: {
        subject: 'A sua conta foi criada e aguarda aprovação',
        body: `
Caro(a) {{name}},<br>
<br>
Obrigado por se registar na plataforma VOLUNTIPS e por ser um possivel candidato a se juntar às causas organizadas pelo Instituto Politécnico de Setúbal<br>
<br>
Uma vez que a sua conta é externa ao IPS pedimos que aguarde até que os seus dados sejam revistos pela gestão da plataforma.<br>
Tentaremos ser o mais breves possivel, obrigado.<br>
`,
    },
    ACCOUNT_WAS_ACTIVATED: {
        subject: 'A sua conta foi ativada',
        body: `
Caro(a) {{name}},<br>
<br>
Enviamos este email para o informar que a sua conta foi ativada com sucesso na VOLUNTIPS!<br>
Já pode aceder à plataforma e usufuir das funcionalidades que temos disponiveis.<br>

`,
    },
    MANAGER_CREATED: {
        subject: 'É agora um gestor da VOLUNTIPS',
        body: `
Caro(a) {{name}},<br>
<br>
Enviamos este email para o informar que o senhor(a) foi adicionado à plataforma VOLUNTIPS como o cargo de gestor<br>
Para poder aceder às funcionalidades de gestão da plataforma, faça login com os seguintes dados.<br>
<br>
Email: {{email}}<br>
Password: {{password}}<br>
`,
    },
    ACCOUNT_REJECTED: {
        subject: 'A sua conta foi recusada',
        body: `
Caro(a) {{name}},<br>
<br>
Enviamos este email para o informar que a sua conta foi recusada para a entrada na plataforma da VOLUNTIPS.<br>
Caso este não seja o resultado esperado, entre em contacto com os orgãos responsaveis do Instituto Politécnico de Setúbal<br>
`,
    },
    RECOVER_ACCOUNT: {
        subject: 'Pedido de recuperação de conta',
        body: `
Caro(a) {{name}},<br>
<br>
Enviamos-lhe este email, pois foi feito um pedido para recuperação de conta.<br>
Se o pedido foi feito por si carregue <a href="{{link}}">aqui</a>.<br>
<br>
Caso o pedido não foi feito por si, por favor contacte o administrador do sistema<br>
`,
    },
    VOLUNTEERING_TO_REQUEST: {
        subject: 'Pedido de participação em voluntariado',
        body: `
Caro(a) {{name}},<br>
<br>
Obrigado por querer participar no voluntariado "{{volunteeringTitle}}"!<br>
<br>
O seu perfil será analisado e em breve enviaremos a resposta indicando se o seu perfil foi aceite ou recusado para a participação

`,
    },
    VOLUNTEERING_TO_RESULT: {
        subject: 'Resultado do pedido de participação em voluntariado',
        body: `
Caro(a) {{name}},<br>
<br>
Obrigado por querer participar no voluntariado "{{volunteeringTitle}}"!<br>
<br>
Após a análise do seu perfil temos então:<br>
<br>
Resultado: {{#result}}Aceite{{/result}}{{^result}}Recusado{{/result}}<br>
<br>
{{#hasMotive}}Motivo: {{/hasMotive}}{{{motive}}}<br>

`,
    },
    VOLUNTEERING_REQUEST: {
        subject: 'Pedido de voluntariado',
        body: `
Caro(a) {{name}},<br>

Obrigado pela submissão de um projeto de voluntariado!<br>
<br>
O seu projeto foi submetido para análise. Dentro em breve terá uma resposta por parte da plataforma com o resultado referente à análise do seu pedido.
        `,
    },
    VOLUNTEERING_RESULT: {
        subject: 'Resultado do pedido de voluntariado',
        body: `
Caro(a) {{name}},<br>

Obrigado pela submissão de um projeto de voluntariado com o nome {{volunteeringTitle}}!<br>
<br>
Após a análise do projeto submetido o Instituto Politécnico de Setúbal chegou temos então:<br>
<br>
Resultado: {{#result}}Aceite{{/result}}{{^result}}Recusado{{/result}}<br>
<br>
{{#hasMotive}}Motivo: {{/hasMotive}}{{{motive}}}<br>
        `,
    },
    DONATION_ACT_REGISTERED: {
        subject: 'A sua doação foi registada',
        body: `
Caro(a) {{name}},<br>
<br>
O Instituto Politécnico de Setúbal agradece por ter doado para a causa "{{donationTitle}}"<br>
<br>
Os seus bens ficarão à nossa responsabilidade<br>
Bens Doados:<br>
{{#products}}
- {{amount}}un de {{name}}<br>
{{/products}}
<br>
`,
    },
};
