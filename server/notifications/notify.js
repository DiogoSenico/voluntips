const sgMail = require('@sendgrid/mail');
const mustache = require('mustache');

const config = require('../../config');
const templates = require('./templates');

sgMail.setApiKey(config.sendgrid.apikey);

module.exports = async (to, type, content) => {
    try {
        const msg = {
            to,
            from: config.sendgrid.from,
            subject: templates[type].subject,
            html: mustache.render(templates[type].body, content),
        };
        await sgMail.send(msg);
        return true;
    } catch (e) {
        return false;
    }
};
