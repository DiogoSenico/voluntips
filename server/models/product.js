const { Schema, model } = require('mongoose');

const ProductSchema = new Schema(
    {
        name: { type: String },
        totalAmount: { type: Number },
    },
    { timestamps: { updatedAt: 'updated_at' } }
);

const Product = model('Product', ProductSchema);
module.exports = Product;
