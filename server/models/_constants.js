module.exports = {
    roles: {
        ADMIN: {
            level: 1,
            name: 'Admin',
        },
        MANAGER: {
            level: 2,
            name: 'Manager',
        },
        USER: {
            level: 3,
            name: 'User',
        },
    },
    accountStatus: ['PENDING', 'ACTIVE', 'INACTIVE'],
    projectTypes: {
        VOLUNTEERING: {
            text: 'Voluntariado',
        },
        DONATION: {
            text: 'Doação',
        },
    },
    memberTypes: {
        STUDENT: {
            text: 'Estudante',
        },
        GRATUATE: {
            text: 'Diplomado',
        },
        TEACHER: {
            text: 'Docente',
        },
        NON_TEACHER: {
            text: 'Não Docente',
        },
        SCHOLAR: {
            text: 'Bolseiro',
        },
        RETIRED: {
            text: 'Aposentado',
        },
        ENTITY: {
            text: 'Entidade',
        },
    },
    interestAreas: [
        'Atividades Académicas',
        'Ambiental',
        'Apoio a Eventos',
        'Informática',
        'Comunicação',
        'Cultural',
        'Desporto',
        'Educação',
        'Saúde',
        'Social',
    ],
    reasons: [
        'Pelo convívio social',
        'Porque pode ser vantajoso para o futuro profissional',
        'Pela possibilidade de integração social',
        'Para ter novas experiências',
        'Porque gosto de ajudar os outros',
        'Porque fui incentivado(a) por outras pessoas',
        'Porque conheço pessoas que já realizaram atividades de voluntariado no IPS',
        'Para me sentir útil',
        'Para ocupar tempo livre',
    ],
    services: [
        'CIMOB - Centro para a Internacionalização e Mobilidade',
        'DA - Divisão Académica',
        'DFAP - Divisão Financeira, de Aprovisionamento e Património',
        'DI - Divisão Informática',
        'DRH - Divisão de Recursos Humanos',
        'GARDOC - Grupo de Apoio aos Recursos Documentais',
        'GI.COM - Gabinete de Imagem e Comunicação',
        'NEP - Núcleo de Estudos e Planeamento',
        'SPE - Serviço de Promoção da Empregabilidade',
        'UAIIDE - Unidade de Apoio à Inovação, I&D e Empreendedorismo',
        'UDRVC - Unidade de Desenvolvimento, Reconhecimento e Validação de Competências',
        'UNIQUA - Unidade de Avaliação e Qualidade',
    ],
    schoolCourse: {
        'ETLA (Sines) - Escola Tecnológica do Litoral Alentejano': [
            'Curso Técnico Superior Profissional em Automação, Robótica e Controlo Industrial',
        ],
        'APIEF - Centro de Formação Profissional para a Indústria Térmica, Energia e Ambiente': [
            'Curso Técnico Superior Profissional em Climatização e Energia',
        ],
        'CINEL (Lisboa) - Centro de Formação Profissional da Indústria Electrónica, Energia, Telecomunicações e Tecnologias da Informação': [
            'Curso Técnico Superior Profissional em Eletromedicina',
        ],
        'IPE (Lisboa) - Instituto dos Pupilos do Exército': [
            'Curso Técnico Superior Profissional em Sistemas Eletrónicos e Computadores',
            'Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de Informação',
        ],
        'CAPS - Centro Aeronáutico de Ponte de Sor': [
            'Curso Técnico Superior Profissional em Produção Aeronáutica',
        ],
        'ESTS - Escola Superior de Tecnologia de Setúbal': [
            'Curso Técnico Superior Profissional em Automação, Robótica e Controlo Industrial',
            'Curso Técnico Superior Profissional em Climatização e Energia',
            'Curso Técnico Superior Profissional em Eletromedicina',
            'Curso Técnico Superior Profissional em Gestão do Ambiente e Segurança',
            'Curso Técnico Superior Profissional em Instalações Elétricas',
            'Curso Técnico Superior Profissional em Manutenção Industrial',
            'Curso Técnico Superior Profissional em Modelação e Fabrico Assistidos por Computador',
            'Curso Técnico Superior Profissional em Organização e Gestão Industrial',
            'Curso Técnico Superior Profissional em Produção Aeronáutica',
            'Curso Técnico Superior Profissional em Programação Web, Dispositivos e Aplicações Móveis',
            'Curso Técnico Superior Profissional em Qualidade Ambiental e Alimentar',
            'Curso Técnico Superior Profissional em Redes e Sistemas Informáticos',
            'Curso Técnico Superior Profissional em Redes Elétricas Inteligentes e Domótica',
            'Curso Técnico Superior Profissional em Sistemas Eletrónicos e Computadores',
            'Curso Técnico Superior Profissional em Tecnologia e Gestão Automóvel',
            'Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de Informação - ESTSetúbal',
            'Curso Técnico Superior Profissional em Tecnologias e Programação de Sistemas de Informação (Turma do Programa BrightStart)',
            'Curso Técnico Superior Profissional em Tecnologias Informáticas (Turma do Programa BrightStart)',
            'Curso Técnico Superior Profissional em Veículos Elétricos',
            'Licenciatura em Tecnologia e Gestão Industrial',
            'Licenciatura em Engenharia de Automação, Controlo e Instrumentação',
            'Licenciatura em Engenharia Eletrotécnica e de Computadores',
            'Licenciatura em Engenharia Informática',
            'Licenciatura em Engenharia Mecânica',
            'Licenciatura em Tecnologia Biomédica',
            'Licenciatura em Tecnologias de Energia',
            'Licenciatura em Tecnologias do Ambiente e do Mar',
            'Mestrado em Engenharia de Produção',
            'Mestrado em Engenharia de Software',
            'Mestrado em Engenharia e Gestão de Energia na Indústria e Edifícios Novo',
            'Mestrado em Engenharia Eletrotécnica e de Computadores',
            'Mestrado em Informática de Gestão',
            'Mestrado em Segurança e Higiene no Trabalho',
            'Pós-Graduação em Motorização de Veículos Elétricos e Híbridos',
            'Pós-Graduação em Engenharia Informática',
            'Pós-Graduação em Tecnologia Aeronáutica Novo',
            'Pós-Graduação em Tecnologia Aeronáutica',
        ],
        'ESTB - Escola Superior de Tecnologia do Barreiro': [
            'Curso Técnico Superior Profissional Contrução Civil',
            'Curso Técnico Superior Profissional Reabilitação Energética e Conservação de Edifícios',
            'Curso Técnico Superior Profissional Tecnologias de Laboratório Químico e Biológico',
            'Curso Técnico Superior Profissional Tecnologias e Programação de Sistemas de Informação',
            'Licenciatura em Bioinformática',
            'Licenciatura em Biotecnologia',
            'Licenciatura em Engenharia Civil',
            'Licenciatura em Engenharia Química',
            'Licenciatura em Gestão da Construção',
            'Licenciatura em Tecnologias do Petróleo',
            'Mestrado Conservação e Reabilitação do Edificado',
            'Mestrado Engenharia Biológica e Química',
            'Mestrado Engenharia Civil',
        ],
        'ESE - Escola Superior de Educação': [
            'Curso Técnico Superior Profissional em Desportos de Natureza',
            'Curso Técnico Superior Profissional em Produção Audiovisual',
            'Curso Técnico Superior Profissional em Serviço Familiar e Comunitário',
            'Curso Técnico Superior Profissional de Apoio à Gestão de Organizações Sociais (em parceria com a ESCE/IPS)',
            'Curso Técnico Superior Profissional Gestão de Turismo (em parceria com a ESCE/IPS)',
            'Licenciatura em Animação e Intervenção Sociocultural',
            'Licenciatura em Comunicação Social',
            'Licenciatura em Desporto',
            'Licenciatura em Educação Básica',
            'Licenciatura em Tradução e Interpretação de Língua Gestual Portuguesa',
        ],
        'ESCE - Escola Superior de Ciências Empresariais': [
            'Curso Técnico Superior Profissional Apoio à Gestão de Organizações Sociais (em parceria com a ESE/IPS)',
            'Curso Técnico Superior Profissional Logística (regime diurno na ESCE/IPS)',
            'Curso Técnico Superior Profissional Logística (regime pós-laboral - turma a funcionar na ETLA em Sines)',
            'Curso Técnico Superior Profissional Gestão de Turismo (em parceria com a ESE/IPS)',
            'Curso Técnico Superior Profissional Gestão de Turismo (Turma a funcionar em Grândola) (em parceria com a ESE/IPS)',
            'Curso Técnico Superior Profissional Assessoria de Gestão',
            'Licenciatura em Contabilidade e Finanças',
            'Licenciatura em Contabilidade e Finanças Noturno',
            'Licenciatura em Gestão da Distribuição e da Logística',
            'Licenciatura em Gestão da Distribuição e da Logística Pós-Laboral',
            'Licenciatura em Gestão de Recursos Humanos',
            'Licenciatura em Gestão de Recursos Humanos Pós-Laboral',
            'Licenciatura em Gestão de Sistemas de Informação',
            'Licenciatura em Marketing',
            'Mestrado Ciências Empresariais',
            'Mestrado Contabilidade e Finanças',
            'Mestrado Gestão de Marketing',
            'Mestrado Gestão de Sistemas de Informação',
            'Mestrado Gestão e Administração de Escolas (em parceria com a ESE/IPS)',
            'Mestrado Gestão Estratégica de Recursos Humanos',
            'Mestrado Segurança e Higiene no Trabalho (em parceria com a ESTS/IPS)',
            'Mestrado Gestão em Hotelaria de Saúde e Bem-Estar (em parceria com a ESHTE - e com a ESS/IPS)',
            'Pós-Graduação Contabilidade Pública',
            'Pós-Graduação Gestão e Marketing Turístico (em parceria com o IPL)',
            'Pós-Graduação Empreeendedorismo e Negócios Turísticos (em parceria com a ESHTE e a ESS/IPS)',
        ],
        'ESS - Escola Superior de Saúde': [
            'Licenciatura em Acupuntura',
            'Licenciatura em Enfermagem',
            'Licenciatura em Fisioterapia',
            'Licenciatura em Terapia da Fala',
            'Mestrado Enfermagem',
            'Mestrado Estudos em Enfermagem',
            'Mestrado Fisioterapia em Condições Músculo-Esqueléticas',
            'Mestrado Gestão em Hotelaria de Saúde e Bem-Estar',
            'Pós-Graduação Enfermagem do Trabalho',
            'Pós-Graduação Gestão em Saúde e Enfermagem',
            'Pós-Graduação Saúde Sexual e Reprodutiva: Mutilação Genital Feminina',
        ],
    },
};
