const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

const { roles, memberTypes, accountStatus } = require('./_constants');

const UserSchema = new Schema(
    {
        name: { type: String, required: true },
        image: { type: String },
        email: {
            type: String,
            unique: true,
            required: true,
        },
        password: {
            type: String,
            required: true,
            set: (password) => {
                if (!password) return;
                const salt = bcrypt.genSaltSync(10);
                const hash = bcrypt.hashSync(password, salt);
                return hash.toString();
            },
        },
        phone: { type: String },
        address: { type: String },
        birthdate: { type: Date },
        observations: { type: String },
        accountStatus: {
            type: String,
            default: 'INACTIVE',
            enum: accountStatus,
        },
        activationToken: { type: String },
        passwordToken: { type: String },
        isExternal: { type: Boolean, default: false },
        memberType: {
            type: String,
            enum: Object.keys(memberTypes),
            get: (mt) => memberTypes[mt] && memberTypes[mt].text,
        },
        school: { type: String },
        course: { type: String },
        department: { type: String },
        service: { type: String },
        entity: { type: String },
        reasons: {
            type: [String],
        },
        interestAreas: {
            type: [String],
        },
        role: {
            type: String,
            default: 'USER',
            enum: Object.keys(roles),
        },
    },
    {
        toObject: { getters: true },
        toJSON: { getters: true },
    }
);

const User = model('User', UserSchema);
module.exports = User;
