const { Schema, model } = require('mongoose');

const DonationSchema = new Schema(
    {
        donator: { type: Schema.ObjectId, ref: 'User' },
        registeredBy: { type: Schema.ObjectId, ref: 'User' },
        products: [
            {
                product: { type: Schema.ObjectId, ref: 'Product' },
                amount: { type: Number },
            },
        ],
    },
    { timestamps: { createdAt: 'created_at' } }
);

const Donation = model('Donation', DonationSchema);
module.exports = Donation;
