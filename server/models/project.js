const { Schema, model } = require('mongoose');

const { projectTypes } = require('./_constants');

const STATUS = ['PENDDING', 'ACCEPTED', 'REJECTED'];

const ProjectSchema = new Schema(
    {
        type: {
            type: String,
            required: true,
            enum: Object.keys(projectTypes),
            get: (pt) => projectTypes[pt] && projectTypes[pt].text,
        },
        status: {
            type: String,
            required: true,
            enum: STATUS,
            default: 'PENDDING',
        },
        acceptedBy: { type: Schema.ObjectId, ref: 'User' },
        proposedBy: { type: Schema.ObjectId, ref: 'User' },
        isExternal: { type: Boolean, default: true },
        entity: { type: String },
        phone: { type: String, require: true },
        email: { type: String, require: true },
        userName: { type: String, require: true },
        title: { type: String, require: true },
        resume: { type: String, require: true },
        interventionArea: { type: String, require: true },
        targetAudience: { type: String, require: true },
        objectives: { type: String },
        description: { type: String, require: true },
        formationType: { type: String },
        mindate: { type: Date, require: true },
        maxdate: { type: Date, require: true },
        interestAreas: [{ type: String, require: true }],
        otherEntities: [{ type: String }],
        observations: { type: String },
        donations: {
            type: [{ type: Schema.ObjectId, ref: 'Donation' }],
            default: undefined,
        },

        autoAccept: { type: Boolean, default: false },
        maxVolunteers: { type: Number },
        volunteers: {
            type: [
                {
                    createdAt: { type: Date, default: new Date() },
                    status: {
                        type: String,
                        required: true,
                        enum: STATUS,
                        default: 'PENDDING',
                    },
                    user: { type: Schema.ObjectId, ref: 'User' },
                },
            ],
            default: undefined,
        },
    },
    { timestamps: true }
);

const Project = model('Project', ProjectSchema);
module.exports = Project;
