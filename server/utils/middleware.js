const { roles } = require('../models/_constants');
const config = require('../../config');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

function response(res, status, message, content) {
    res.status(status).json({ message, content });
}

async function verifyAuthorization(token) {
    try {
        const decoded = jwt.verify(token, config.jwt.secret);

        if (!decoded) return false;

        const user = await User.findOne({ _id: decoded.id });

        return user;
    } catch (err) {
        return { message: err, err };
    }
}

async function NeedsAuth(req, res, next) {
    try {
        if (!req.cookies.token) {
            return response(res, 401, 'Sessão não iniciada');
        } else {
            const currentUser = await verifyAuthorization(req.cookies.token);

            if (!currentUser._id) {
                res.clearCookie('token');
                return response(res, 401, 'Token Inválido');
            }
            req.user = currentUser;
        }
        next();
    } catch (err) {
        response(res, 500, 'Ocorreu um erro inesperado', err);
    }
}

function DontNeedsAuth(req, res, next) {
    try {
        if (req.cookies.token) {
            return response(res, 401, 'Sessão iniciada');
        }
        next();
    } catch (err) {
        response(res, 500, 'Ocorreu um erro inesperado', err);
    }
}

function VerifyRole(req, res, next, role) {
    try {
        if (!req.user) return response(res, 401, 'Sessão não iniciada');
        if (roles[req.user.role].level > role.level)
            return response(res, 401, 'Não autorizado');
        next();
    } catch (err) {
        response(res, 500, 'Ocorreu um erro inesperado', err);
    }
}

module.exports = {
    response,
    NeedsAuth,
    DontNeedsAuth,
    VerifyRole: (role) => (req, res, next) => VerifyRole(req, res, next, role),
};
