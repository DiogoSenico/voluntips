const config = require('../../config');

const User = require('../models/user');

async function admin() {
    try {
        const hasAdmin = await User.findOne({
            role: 'ADMIN',
        }).lean();
        if (hasAdmin) return;

        const admin_data = config.adminAccount;
        admin_data.role = 'ADMIN';
        admin_data.accountStatus = 'ACTIVE';

        const admin = new User(config.adminAccount);
        const err = admin.validateSync();

        if (err) throw err;

        await admin.save();
    } catch (err) {
        console.log(err);
    }
}

function populate() {
    admin();
}

module.exports = populate;
