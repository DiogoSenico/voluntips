const mongoose = require('mongoose');

const config = require('../../config');
const User = require('../models/user');

module.exports.init = async () => {
    mongoose.connect(config.remotedb, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    });
};

module.exports.populate = async () => {
    admin();
};

async function admin() {
    try {
        const hasAdmin = await User.findOne({
            role: 'ADMIN',
        }).lean();
        if (hasAdmin) return;

        const admin = new User(config.adminAccount);
        const err = admin.validateSync();

        if (err) throw err;

        await admin.save();
    } catch (err) {
        console.log(err);
    }
}
